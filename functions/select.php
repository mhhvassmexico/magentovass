<?php
class select
{
	private $_connection;

	public function __construct($cn)
	{
		$this->_connection = $cn;
	}

	public function selectPerfiles()
	{
		$this->_connection->query('SELECT * FROM perfil');		
		return $this->_connection->resultset();		
	}

	public function selectSprints()
	{
		$sql = "select a.nombre, a.fecha_inicio, a.fecha_fin, a.estatus
			      from sprint a			
			 where a.estatus = 1";
		$this->_connection->query($sql);		
		return $this->_connection->resultset();
	}

	public function selectTicketsSprint()
	{
		$sql = 'select a.id_r_st, a.id_sprint, a.id_ticket, a.id_perfil,
           c.numero_jira, c.nombre, c.descripcion, c.porcentaje_avance, c.link_jira, c.fecha_creacion, c.estatus,
           d.nombre nombreP, d.apellidos apellidosP, d.email emailP, d.avatar
			      from r_sprint_ticket a
			inner join sprint b on(b.id_sprint = a.id_sprint)  
			inner join ticket c on(c.id_ticket = a.id_ticket)
			inner join perfil d on(d.id_perfil = a.id_perfil)
			     where b.id_sprint = 1
			     order by c.fecha_creacion asc';

		$this->_connection->query($sql);
		return $this->_connection->resultset();
	}	

	public function TotalRegistroUsuario()
	{			
		$this->RegistrosUsuario();
		return $this->_connection->rowCount();		
	}

	public function RegistrosUsuarioTodos()
	{
		$sql = 'SELECT rc.fecha_registro, c.id_ciudadano, c.nombre, c.apellido_paterno, c.apellido_materno, c.fecha_nacimiento, c.sexo, dc.calle, dc.numero, dc.colonia, dc.municipio, s.nombre seccion, s.descripcion, u.nombre nombreUsuario, u.id_usuario, u.apellido_paterno apellidoPaternoUsuario, u.apellido_materno apellidoMaternoUsuario, u.sexo sexoUsuario, u.edad edadUsuario, u.email emailUsuario
		          FROM registro_ciudadano rc,
		               ciudadanos c,
		               direccion_ciudadano dc,
		               secciones s,
		               usuarios u
		         WHERE rc.id_ciudadano = c.id_ciudadano
		           AND rc.id_direccion = dc.id_direccion
		           AND rc.id_seccion = s.id_seccion
		           AND rc.id_usuario = u.id_usuario';

		$this->_connection->query($sql);		
		$this->_connection->resultset();
	}	


	public function TableRegistrosUsuariosSistema()
	{
		if(isset($_GET['filtro'])&&$_SESSION['accion']!=''){
			$sql = 'SELECT u.id_usuario, u.nombre nombreUsuario, u.apellido_paterno apellidoPaternoUsuario, u.apellido_materno apellidoMaternoUsuario, u.sexo sexoUsuario, u.edad edadUsuario, u.email emailUsuario, u.telefono,u.fecha_registro, a.usuario, a.estado, tu.nombre tipoUsuario, a.id_tipo_usuario,
       (select count(*) total from registro_ciudadano rc where rc.id_usuario = u.id_usuario) totalRegistros
		  FROM usuarios u		    
	RIGHT JOIN tipos_usuarios tu on(tu.id_tipo_usuario = tu.id_tipo_usuario)
	RIGHT JOIN acceso a on(a.id_usuario = u.id_usuario AND a.id_tipo_usuario = tu.id_tipo_usuario)
	WHERE tu.id_tipo_usuario = 3
 ORDER BY totalRegistros desc';	
		}else{
			$sql = 'SELECT u.id_usuario, u.nombre nombreUsuario, u.apellido_paterno apellidoPaternoUsuario, u.apellido_materno apellidoMaternoUsuario, u.sexo sexoUsuario, u.edad edadUsuario, u.email emailUsuario, u.telefono,u.fecha_registro, a.usuario, a.estado, tu.nombre tipoUsuario, a.id_tipo_usuario
					          FROM usuarios u		    
					    RIGHT JOIN tipos_usuarios tu on(tu.id_tipo_usuario = tu.id_tipo_usuario)
					    RIGHT JOIN acceso a on(a.id_usuario = u.id_usuario AND a.id_tipo_usuario = tu.id_tipo_usuario)';	
		}

		$this->_connection->query($sql);		
		return $this->_connection->resultset();		
	}

	public function TableRegistrosSeccionesTodos()
	{
		$sql = 'SELECT * FROM secciones';		    

		$this->_connection->query($sql);		
		return $this->_connection->resultset();	
	}

	public function TableRegistrosTiposUsuarios()
	{
		$sql = 'SELECT u.id_usuario, u.nombre, u.apellido_paterno, u.apellido_materno, tu.nombre tipoUsuario, ifnull((SELECT cu.id FROM capturistas_usuarios cu WHERE cu.id_usuario = u.id_usuario AND cu.id_capturista = :id_capturista), 0) id
      FROM usuarios u
INNER JOIN acceso a ON(a.id_usuario = u.id_usuario)
INNER JOIN tipos_usuarios tu ON(tu.id_tipo_usuario = a.id_tipo_usuario)
     WHERE a.id_tipo_usuario in(1,3)';		    

		$this->_connection->query($sql);		
		$this->_connection->bind(':id_capturista',$_GET['id_usuario']);
		return $this->_connection->resultset();	
	}

	public function TableRegistrosCasillasTodos()
	{
		$sql = 'SELECT * FROM casillas';		    

		$this->_connection->query($sql);		
		return $this->_connection->resultset();	
	}	

	public function TableRegistrosUsuariosTodos()
	{
		$this->RegistrosUsuarioTodos();
		return $this->_connection->resultset();
	}

	public function TableRegistrosUsuarios()
	{
		$this->RegistrosUsuario();
		return $this->_connection->resultset();
	}

	public function TotalRegistroUsuarioGlobal()
	{
		$this->_connection->query('SELECT * FROM registro_ciudadano');		
		$this->_connection->resultset();
		return $this->_connection->rowCount();		
	}

	public function TableRegistrosSecciones()
	{
		$sql = 'SELECT s.id_seccion, s.nombre, s.descripcion, ifnull((SELECT su.id_seccion_usuario FROM secciones_usuarios su where su.id_seccion = s.id_seccion AND su.id_usuario = :id_usuario), 0) id_seccion_usuario FROM secciones s';

		$this->_connection->query($sql);
		$this->_connection->bind(':id_usuario',$_SESSION['id_usuario']);
		return $this->_connection->resultset();		
	}

	public function DatosUsuario()
	{
		$sql = 'SELECT u.id_usuario, u.nombre nombreUsuario, u.apellido_paterno apellidoPaternoUsuario, u.apellido_materno apellidoMaternoUsuario, u.sexo sexoUsuario, u.edad edadUsuario, u.email emailUsuario, u.fecha_registro, u.telefono, tu.nombre tipoUsuario
		          FROM usuarios u,
		               acceso a,
		               tipos_usuarios tu
		         WHERE u.id_usuario = :id_usuario
		           AND u.id_usuario = a.id_usuario
		           AND a.id_tipo_usuario = tu.id_tipo_usuario';

		$this->_connection->query($sql);
		$this->_connection->bind(':id_usuario',$_SESSION['id_usuario']);
		return $this->_connection->single();
	}

	public function detalleciudadano()
	{
		$sql = "SELECT rc.id_registro, rc.id_seccion, rc.id_usuario, rc.id_casilla, rc.fecha_registro, c.id_ciudadano, c.nombre, c.apellido_paterno, c.apellido_materno, c.fecha_nacimiento, c.sexo, c.telefono, dc.calle, dc.numero, dc.colonia, dc.municipio, s.nombre seccion
      FROM registro_ciudadano rc
INNER JOIN ciudadanos c ON(c.id_ciudadano = rc.id_ciudadano)
INNER JOIN direccion_ciudadano dc ON(dc.id_direccion = rc.id_direccion)
INNER JOIN secciones s ON(s.id_seccion = rc.id_seccion)
     WHERE rc.id_ciudadano = :id_ciudadano";

     	$this->_connection->query($sql);
		$this->_connection->bind(':id_ciudadano',$_GET['id_ciudadano']);
		return $this->_connection->resultset();

	}
	
		public function skus()
	{
		$sql = "SELECT * FROM materiales";		
		$this->_connection->query($sql);
//		$this->_connection->bind(':usuario',$_POST['usuario']);
	//	$this->_connection->bind(':contrasena',MD5($_POST['contrasena']));		
		return $this->_connection->resultset();
		
	}

	public function funciones($sku)
	{
		
		$sql = "SELECT cod_funcion, nombre_funcion, descripcion_funcionalidad FROM funciones WHERE sku = :sku";
		$this->_connection->query($sql);
		$this->_connection->bind(':sku',$sku);
		return $this->_connection->resultset();
		
	}
}