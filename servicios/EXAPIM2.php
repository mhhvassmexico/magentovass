<?php
	
	//Authentication rest API magento2.Please change url accordingly your url
	$adminUrl='http://qa.movistar.com.mx/tienda/index.php/rest/V1/integration/admin/token';
	$ch = curl_init();
	$data = array("username" => "hector", "password" => "Hector2018api!");                                                                    
	$data_string = json_encode($data);                       
	$ch = curl_init($adminUrl); 
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
	    'Content-Type: application/json',                                                                                
	    'Content-Length: ' . strlen($data_string))                                                                       
	);       
	$token = curl_exec($ch);
	$token =  json_decode($token);

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Ejemplo de uso de API Magento 2</title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body style="padding: 24px;">
	<header style="background: #022540;padding: 24px;">
		<img src="http://www.vasslatam.mx/wp-content/blogs.dir/6/files/sites/6/2018/04/logo_vass_white.png" alt="Vass" style="display: block;position: relative;;margin: 0 auto;">
	</header>
	<h1>"Ejemplo de uso de API Magento 2"</h1>
	<h3>Explicación de autenticación basada en OAuth => <a href="https://devdocs.magento.com/guides/v2.2/get-started/authentication/gs-authentication-oauth.html" target="_blank"> https://devdocs.magento.com/guides/v2.2/get-started/authentication/gs-authentication-oauth.html</a></h3>
	<h3>Autenticación basada en tokens => <a href="https://devdocs.magento.com/guides/v2.2/get-started/authentication/gs-authentication-token.html" target="_blank">https://devdocs.magento.com/guides/v2.2/get-started/authentication/gs-authentication-token.html</a></h3>
	<h3>Link recomendado para crear búsquedas especializadas => <a href="https://devdocs.magento.com/guides/v2.2/rest/performing-searches.html" target="_blank"> https://devdocs.magento.com/guides/v2.2/rest/performing-searches.html</a></h3>


	<p>Test de funcionalidad:</p>
	<form action="#" method="POST">
		<select name="tquery" id="tquery" onchange="realizaBusqueda()">
			<option value="busqueda-0" id="elimina">Elija una opción</option>
		  	<option value="busqueda-1">1.- Buscar un sku en específico</option>
		  	<option value="busqueda-2">2.- Buscar los primeros 20 productos</option>
		  	<option value="busqueda-3">3.- Buscar todos los productos del catálogo</option>
		  	<option value="busqueda-4">4.- Filtrando datos relevantes sku 45734</option>
		</select>
		<input type="hidden" name="token" id="token" value="<?php echo $token; ?>">
	</form>
	
	<div id="resultado" style="border:2px solid #bababa;position:relative;display: block;margin: 24px auto;width: 90%;min-height: 24px;padding: 12px;"></div>
<h3>Detalles de los archivos y código esencial:</h3>
<blockquote>
	PHP: /*Encargado de obtener el token de seguridad para poder hacer uso de la API de MAgento 2*/ <br>
	//Authentication rest API magento2.Please change url accordingly your url <br>
	$adminUrl='<b>-hostingMagento-</b>/index.php/rest/V1/integration/admin/token'; <br>
	$ch = curl_init(); <br>
	$data = array("username" => "<b>-usuario-</b>", "password" =>"<b>-contraseña-</b>"); <br>
	$data_string = json_encode($data); <br>                       
	$ch = curl_init($adminUrl); <br> 
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST"); <br>                                                                     
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string); <br>                                                                 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); <br>                                                                      
	curl_setopt($ch, CURLOPT_HTTPHEADER, array( <br>                                                                         
	    'Content-Type: application/json', <br>                                                                                
	    'Content-Length: ' . strlen($data_string)) <br>                                                                       
	); <br>       
	$token = curl_exec($ch); <br>
	$token =  json_decode($token); <br>
</blockquote>
<blockquote>
	PHP: /*Encargado de realizar la búsqueda indicada y retornar la respuesta en formato json*/ <br>
	//Use above token into header <br>
	$headers = array("Authorization: Bearer $token"); <br><br>

	$requestUrl='<b>-hostingMagento-</b>/index.php/rest/V1/products/<b>-criterios de busqueda-</b>' <br><br>

	$ch = curl_init(); <br>
	$ch = curl_init($requestUrl); <br>
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers); <br>
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); <br><br> 

	$result = curl_exec($ch); <br>
	$result=  json_decode($result); <br>
	var_dump($result); <br>
</blockquote>

<blockquote>
	JAVA: <br>
        Se encontró un módulo de ejemplo que circula por la red => 
        <a href="recursosdig/java-magento-client-master.zip" >Descarga aquí</a>

</blockquote>
<h3>Detalle a groso modo de la estructura de cada búsqueda señala en la sección superior:</h3>
<ul>
	<li>1.- Buscar un sku en específico: <b>-hostingMagento-</b>/rest/V1/products/<b>-sku-</b></li>
	<li>2.- Buscar los primeros 20 productos: <b>-hostingMagento-</b>/rest/V1/products<b>?searchCriteria[pageSize]=20</b></li>
	<li>3.- Buscar todos los productos del catálogo: <b>-hostingMagento-</b>/rest/V1/products<b>?searchCriteria</b></li>
	<li>4.- Filtrando datos relevantes sku 45734: <b>-hostingMagento-</b>/rest/V1/products/<b>-sku-</b> <br><br> 
		//En el ejemplo 4 manipulamos el json devuleto por Magento<br>
		foreach($result as $clave => $valor) { <br>
			if($clave == "price" || $clave == "sku" || $clave == "name"){ <br>
				echo "$clave => $valor\n"; <br>
			} <br>
		} <br>
	</li>
</ul>
<p>
	Con esto lograremos hacer uso de la API y manejar el json retornado por Magento como mejor nos convenga.
</p>
<footer style="background: #f5f5f5">
	<img src="http://www.vasslatam.mx/wp-content/blogs.dir/6/files/sites/6/2018/04/logotipo-VASS.png" alt="Vass" style="display: block;position: relative;;margin: 0 auto;">
</foooter>

<script type="text/javascript">
	function realizaBusqueda(valorCaja1,valorCaja2){
		$('#elimina').remove();
		var parametros ={
			"valorCaja1":  $('#tquery').val(),
			"valorCaja2": $('#token').val()
		};
		$.ajax({
			data: parametros,
			url: 'querys.php',
			type: 'post',
			beforeSend: function(){
				$('#resultado').html('Procesando, espere por favor....');		
			},
			success: function(response){
				$('#resultado').html(response);
			}
		});
	}
</script>

</body>
</html>