        <!-- Left Sidebar  -->
        <div class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="nav-devider"></li>
                        <li class="nav-label"><a href="admin.php">Inicio</a></li>
                        <li> 
                            <a class="has-arrow  " href="#" aria-expanded="false">
                                <i class="fa fa-users"></i>
                                <span class="hide-menu">Ciudadanos <span class="label label-rouded label-primary pull-right"><?php echo $select->TotalRegistroUsuario(); ?></span></span>
                            </a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="registrar.php">Registrar </a></li>
                                <?php // if($_SESSION['id_tipo_usuario']!=1): ?>
                                <li><a href="mostrar.php">Mostrar Registros</a></li>
                                <?php // endif; ?>
                            </ul>
                        </li>
                        <?php if($_SESSION['id_tipo_usuario']==1):?>
                            <li class="nav-label">Administración</li>
                            <li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-bell"></i><span class="hide-menu">Control</span></a>
                                <ul aria-expanded="false" class="collapse">
                                    <li><a href="mostrar-todos.php">Todos los Registros</a></li>                                    
                                </ul>
                            </li>  
                            <li> <a class="has-arrow" href="#" aria-expanded="false"><i class="fa fa-users"></i><span class="hide-menu">Usuarios</span></a>
                                <ul aria-expanded="false" class="collapse">
                                    <li><a href="agregar-usuario.php">Agregar Usuario</a></li>
                                    <li><a href="mostrar-usuarios.php">Mostrar Usuarios</a></li>                                    
                                </ul>
                            </li>
                            <!--
                            <li><a class="has-arrow" href="#" aria-expanded="false"><i class="fas fa-money-check"></i><span class="hide-menu">Catalogos</span></a>
                                <ul aria-expanded="false" class="collapse">
                                    <li><a class="has-arrow" href="#" aria-expanded="false">Secciones</a>
                                        <ul aria-expanded="false" class="collapse">
                                            <li><a href="#">Agregar Seccion</a></li>
                                            <li><a href="#">Mostrar Secciones</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                        -->
                        <li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-book"></i><span class="hide-menu">Catalogos</span></a>
                            <ul aria-expanded="false" class="collapse">                                
                                <li> <a class="has-arrow" href="#" aria-expanded="false">Secciones</a>
                                    <ul aria-expanded="false" class="collapse">
                                        <li><a href="agregar-seccion.php">Agregar Sección</a></li>
                                        <li><a href="mostrar-secciones.php">Mostrar Secciones</a></li>
                                    </ul>
                                </li>                                
                            </ul>
                            <ul aria-expanded="false" class="collapse">                                
                                <li> <a class="has-arrow" href="#" aria-expanded="false">Casillas</a>
                                    <ul aria-expanded="false" class="collapse">
                                        <li><a href="agregar-casilla.php">Agregar casilla</a></li>
                                        <li><a href="mostrar-casillas.php">Mostrar casillas</a></li>
                                    </ul>
                                </li>                                
                            </ul>                            
                        </li>                            
                        <?php endif;?>
                        <!--                                        
                        <li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-map-marker"></i><span class="hide-menu">Maps</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="map-google.html">Google</a></li>
                                <li><a href="map-vector.html">Vector</a></li>
                            </ul>
                        </li>
                        <li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-level-down"></i><span class="hide-menu">Multi level dd</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="#">item 1.1</a></li>
                                <li><a href="#">item 1.2</a></li>
                                <li> <a class="has-arrow" href="#" aria-expanded="false">Menu 1.3</a>
                                    <ul aria-expanded="false" class="collapse">
                                        <li><a href="#">item 1.3.1</a></li>
                                        <li><a href="#">item 1.3.2</a></li>
                                        <li><a href="#">item 1.3.3</a></li>
                                        <li><a href="#">item 1.3.4</a></li>
                                    </ul>
                                </li>
                                <li><a href="#">item 1.4</a></li>
                            </ul>
                        </li>
                    -->
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </div>
        <!-- End Left Sidebar  -->
        <!-- Page wrapper  -->
        <div class="page-wrapper">        