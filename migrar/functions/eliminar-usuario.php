<?php
include '../include/config.php';

$cn = new connection();

// Eliminamos Registro Usuario
$cn->query("DELETE FROM usuarios WHERE id_usuario = :id_usuario");
$cn->bind(':id_usuario', $_POST['id_usuario']);
$cn->execute();
// Eliminamos Registro Acceso
$cn->query("DELETE FROM acceso WHERE id_usuario = :id_usuario");
$cn->bind(':id_usuario', $_POST['id_usuario']);
$cn->execute();