<?php
class select
{
	private $_connection;

	public function __construct($cn)
	{
		$this->_connection = $cn;
	}

	public function selectTiposUsuarios()
	{
		$this->_connection->query('SELECT * FROM tipos_usuarios');
		$data = $this->_connection->resultset();
		$s = '';
		for($i=0;$i<count($data);$i++){
			$s .= '<option value="'.$data[$i]['id_tipo_usuario'].'">'.$data[$i]['nombre'].'</option>';
		}
		return $s;
	}

	public function NombreMovilizador()
	{
		$sql = "SELECT nombre, apellido_paterno, apellido_materno FROM usuarios WHERE id_usuario = :id_usuario";
		$this->_connection->query($sql);
		$this->_connection->bind(':id_usuario',$_GET['id_usuario']);
		return $this->_connection->single();
	}

	public function selectCasillas()
	{
		$sql = 'SELECT * 
		          FROM casillas';

		$this->_connection->query($sql);
		$data = $this->_connection->resultset();
		$s = '';
		$s .= '<option value="0">Selecciona Casilla</option>';
		for($i=0;$i<count($data);$i++){
			$s .= '<option value="'.$data[$i]['id_casilla'].'">'.$data[$i]['nombre'].'</option>';
		}
		return $s;
	}	

	public function selectMovilizadores()
	{
		$sql = 'SELECT u.id_usuario, u.nombre, u.apellido_paterno, u.apellido_materno
			      FROM capturistas_usuarios cu
			INNER JOIN usuarios u ON(u.id_usuario = cu.id_usuario)     
			    WHERE cu.id_capturista = '.$_SESSION['id_usuario'];

		$this->_connection->query($sql);
		$data = $this->_connection->resultset();
		$s = '';
		$s .= '<option value="0">Selecciona Movilizador</option>';
		for($i=0;$i<count($data);$i++){
			$s .= '<option value="'.$data[$i]['id_usuario'].'">('.$data[$i]['id_usuario'].') '.$data[$i]['nombre'].' ' .$data[$i]['apellido_paterno'].' '.$data[$i]['apellido_materno'].'</option>';
		}
		return $s;
	}

	public function selectSecciones()
	{
		if($_SESSION['id_tipo_usuario']==2){
			$this->_connection->query('SELECT * FROM secciones');
		}else{
			$this->_connection->query('SELECT s.id_seccion, s.nombre, s.descripcion
									      FROM secciones s
									INNER JOIN secciones_usuarios su ON(su.id_seccion = s.id_seccion)
									     WHERE su.id_usuario = '.$_SESSION['id_usuario']);
		}

		$data = $this->_connection->resultset();
		$s = '';
		for($i=0;$i<count($data);$i++){
			$s .= '<option value="'.$data[$i]['id_seccion'].'">'.$data[$i]['nombre'].'</option>';
		}
		return $s;

	}

	public function TotalRegistroUsuario()
	{			
		$this->RegistrosUsuario();
		return $this->_connection->rowCount();		
	}

	public function RegistrosUsuarioTodos()
	{
		$sql = 'SELECT rc.fecha_registro, c.id_ciudadano, c.nombre, c.apellido_paterno, c.apellido_materno, c.fecha_nacimiento, c.sexo, dc.calle, dc.numero, dc.colonia, dc.municipio, s.nombre seccion, s.descripcion, u.nombre nombreUsuario, u.id_usuario, u.apellido_paterno apellidoPaternoUsuario, u.apellido_materno apellidoMaternoUsuario, u.sexo sexoUsuario, u.edad edadUsuario, u.email emailUsuario
		          FROM registro_ciudadano rc,
		               ciudadanos c,
		               direccion_ciudadano dc,
		               secciones s,
		               usuarios u
		         WHERE rc.id_ciudadano = c.id_ciudadano
		           AND rc.id_direccion = dc.id_direccion
		           AND rc.id_seccion = s.id_seccion
		           AND rc.id_usuario = u.id_usuario';

		$this->_connection->query($sql);		
		$this->_connection->resultset();
	}	

	public function RegistrosUsuario()
	{
		// capturista
		if($_SESSION['id_tipo_usuario']==2){
			/*
			$sql = 'SELECT c.id_ciudadano, rc.fecha_registro, c.nombre, c.apellido_paterno, c.apellido_materno, c.fecha_nacimiento, c.sexo, dc.calle, dc.numero, dc.colonia, dc.municipio, s.nombre seccion, s.descripcion, u.nombre nombreUsuario, u.apellido_paterno apellidoPaternoUsuario, u.apellido_materno apellidoMaternoUsuario, u.sexo sexoUsuario, u.edad edadUsuario, u.email emailUsuario, ca.nombre casilla
			          FROM registro_ciudadano rc			               
                INNER JOIN ciudadanos c ON(rc.id_ciudadano = c.id_ciudadano)
                INNER JOIN direccion_ciudadano dc ON(rc.id_direccion = dc.id_direccion)
                INNER JOIN secciones s ON(rc.id_seccion = s.id_seccion)
                INNER JOIN usuarios u ON(rc.id_usuario = u.id_usuario)
                 RIGHT JOIN casillas ca ON(ca.id_casilla = rc.id_casilla)                 
			         WHERE rc.id_usuario = '.$_SESSION['id_usuario'];
			         */
			$sql = 'SELECT c.id_ciudadano, rci.fecha_registro, c.nombre, c.apellido_paterno, c.apellido_materno, c.fecha_nacimiento, c.sexo, dc.calle, dc.numero, dc.colonia, dc.municipio, s.nombre seccion, s.descripcion, u.nombre nombreUsuario, u.apellido_paterno apellidoPaternoUsuario, u.apellido_materno apellidoMaternoUsuario, u.sexo sexoUsuario, u.edad edadUsuario, u.email emailUsuario, ca.nombre casilla
			          FROM registro_capturista rc
			    INNER JOIN registro_ciudadano rci ON(rci.id_registro = rc.id_registro)
			    INNER JOIN ciudadanos c ON(c.id_ciudadano = rci.id_ciudadano)
			    INNER JOIN direccion_ciudadano dc ON(dc.id_direccion = rci.id_direccion)
			    INNER JOIN secciones s ON(s.id_seccion = rci.id_seccion)
			    INNER JOIN usuarios u ON(u.id_usuario = rci.id_usuario)
			    LEFT JOIN casillas ca ON(ca.id_casilla = rci.id_casilla)
			         WHERE rc.id_usuario = '.$_SESSION['id_usuario'];
		}else{
			$sql = 'SELECT c.id_ciudadano, rc.fecha_registro, c.nombre, c.apellido_paterno, c.apellido_materno, c.fecha_nacimiento, c.sexo, dc.calle, dc.numero, dc.colonia, dc.municipio, s.nombre seccion, s.descripcion, u.nombre nombreUsuario, u.apellido_paterno apellidoPaternoUsuario, u.apellido_materno apellidoMaternoUsuario, u.sexo sexoUsuario, u.edad edadUsuario, u.email emailUsuario, ca.nombre casilla
			          FROM registro_ciudadano rc			               
                INNER JOIN ciudadanos c ON(rc.id_ciudadano = c.id_ciudadano)
                INNER JOIN direccion_ciudadano dc ON(rc.id_direccion = dc.id_direccion)
                 LEFT JOIN secciones s ON(rc.id_seccion = s.id_seccion)
                INNER JOIN usuarios u ON(rc.id_usuario = u.id_usuario)
                 LEFT JOIN casillas ca ON(ca.id_casilla = rc.id_casilla)                 
			         WHERE rc.id_usuario = :id_usuario';
		}
		$this->_connection->query($sql);
		$this->_connection->bind(':id_usuario',$_SESSION['id_usuario']);
		$this->_connection->resultset();
	}

	public function TableRegistrosUsuariosSistema()
	{
		if(isset($_GET['filtro'])&&$_SESSION['accion']!=''){
			$sql = 'SELECT u.id_usuario, u.nombre nombreUsuario, u.apellido_paterno apellidoPaternoUsuario, u.apellido_materno apellidoMaternoUsuario, u.sexo sexoUsuario, u.edad edadUsuario, u.email emailUsuario, u.telefono,u.fecha_registro, a.usuario, a.estado, tu.nombre tipoUsuario, a.id_tipo_usuario,
       (select count(*) total from registro_ciudadano rc where rc.id_usuario = u.id_usuario) totalRegistros
		  FROM usuarios u		    
	RIGHT JOIN tipos_usuarios tu on(tu.id_tipo_usuario = tu.id_tipo_usuario)
	RIGHT JOIN acceso a on(a.id_usuario = u.id_usuario AND a.id_tipo_usuario = tu.id_tipo_usuario)
	WHERE tu.id_tipo_usuario = 3
 ORDER BY totalRegistros desc';	
		}else{
			$sql = 'SELECT u.id_usuario, u.nombre nombreUsuario, u.apellido_paterno apellidoPaternoUsuario, u.apellido_materno apellidoMaternoUsuario, u.sexo sexoUsuario, u.edad edadUsuario, u.email emailUsuario, u.telefono,u.fecha_registro, a.usuario, a.estado, tu.nombre tipoUsuario, a.id_tipo_usuario
					          FROM usuarios u		    
					    RIGHT JOIN tipos_usuarios tu on(tu.id_tipo_usuario = tu.id_tipo_usuario)
					    RIGHT JOIN acceso a on(a.id_usuario = u.id_usuario AND a.id_tipo_usuario = tu.id_tipo_usuario)';	
		}

		$this->_connection->query($sql);		
		return $this->_connection->resultset();		
	}

	public function TableRegistrosSeccionesTodos()
	{
		$sql = 'SELECT * FROM secciones';		    

		$this->_connection->query($sql);		
		return $this->_connection->resultset();	
	}

	public function TableRegistrosTiposUsuarios()
	{
		$sql = 'SELECT u.id_usuario, u.nombre, u.apellido_paterno, u.apellido_materno, tu.nombre tipoUsuario, ifnull((SELECT cu.id FROM capturistas_usuarios cu WHERE cu.id_usuario = u.id_usuario AND cu.id_capturista = :id_capturista), 0) id
      FROM usuarios u
INNER JOIN acceso a ON(a.id_usuario = u.id_usuario)
INNER JOIN tipos_usuarios tu ON(tu.id_tipo_usuario = a.id_tipo_usuario)
     WHERE a.id_tipo_usuario in(1,3)';		    

		$this->_connection->query($sql);		
		$this->_connection->bind(':id_capturista',$_GET['id_usuario']);
		return $this->_connection->resultset();	
	}

	public function TableRegistrosCasillasTodos()
	{
		$sql = 'SELECT * FROM casillas';		    

		$this->_connection->query($sql);		
		return $this->_connection->resultset();	
	}	

	public function TableRegistrosUsuariosTodos()
	{
		$this->RegistrosUsuarioTodos();
		return $this->_connection->resultset();
	}

	public function TableRegistrosUsuarios()
	{
		$this->RegistrosUsuario();
		return $this->_connection->resultset();
	}

	public function TotalRegistroUsuarioGlobal()
	{
		$this->_connection->query('SELECT * FROM registro_ciudadano');		
		$this->_connection->resultset();
		return $this->_connection->rowCount();		
	}

	public function TableRegistrosSecciones()
	{
		$sql = 'SELECT s.id_seccion, s.nombre, s.descripcion, ifnull((SELECT su.id_seccion_usuario FROM secciones_usuarios su where su.id_seccion = s.id_seccion AND su.id_usuario = :id_usuario), 0) id_seccion_usuario FROM secciones s';

		$this->_connection->query($sql);
		$this->_connection->bind(':id_usuario',$_SESSION['id_usuario']);
		return $this->_connection->resultset();		
	}

	public function DatosUsuario()
	{
		$sql = 'SELECT u.id_usuario, u.nombre nombreUsuario, u.apellido_paterno apellidoPaternoUsuario, u.apellido_materno apellidoMaternoUsuario, u.sexo sexoUsuario, u.edad edadUsuario, u.email emailUsuario, u.fecha_registro, u.telefono, tu.nombre tipoUsuario
		          FROM usuarios u,
		               acceso a,
		               tipos_usuarios tu
		         WHERE u.id_usuario = :id_usuario
		           AND u.id_usuario = a.id_usuario
		           AND a.id_tipo_usuario = tu.id_tipo_usuario';

		$this->_connection->query($sql);
		$this->_connection->bind(':id_usuario',$_SESSION['id_usuario']);
		return $this->_connection->single();
	}

	public function detalleciudadano()
	{
		$sql = "SELECT rc.id_registro, rc.id_seccion, rc.id_usuario, rc.id_casilla, rc.fecha_registro, c.id_ciudadano, c.nombre, c.apellido_paterno, c.apellido_materno, c.fecha_nacimiento, c.sexo, c.telefono, dc.calle, dc.numero, dc.colonia, dc.municipio, s.nombre seccion
      FROM registro_ciudadano rc
INNER JOIN ciudadanos c ON(c.id_ciudadano = rc.id_ciudadano)
INNER JOIN direccion_ciudadano dc ON(dc.id_direccion = rc.id_direccion)
INNER JOIN secciones s ON(s.id_seccion = rc.id_seccion)
     WHERE rc.id_ciudadano = :id_ciudadano";

     	$this->_connection->query($sql);
		$this->_connection->bind(':id_ciudadano',$_GET['id_ciudadano']);
		return $this->_connection->resultset();

	}
}