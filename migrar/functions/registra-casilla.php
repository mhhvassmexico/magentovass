<?php
session_start();
include '../include/config.php';
include '../include/query.php';
include '../class/Session.php';
include '../class/Redirect.php';

$cn = new connection();
// Redirect
$redirect = new Redirect();

$cn->query("INSERT INTO casillas(nombre, descripcion) VALUES(:nombre, :descripcion)");
$cn->bind(':nombre', $_POST['val-nombre_casilla']);
$cn->bind(':descripcion', $_POST['val-descripcion']);
$cn->execute();

$redirect->Location('AgregarCasilla');