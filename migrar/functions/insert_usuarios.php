<?php
include '../include/config.php';

$cn = new connection();
$cn->query("INSERT INTO usuarios(nombre, apellido_paterno, apellido_materno, sexo, edad, email) VALUES(:nombre, :apellido_paterno, :apellido_materno, :sexo, :edad, :email)");
$cn->bind(':nombre', 'Marcos Antonio');
$cn->bind(':apellido_paterno', 'Urban');
$cn->bind(':apellido_materno', 'Olivares');
$cn->bind(':sexo', 1);
$cn->bind(':edad', 36);
$cn->bind(':email', 'mark_urbn@hotmail.com');
$cn->execute();
