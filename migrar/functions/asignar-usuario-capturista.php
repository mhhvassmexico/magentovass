<?php
include '../include/config.php';

$cn = new connection();

$cn->query('SELECT * FROM capturistas_usuarios WHERE id_capturista = :id_capturista AND id_usuario = :id_usuario');
$cn->bind(':id_capturista', $_POST['id_capturista']);
$cn->bind(':id_usuario', $_POST['id_usuario']);
$cn->resultset();
$total = $cn->rowCount();


if($total <= 0){	
	if($_POST['active']==true&&$total<=0){		
		$cn->query("INSERT INTO capturistas_usuarios(id_capturista, id_usuario) VALUES(:id_capturista, :id_usuario)");
		$cn->bind(':id_capturista', $_POST['id_capturista']);
		$cn->bind(':id_usuario', $_POST['id_usuario']);		
		$cn->execute();
		echo "1";
	}else{		
		$cn->query("DELETE FROM capturistas_usuarios WHERE id_capturista = :id_capturista AND id_usuario = :id_usuario");
		$cn->bind(':id_capturista', $_POST['id_capturista']);
		$cn->bind(':id_usuario', $_POST['id_usuario']);		
		$cn->execute();
		echo "0";
	}	
}else{	
	if($_POST['active']==true&&$total<=0){		
		$cn->query("INSERT INTO capturistas_usuarios(id_capturista, id_usuario) VALUES(:id_capturista, :id_usuario)");
		$cn->bind(':id_capturista', $_POST['id_capturista']);
		$cn->bind(':id_usuario', $_POST['id_usuario']);	
		$cn->execute();
		echo "1";
	}else{				
		$cn->query("DELETE FROM capturistas_usuarios WHERE id_capturista = :id_capturista AND id_usuario = :id_usuario");
		$cn->bind(':id_capturista', $_POST['id_capturista']);
		$cn->bind(':id_usuario', $_POST['id_usuario']);	
		$cn->execute();
		echo "0";
	}	
}