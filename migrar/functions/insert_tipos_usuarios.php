<?php
include '../include/config.php';

$cn = new connection();
$cn->query("INSERT INTO tipos_usuarios(nombre, descripcion) VALUES(:nombre, :descripcion)");
$cn->bind(':nombre', 'Movilizador');
$cn->bind(':descripcion', 'Usuarios encargados de registrar ciudadanos');
$cn->execute();
