<?php
include '../include/config.php';

$cn = new connection();

$sql = 'SELECT * FROM registro_ciudadano WHERE id_ciudadano = :id_ciudadano';
$cn->query($sql);
$cn->bind(':id_ciudadano',$_POST['id_ciudadano']);
$result = $cn->single();

// Eliminamos Registro Ciudadano
$cn->query("DELETE FROM ciudadanos WHERE id_ciudadano = :id_ciudadano");
$cn->bind(':id_ciudadano', $_POST['id_ciudadano']);
$cn->execute();
// Eliminamos Registro Direccion Ciudadano
$cn->query("DELETE FROM direccion_ciudadano WHERE id_direccion = :id_direccion");
$cn->bind(':id_direccion', $result['id_direccion']);
$cn->execute();
// Eliminamos Registro Ciudadano
$cn->query("DELETE FROM registro_ciudadano WHERE id_registro = :id_registro");
$cn->bind(':id_registro', $result['id_registro']);
$cn->execute();
// Eliminamos Registro Capturista
$cn->query("DELETE FROM registro_capturista WHERE id_registro = :id_registro");
$cn->bind(':id_registro', $result['id_registro']);
$cn->execute();