<?php
session_start();
include '../include/config.php';
include '../include/query.php';
include '../class/Session.php';
include '../class/Redirect.php';

$cn = new connection();
// Redirect
$redirect = new Redirect();
// Inserta registro ciudadano
$sql = new query();
$sqlRes = $sql->registraCiudadano();

$cn->query($sqlRes);
$cn->bind(':nombre', $_POST['val-nombre']);
$cn->bind(':apellido_paterno', $_POST['val-apellidop']);
$cn->bind(':apellido_materno', $_POST['val-apellidom']);
$cn->bind(':sexo', $_POST['val-genero']);
$cn->bind(':telefono', $_POST['val-genero']);
$cn->execute();

$insert_id_Ciudadano = $cn->lastInsertId();
// Inserta registro direccion
$sqlResDir = $sql->registraDireccion();

$cn->query($sqlResDir);
$cn->bind(':id_estado', 1);
$cn->bind(':calle', $_POST['val-calle']);
$cn->bind(':numero', $_POST['val-numero']);
$cn->bind(':colonia', $_POST['val-colonia']);
$cn->bind('municipio', $_POST['val-municipio']);
$cn->execute();

$insert_id_Direccion = $cn->lastInsertId();
// Inserta Registro

$sqlResReg = $sql->registraRegistro();

$cn->query($sqlResReg);
$cn->bind(':id_ciudadano', $insert_id_Ciudadano);
$cn->bind(':id_direccion', $insert_id_Direccion);

if(isset($_POST['val-seccion']) && $_POST['val-seccion']!=''){
	$seccion = $_POST['val-seccion'];
}else{
	$seccion = '';
}
$cn->bind(':id_seccion', $seccion);
if($_SESSION['id_tipo_usuario']==2){
	$cn->bind(':id_usuario', $_POST['val-movilizador']);
}else{
	$cn->bind(':id_usuario', $_SESSION['id_usuario']);
}

if(isset($_POST['val-casilla']) && $_POST['val-casilla']!=''){
	$casilla = $_POST['val-casilla'];
}else{
	$casilla = '';
}

$cn->bind(':id_casilla', $casilla);

$cn->execute();

$insert_id_Registro = $cn->lastInsertId();

// valida si es un capturista para generar un registro de Captura
if($_SESSION['id_tipo_usuario']==2){
	$sqlResCap = $sql->registraCapturista();
	$cn->query($sqlResCap);
	$cn->bind(':id_registro',$insert_id_Registro);
	$cn->bind(':id_usuario',$_SESSION['id_usuario']);
	$cn->execute();
}

// validamos si trae campo de no padron
if(isset($_POST['val-no_padron'])&& $_POST['val-no_padron']!=''){
	$sqlResCap = $sql->registraNoPadron();
	$cn->query($sqlResCap);
	$cn->bind(':id_registro',$insert_id_Registro);
	$cn->bind(':no_padron',$_POST['val-no_padron']);
	$cn->execute();	
}

$redirect->Location('Admin');
?>
