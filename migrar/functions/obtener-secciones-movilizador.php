<?php
include '../include/config.php';

$cn = new connection();
$sql = 'SELECT s.id_seccion, s.nombre
          FROM secciones_usuarios su 
    INNER JOIN secciones s ON(s.id_seccion = su.id_seccion)
         WHERE su.id_usuario = :id_usuario';
$cn->query($sql);
$cn->bind(':id_usuario', $_POST['id_movilizador']);
$data = $cn->resultset();

echo '<select class="form-control custom-select" name="val-seccion">';
for($i=0;$i<count($data);$i++){
echo '<option value="'.$data[$i]['id_seccion'].'">'.$data[$i]['nombre'].'</option>';
}
echo '</select>';
