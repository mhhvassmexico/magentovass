<?php
session_start();
include '../include/config.php';
include '../include/query.php';
include '../class/Session.php';
include '../class/Redirect.php';

$cn = new connection();
// Redirect
$redirect = new Redirect();
// Inserta registro ciudadano
$sql = new query();
$sqlRes = $sql->registraUsuario();

$cn->query($sqlRes);
$cn->bind(':nombre', $_POST['val-nombre']);
$cn->bind(':apellido_paterno', $_POST['val-apellidop']);
$cn->bind(':apellido_materno', $_POST['val-apellidom']);
$cn->bind(':sexo', $_POST['val-genero']);
$cn->bind(':edad', '0');
$cn->bind(':email', $_POST['val-email']);
$cn->bind(':telefono', '');
$cn->execute();

$insert_id_Usuario = $cn->lastInsertId();
// Inserta registro direccion
$sqlResAcc = $sql->registraAcceso();

$cn->query($sqlResAcc);
$cn->bind(':id_usuario', $insert_id_Usuario);
$cn->bind(':id_tipo_usuario', $_POST['val-tipo_usuario']);
$cn->bind(':usuario', $_POST['val-usuario-email']);
$cn->bind(':contrasena', MD5($_POST['val-contrasena']));
$cn->bind(':estado', 1);
$cn->execute();

$redirect->Location('Admin');
?>
