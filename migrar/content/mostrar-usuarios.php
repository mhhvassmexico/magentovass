<?php global $select, $helper; ?>
<?php 
$data = $select->TableRegistrosUsuariosSistema(); 
$totalRegistros = $select->TotalRegistroUsuarioGlobal();
?>
<div class="row">
    <!-- /# column -->
    <div class="col-lg-12">
        <div class="card">
            <div class="card-title">
                <h4>Registro de Usuario <?php if(isset($_GET['filtro'])&&$_SESSION['accion']!=''):?><strong>(<?php echo strtoupper($_GET['filtro']);?>)</strong><?php endif;?></h4>
                <?php if(isset($_GET['filtro'])&&$_SESSION['accion']!=''):?>
                <span class="float-right">TOTAL GLOBAL <strong style="font-size: 28px;">1891</strong></span>
            <?php endif; ?>
            </div>
            <hr>                            
                <div class="card-body">
                    <form action="#">
                        
                        <div class="form-actions">
                            <label><strong>Filtros: </strong></label>
                            <a href="?filtro=movilizadores" class="btn btn-success"> <i class="fa fa-users"></i> Movilizadores</a>
                            <?php if(isset($_GET['filtro'])&&$_SESSION['accion']!=''):?>
                            <a href="mostrar-usuarios.php" class="btn btn-inverse">Limpiar Filtro</a>
                        <?php endif;?>
                        </div>
                    </form>
                </div>
            <hr>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nombre</th>
                                <th>Email</th>
                                <th>Telefono</th>
                                <th>Nivel</th>
                                <?php if(isset($_GET['filtro'])&&$_SESSION['accion']!=''){ ?>
                                <th>Registros</th>
                                <th>%</th>
                                <?php }else{ ?>                                
                                    <th>+(Secciones)</th>
                                    <?php if($_SESSION['id_tipo_usuario']==1){ ?>
                                        <th>Acciones</th>
                                    <?php } ?>
                                <?php }?>
                            </tr>
                        </thead>
                        <tbody>
                        	<?php
                        	for($i=0;$i<count($data);$i++){    
                        	echo '<tr>';
                        	echo '<th scope="row">'.($i+1).'</th>';
                        	echo '<td>'.$data[$i]['nombreUsuario'].'</td>';
                        	echo '<td>'.$data[$i]['emailUsuario'].'</td>';
                        	echo '<td>'.$data[$i]['telefono'].'</td>';
                        	echo '<td>'.$helper->nivelUsuario($data[$i]['tipoUsuario']).'</td>';
                        if(isset($_GET['filtro'])&&$_SESSION['accion']!=''){
                            $porcentaje = $helper->porcentajePadron($totalRegistros, $data[$i]['totalRegistros']);
                            echo '<td>'.$data[$i]['totalRegistros'].'</td>';
                            if($porcentaje==0){
                                echo '<td><strong class="text-danger">'.round($porcentaje,2).' %</strong></td>';
                            }else{
                                echo '<td><strong>'.round($porcentaje,2).' %</strong></td>';
                            }
                        }else{                            
                            if($data[$i]['id_tipo_usuario']=='1'||$data[$i]['id_tipo_usuario']=='3'){
                            echo '<td><a href="asignar-secciones.php?id_usuario='.$data[$i]['id_usuario'].'" class="text-danger"><i class="mdi mdi-plus font-18 align-middle mr-2"></i><b>Asignar </b></a></td>';
                            }else{
                            echo '<td><a href="definir-promotor.php?id_usuario='.$data[$i]['id_usuario'].'" class="text-warning"><i class="mdi mdi-plus font-18 align-middle mr-2"></i><b>Asignar </b></a></td>';
                            }
                            if($_SESSION['id_tipo_usuario']==1&&$_SESSION['id_usuario']!=$data[$i]['id_usuario']){
                                    echo '<td><a href="'.$data[$i]['id_usuario'].'" class="sweet-confirm"><i class="fa fa-minus-circle text-danger"></i></a></td>';
                            }else{
                                    echo '<td><a href="javascript:void(0)"><i class="fa fa-minus-circle text-muted"></i></a></td>';
                            }
                        	echo '</tr>';
                        	}
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>	
</div>
    <script type="text/javascript">

jQuery(document).ready(function(){
    jQuery('a.sweet-confirm').each(function(){      
        jQuery(this).bind('click', function(e){         
            obj = jQuery(this);
            e.preventDefault();
                    swal({
                        title: "Realmente deseas eliminar el registro?",
                        text: "Esta accion eliminara el registro por completo",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Eliminar",
                        closeOnConfirm: false
                    },
                    function(){
                        swal("eliminado !!", "El registro se ha eliminado", "success");
                        obj.parent().parent().fadeOut();
                        eliminarRegistro(obj.attr("href"));
                    });
        });

    });
});

function eliminarRegistro(id_usuario)
{    
    var pagina = 'functions/eliminar-usuario.php';
    var datos = 'id_usuario='+id_usuario+'&rand='+Math.random()*99999;

    jQuery.ajax({
        type : 'post',
        url : pagina,
        data : datos,
        success : function(data){
            // dato eliminado
            // alert(data);
        }
    });
}

        
    </script>