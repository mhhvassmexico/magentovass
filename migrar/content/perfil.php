<?php global $select, $helper; ?>
<?php $data = $select->DatosUsuario(); ?>
<!-- End header header -->
<?php $helper->Msg(); ?>
<div class="container-fluid">
    <!-- Start Page Content -->
    <div class="row">
        <div class="col-lg-12">
            <div id="invoice" class="effect2" style="margin: 0 !important;">
                <div id="invoice-top">
                                                   
                    <!--End Info-->
                    <div class="title">
                        <h4>Folio <?php  echo $data['tipoUsuario'];?> #<?php echo $data['id_usuario']; ?></h4>
                        <p>Fecha de Registro: <?php echo $data['fecha_registro'];?><br> Ultimo Acceso: <?php echo $data['fecha_registro'];?>
                        </p>
                    </div>
                    <!--End Title-->
                </div>
                <!--End InvoiceTop-->
                <div id="invoice-mid">

                    <div class="clientlogo"></div>
                    <div class="invoice-info">
                        <h2><strong><?php echo $data['nombreUsuario'].' '.$data['apellidoPaternoUsuario'].' '.$data['apellidoMaternoUsuario'];?></strong></h2>
                        <p><?php echo $data['emailUsuario'];?><br> <?php echo $data['telefono'];?>
                            <br>
                    </div>

                    <div id="project">
                        <h2>Programa</h2>
                        <p>Afiliado al programa de registro ciudadano, para contabilizar y estimar un porcentaje de posibilida de padron electoral.</p>
                    </div>

                </div>
                <!--End Invoice Mid-->
                <div id="invoice-bot" style="min-height: 40px !important;">
                    <div id="legalcopy">
                        <p class="legal"><strong>Movilizador!</strong> Programa de registro y control ciudadano.
                        </p>
                    </div>

                </div>                    
                <!--End InvoiceBot-->
            </div>
            <!--End Invoice-->
        </div>
    </div>

                <div class="row justify-content-center">
                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-validation">
                                    <form class="form-valide" action="functions/cambiar-contrasena.php" method="post">

                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="val-password">Contraseña <span class="text-danger">*</span></label>
                                            <div class="col-lg-6">
                                                <input type="password" class="form-control" id="val-password" name="val-password" placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="val-confirm-password">confirmar Contraseña <span class="text-danger">*</span></label>
                                            <div class="col-lg-6">
                                                <input type="password" class="form-control" id="val-confirm-password" name="val-confirm-password" placeholder="">
                                            </div>
                                        </div>
                                      
                                        <div class="form-group row">
                                            <div class="col-lg-8 ml-auto">
                                                <button type="submit" class="btn btn-primary">Cambiar contraseña</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

    <!-- End PAge Content -->
</div>