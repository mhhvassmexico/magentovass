<?php global $select, $helper; ?>
<?php $data = $select->TableRegistrosUsuariosTodos(); ?>
<!-- Container fluid  -->
<div class="container-fluid">
    <!-- Start Page Content -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Lista de Captura</h4>
                    <h6 class="card-subtitle">Exportar a los formatos, CSV, Excel, PDF & Print</h6>
                    <div class="table-responsive m-t-40">
                        <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                	<th>Registro</th>
                                    <th>Nombre</th>
                                    <th>Apellido</th>
                                    <th>Municipio</th>
                                    <th>Colonia</th>
                                    <th>Calle</th>
                                    <th>FechaRegistro</th>
                                    <th>Seccion</th>
                                <?php if($_SESSION['id_tipo_usuario']==1&&$_SESSION['accion']!=''){ ?>
                                    <th>Acciones</th>
                                <?php } ?>                                    
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                	<th>Registro</th>
                                    <th>Nombre</th>
                                    <th>Apellido</th>
                                    <th>Municipio</th>
                                    <th>Colonia</th>
                                    <th>Calle</th>
                                    <th>FechaRegistro</th>
                                    <th>Seccion</th>
                                <?php if($_SESSION['id_tipo_usuario']==1&&$_SESSION['accion']!=''){ ?>
                                    <th>Acciones</th>
                                <?php } ?>                                    
                                </tr>
                            </tfoot>
                            <tbody>
                            	<?php 
                            	for($i=0;$i<count($data);$i++){
	                            	echo '<tr>';
	                            	echo '<td>('.$data[$i]['nombreUsuario'].' '.$data[$i]['apellidoPaternoUsuario'].' '.$data[$i]['apellidoMaternoUsuario'].')</td>';
	                            	echo '<td>'.$data[$i]['nombre'].'</td>';
	                            	echo '<td>'.$data[$i]['apellido_paterno'].' '.$data[$i]['apellido_materno'].'</td>';	                            	
	                            	echo '<td>'.$data[$i]['municipio'].'</td>';
	                            	echo '<td>'.$data[$i]['colonia'].'</td>';
	                            	echo '<td>'.$data[$i]['calle'].'</td>';
	                            	echo '<td>'.$data[$i]['fecha_registro'].'</td>';
	                            	echo '<td>'.$data[$i]['seccion'].'</td>';
                                    if($_SESSION['id_tipo_usuario']==1&&$_SESSION['accion']!=''){
                                    echo '<td>';
                                        echo '<a href="editar-ciudadano.php?id_ciudadano='.$data[$i]['id_ciudadano'].'"><i class="fa fa-edit text-success"></i></a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;';                                 
                                        echo '<a href="javascript:void(0)" class="sweet-confirm" onclick="eliminarRegistroCiudadano('.$data[$i]['id_ciudadano'].', this)"><i class="fa fa-minus-circle text-danger"></i></a>';
                                    echo '</td>';
                                    }                                    
	                            	echo '</tr>';
                            	}
                            	?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- End PAge Content -->
</div>
<!-- End Container fluid  -->
    <script type="text/javascript">

function eliminarRegistroCiudadano(id_ciudadano, obj)
{
    swal({
        title: "Realmente deseas eliminar el registro?",
        text: "Esta accion eliminara el registro por completo",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Eliminar",
        closeOnConfirm: false
    },
    function(){
        swal("eliminado !!", "El registro se ha eliminado", "success");
        $(obj).parent().parent().fadeOut();
        eliminarRegistro(id_ciudadano);
    });
}


jQuery(document).ready(function(){
    jQuery('a.sweet-confirm').each(function(){      
        jQuery(this).bind('click', function(e){         
            obj = jQuery(this);
            e.preventDefault();
                    swal({
                        title: "Realmente deseas eliminar el registro?",
                        text: "Esta accion eliminara el registro por completo",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Eliminar",
                        closeOnConfirm: false
                    },
                    function(){
                        swal("eliminado !!", "El registro se ha eliminado", "success");
                        obj.parent().parent().fadeOut();
                        eliminarRegistro(obj.attr("href"));
                    });
        });

    });
});

function eliminarRegistro(id_ciudadano)
{
    console.log(id_ciudadano);
    var pagina = 'functions/eliminar-ciudadano.php';
    var datos = 'id_ciudadano='+id_ciudadano+'&rand='+Math.random()*99999;
    console.log(datos);
    jQuery.ajax({
        type : 'post',
        url : pagina,
        data : datos,
        success : function(data){
            // dato eliminado
            //alert(data);
        }
    });
}

        
    </script>