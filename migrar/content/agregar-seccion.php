<?php global $select, $helper; ?>
<div class="container-fluid">
    <!-- Start Page Content -->
    <div class="row justify-content-center">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-body">
                    <div class="form-validation">
                        <form class="form-valide" action="functions/registra-seccion.php" method="post">
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label">Nombre Sección </label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" name="val-nombre_seccion" placeholder="Introduce un nombre de sección..">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label">Descripción </label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" name="val-descripcion" placeholder="Describe la sección">
                                </div>
                            </div>                            
                            <hr class="m-t-0 m-b-40"> 
                            <div class="form-group row">
                                <div class="col-lg-8 ml-auto">
                                    <button type="submit" class="btn btn-primary">Agregar Sección</button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- End PAge Content -->
</div>