<?php global $select, $helper; ?>
<div class="container-fluid">
    <!-- Start Page Content -->
    <div class="row justify-content-center">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-body">
                    <div class="form-validation">
                        <form class="form-valide" action="functions/registra-ciudadano.php" method="post">
                            <?php if($_SESSION['id_tipo_usuario']==2){?>
                            <div class="form-group row">                                
                                <label class="col-lg-4 col-form-label">Movilizador</label>
                                <div class="col-lg-6">
                                    <select class="form-control custom-select chosen-select" name="val-movilizador" id="movilizador">
                                        <?php echo $select->selectMovilizadores(); ?>                                        
                                    </select>
                                </div>    
                            </div>
                            <?php }?>                            
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label">Nombre <span class="text-danger">*</span></label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" name="val-nombre" id="val-nombre" placeholder="Introduce un nombre.." onkeyup="javascript:this.value=this.value.toUpperCase();">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label">Apellido Paterno <span class="text-danger">*</span></label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" name="val-apellidop" id="val-apellidop" placeholder="Apellido paterno" onkeyup="javascript:this.value=this.value.toUpperCase();">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label">Apellido Materno <span class="text-danger">*</span></label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" name="val-apellidom" id="val-apellidom" placeholder="Apellido materno" onkeyup="javascript:this.value=this.value.toUpperCase();">
                                </div>
                            </div>
                            <div class="form-group row">                                
                                <label class="col-lg-4 col-form-label">Genero</label>
                                <div class="col-lg-6">
                                    <select class="form-control custom-select" name="val-genero">
                                        <option value="1">Hombre</option>
                                        <option value="2">Mujer</option>
                                    </select>
                                </div>    
                            </div>
                            <!--
                            <div class="form-group row">                                                                            
                                <label class="col-lg-4 col-form-label">Fecha de Nacimiento</label>
                                <div class="col-lg-6">
                                    <input type="date" class="form-control" placeholder="Día/Mes/Año" name="val-fecha_nacimiento">
                                </div>
                            </div>
                        -->
                        <!---
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label">Email </label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" id="val-email" name="val-email" placeholder="Correo electronico">
                                </div>
                            </div>  
                        -->
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label">Telefono <span class="text-danger">*</span></label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" id="val-telefono" name="val-telefono" placeholder="Telefono">
                                </div>
                            </div>                                                        
                            <h3 class="box-title">Dirección</h3>
                            <hr class="m-t-0 m-b-40">            
                            <div class="form-group row">                                
                                <label class="col-lg-4 col-form-label">Estado</label>
                                <div class="col-lg-6">
                                    <select class="form-control custom-select" name="val-estado">
                                        <option value="1">México</option>                                        
                                    </select>
                                </div>    
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label">Municipio </label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" name="val-municipio" placeholder="Municipio">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label">Colonia/Barrio </label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" name="val-colonia" placeholder="Colonia o Barrio">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label">Calle </label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" name="val-calle" placeholder="Calle">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label">Numero </label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" name="val-numero" placeholder="Numero">
                                </div>
                            </div>
                            <h3 class="box-title">Sección</h3>
                            <div class="form-group row">                                
                                <label class="col-lg-4 col-form-label">Sección</label>
                                <?php if($_SESSION['id_tipo_usuario']==2){ ?>
                                    <div class="col-lg-6" id="secciones">
                                         <select class="form-control custom-select" name="val-seccion">
                                            <?php // echo $select->selectSecciones(); ?>                                        
                                        </select>
                                    </div>    
                                <?php }else{ ?>
                                    <div class="col-lg-6" id="secciones-admin">
                                         <select class="form-control custom-select" name="val-seccion">
                                            <?php echo $select->selectSecciones(); ?>                                        
                                        </select>
                                    </div>    
                                <?php } ?>
                            </div>
                            <hr class="m-t-0 m-b-40"> 
                            <input type="hidden" name="val-casilla" value="0">
                            <input type="hidden" name="val-no_padron" value="0">
                           <?php /*
                            <div class="form-group row">                                
                                <label class="col-lg-4 col-form-label">Casilla</label>
                                <div class="col-lg-6">
                                    <select class="form-control custom-select" name="val-casilla">
                                        <?php echo $select->selectCasillas(); ?>                                        
                                    </select>
                                </div>    
                            </div>                    
                            <hr class="m-t-0 m-b-40"> 
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label">No Padron </label>
                                <div class="col-lg-6">                                    
                                    <textarea class="form-control" name="val-no_padron" placeholder="No Padron"></textarea>
                                </div>
                            </div>                            
                            <hr class="m-t-0 m-b-40"> 
                            */ ?>        
                            <div class="form-group row">
                                <div class="col-lg-8 ml-auto">
                                    <button type="submit" class="btn btn-primary">Registrar Ciudadano</button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- End PAge Content -->
</div>
<script type="text/javascript">
    jQuery(document).ready(function(){
        // event Movilizador
        jQuery('#movilizador').change(function(){
            var id_movilizador = jQuery(this).val();
            var pagina = 'functions/obtener-secciones-movilizador.php';
            var datos = 'id_movilizador='+id_movilizador+'&rand='+Math.random()*9999999;

            jQuery.ajax({
                type : 'post',
                url : pagina,
                data : datos,
                success : function(data){                    
                    jQuery('#secciones').html(data);
                }
            });

        });




        var objNombre = jQuery('#val-nombre');
        var objApellidop = jQuery('#val-apellidop');
        var objApellidom = jQuery('#val-apellidom');

        objNombre.blur(function(){
            if(objNombre.val()!=''&&objApellidop.val()!=''&&objApellidom.val()!=''){
                validaNombreExistencia(objNombre, objApellidop, objApellidom);
            }
        });

        objApellidop.blur(function(){
            if(objNombre.val()!=''&&objApellidop.val()!=''&&objApellidom.val()!=''){
                validaNombreExistencia(objNombre, objApellidop, objApellidom);
            }
        });

        objApellidom.blur(function(){
            if(objNombre.val()!=''&&objApellidop.val()!=''&&objApellidom.val()!=''){
                validaNombreExistencia(objNombre, objApellidop, objApellidom);
            }
        });
    });

    function validaNombreExistencia(objNombre, objApellidop, objApellidom)
    {
        var pagina = 'functions/valida-existe-ciudadano.php';
        var datos = 'nombre='+objNombre.val()+'&apellido_paterno='+objApellidop.val()+'&apellido_materno='+objApellidom.val()+'&rand='+Math.random()*9999999;
        jQuery.ajax({
            type : 'post',
            url : pagina,
            data : datos,
            success : function(data){
                if(data==1){                    
            objApellidom.val('');
            objApellidom.focus();
    toastr.error('Ya no es posible registrar este ciudadano nuevamente','Ciudadano Existenet',{
        "positionClass": "toast-top-full-width",
        timeOut: 5000,
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
        "tapToDismiss": false

    })




                }
            }
        });
    }
</script>