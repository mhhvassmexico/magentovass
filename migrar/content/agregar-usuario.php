<?php global $select, $helper; ?>
<div class="container-fluid">
    <!-- Start Page Content -->
    <div class="row justify-content-center">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-body">
                    <div class="form-validation">
                        <form class="form-valide" action="functions/registra-usuario.php" method="post">
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label">Nombre <span class="text-danger">*</span></label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" name="val-nombre" placeholder="Introduce un nombre.." onkeyup="javascript:this.value=this.value.toUpperCase();">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label">Apellido Paterno <span class="text-danger">*</span></label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" name="val-apellidop" placeholder="Apellido paterno" onkeyup="javascript:this.value=this.value.toUpperCase();">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label">Apellido Materno <span class="text-danger">*</span></label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" name="val-apellidom" placeholder="Apellido materno" onkeyup="javascript:this.value=this.value.toUpperCase();">
                                </div>
                            </div>
                            <div class="form-group row">                                
                                <label class="col-lg-4 col-form-label">Genero</label>
                                <div class="col-lg-6">
                                    <select class="form-control custom-select" name="val-genero">
                                        <option value="1">Hombre</option>
                                        <option value="2">Mujer</option>
                                    </select>
                                </div>    
                            </div>
                            <!--
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label">Edad </label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" name="val-edad" placeholder="Edad">
                                </div>
                            </div>
							<div class="form-group row">
                                <label class="col-lg-4 col-form-label">Telefono </label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" name="val-telefono" placeholder="Telefono">
                                </div>
                            </div>                            
                            -->
                            <!--
                            <div class="form-group row">                                                                            
                                <label class="col-lg-4 col-form-label">Fecha de Nacimiento</label>
                                <div class="col-lg-6">
                                    <input type="date" class="form-control" placeholder="Día/Mes/Año" name="val-fecha_nacimiento">
                                </div>
                            </div>
                        -->
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label">Email <span class="text-danger">*</span></label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" id="val-email" name="val-email" placeholder="Correo electronico">
                                </div>
                            </div>                            
                            <h3 class="box-title">Datos de Acceso</h3>
                            <hr class="m-t-0 m-b-40">            
                            <div class="form-group row">                                
                                <label class="col-lg-4 col-form-label">Tipo Usuario</label>
                                <div class="col-lg-6">
                                    <select class="form-control custom-select" name="val-tipo_usuario">
                                        <?php echo $select->selectTiposUsuarios(); ?>                                        
                                    </select>
                                </div>    
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label">Usuario/Email </label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" name="val-usuario-email" id="val-usuario-email" placeholder="Usuario">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label">Contraseña </label>
                                <div class="col-lg-6">
                                    <input type="password" class="form-control" name="val-contrasena">
                                </div>
                            </div>                            
                            <hr class="m-t-0 m-b-40"> 
                            <div class="form-group row">
                                <div class="col-lg-8 ml-auto">
                                    <button type="submit" class="btn btn-primary">Registrar Usuario</button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- End PAge Content -->
</div>
<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery('#val-email').blur(function(){
			var email = jQuery(this).val();
			jQuery('#val-usuario-email').val(email);
		});
	});
</script>