<?php global $select, $helper; ?>
<?php $data = $select->TableRegistrosTiposUsuarios(); ?>
<?php $nombre = implode(" ",$select->NombreMovilizador()); ?>
<style type="text/css">
    #example23_wrapper > .dt-buttons{
        display: none;
    }
</style>
<!-- Container fluid  -->
<div class="container-fluid">
    <!-- Start Page Content -->
    <div class="row justify-content-center">
        <div class="col-6">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Listado de Movilizadores <button type="button" class="btn btn-info btn-flat m-b-10 m-l-5 volver" style="float: right;">Volver</button></h4>
                    <div class="clear"></div><br>
                    <div class="clear"></div>
                    <div class="alert alert-secondary"><span class="text-muted">Asignar movilizador a: </span><strong><?php echo $nombre;?></strong></div>
                    <div class="table-responsive m-t-40">
                        <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Nombre Movilizador</th>
                                    <th>Tipo Usuario</th>                                    
                                    <?php if($_SESSION['id_tipo_usuario']==1){ ?>
                                    <th>Acciones</th>
                                    <?php } ?>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Nombre Movilizador</th>
                                    <th>Tipo Usuario</th>                                  
                                    <?php if($_SESSION['id_tipo_usuario']==1){ ?>
                                    <th>Acciones</th>
                                    <?php } ?>
                                </tr>
                            </tfoot>
                            <tbody>
                                <?php 
                                for($i=0;$i<count($data);$i++){
                                    echo '<tr>';
                                    echo '<td>'.$data[$i]['nombre'].' '.$data[$i]['apellido_paterno'].' '.$data[$i]['apellido_materno'].'</td>';
                                    echo '<td>'.$data[$i]['tipoUsuario'].'</td>';
                                    if($_SESSION['id_tipo_usuario']==1){
                                    echo '<td><input type="checkbox" name="listaSeccion" title="'.$_GET['id_usuario'].'" class="box-seccion" onclick="asignaUsuarioCapturista('.$data[$i]['id_usuario'].', this)"';

                                    if($data[$i]['id']>0){
                                        echo 'checked';
                                    }
                                    echo '></td>';
                                    }
                                    echo '</tr>';
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- End PAge Content -->
</div>
<!-- End Container fluid  -->
    <script type="text/javascript">

jQuery(document).ready(function(){
    jQuery('.volver').bind('click', function(){
        location.href = "mostrar-usuarios.php";
    });
});

function asignaUsuarioCapturista(id_usuario, obj)
{
    var active = $(obj).is(":checked");    
    var id_capturista = $.get("id_usuario");
    var pagina = 'functions/asignar-usuario-capturista.php';
    var datos = 'active='+active+'&id_usuario='+id_usuario+'&id_capturista='+id_capturista+'&rand='+Math.random()*99999;

    jQuery.ajax({
        type : 'post',
        url : pagina,
        data : datos,
        success : function(data){
            // dato eliminado
            if(data=='1'){
                    toastr.success('La seccion fue asignada','Sección asignada',{
                        "positionClass": "toast-top-center",
                        timeOut: 5000,
                        "closeButton": true,
                        "debug": false,
                        "newestOnTop": true,
                        "progressBar": true,
                        "preventDuplicates": true,
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut",
                        "tapToDismiss": false
                    })

            }else{
                    toastr.error('La seccion fue eliminada','Sección eliminada',{
                        "positionClass": "toast-top-center",
                        timeOut: 5000,
                        "closeButton": true,
                        "debug": false,
                        "newestOnTop": true,
                        "progressBar": true,
                        "preventDuplicates": true,
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut",
                        "tapToDismiss": false
                    })

            }            
        }
    });

}


(function($) {  
    $.get = function(key)   {  
        key = key.replace(/[\[]/, '\\[');  
        key = key.replace(/[\]]/, '\\]');  
        var pattern = "[\\?&]" + key + "=([^&#]*)";  
        var regex = new RegExp(pattern);  
        var url = unescape(window.location.href);  
        var results = regex.exec(url);  
        if (results === null) {  
            return null;  
        } else {  
            return results[1];  
        }  
    }  
})(jQuery);

        
    </script>    