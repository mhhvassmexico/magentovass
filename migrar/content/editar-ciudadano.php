<?php global $select, $helper; ?>
<?php $data = $select->detalleciudadano(); ?>
<?php 
/*
echo "<pre>";
print_r($data);
echo "</pre>";
*/
?>
<div class="row">
	<div class="col-lg-12">
		<div class="card card-outline-info">
            <div class="card-header">
                <h4 class="m-b-0 text-white">Edición Ciudadano</h4>
            </div>
            <div class="card-body">
            	<div class="form-body">
            		<h3 class="box-title m-t-15">Datos Ciudadano</h3>
                    <hr class="m-t-0 m-b-40">
            	</div>

            	<div class="row">
                    <div class="col-md-12">
                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">Nombre Completo</label>
                            <div class="col-md-9">
                                <strong><?php echo $data[0]['nombre']." ".$data[0]['apellido_paterno']." ".$data[0]['apellido_materno'];?></strong>
                            </div>
                        </div>
                    </div>            		
            	</div>
            	<div class="row">
                    <div class="col-md-12">
                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">Genero</label>
                            <div class="col-md-9">
                            	<strong>
                                <?php
                                if($data[0]['sexo']==1){
                                	echo "HOMBRE";
                                }else{
                                	echo "MUJER";
                                }
                                ?>      
                                </strong>
                            </div>
                        </div>
                    </div>            		
            	</div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">Telefono</label>
                            <div class="col-md-9">
                                <strong><?php echo $data[0]['telefono'];?></strong>
                            </div>
                        </div>
                    </div>
                </div>                
            </div>	
            <h3 class="box-title m-t-15">Direccion Ciudadano</h3>
            <hr class="m-t-0 m-b-40">                
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Estado</label>
                                    <div class="col-md-9">
                                        <strong>México</strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <!--/span-->
                            <div class="col-md-12">
                                <div class="form-group has-danger row">
                                    <label class="control-label text-right col-md-3">Municipio</label>
                                    <div class="col-md-9">
                                    	<strong>
                                        <?php echo $data[0]['municipio'];?>
                                        </strong>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Colonia/Barrio</label>
                                    <div class="col-md-9">
                                        <strong><?php echo $data[0]['colonia'];?></strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/row-->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Calle</label>
                                    <div class="col-md-9">
                                        <strong><?php echo $data[0]['calle'];?></strong>
                                    </div>
                                </div>
                            </div>
                        </div>        
                                                                <!--/row-->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Numero</label>
                                    <div class="col-md-9">
                                        <strong><?php echo $data[0]['numero'];?></strong>
                                    </div>
                                </div>
                            </div>
                        </div>   
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Sección</label>
                                    <div class="col-md-9">
                                        <strong><?php echo $data[0]['seccion'];?></strong>
                                	</div>
                            	</div>
                        	</div>                        
                        </div>
            <hr>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn btn-success">Guardar Información</button>
                                <button type="button" class="btn btn-inverse cancelar">Cancelar</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6"> </div>
                </div>
            </div>            	
		</div>
	</div>	
</div>
<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery('.cancelar').bind('click', function(){
			location.href = "mostrar-todos.php"
		});
	});
</script>