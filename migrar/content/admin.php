<?php 
    global $select, $helper; 
    $totalRegistros = $select->TotalRegistroUsuarioGlobal();
    $totalUsuario = $select->TotalRegistroUsuario();
    $porcentaje = $helper->porcentajePadron($totalRegistros, $totalUsuario);
?>
<div class="row">
    <div class="col-md-3">
        <div class="card p-30">
            <div class="media">
                <div class="media-left meida media-middle">
                    <span><i class="fa fa-users f-s-40 color-primary"></i></span>
                </div>
                <div class="media-body media-text-right">
                    <h2><?php echo $totalUsuario; ?></h2>
                    <p class="m-b-0">Total Capturados</p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card p-30">
            <div class="media">
                <div class="media-left meida media-middle">
                    <span><i class="fa fa-users f-s-40 color-success"></i></span>
                </div>
                <div class="media-body media-text-right">
                    <h2><?php echo $select->TotalRegistroUsuario(); ?></h2>
                    <p class="m-b-0">Esta Semana</p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card p-30">
            <div class="media">
                <div class="media-left meida media-middle">
                    <span><i class="fa fa-users f-s-40 color-warning"></i></span>
                </div>
                <div class="media-body media-text-right">
                    <h2><?php echo $select->TotalRegistroUsuario(); ?></h2>
                    <p class="m-b-0">Hoy</p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card p-30">
            <div class="media">
                <div class="media-left meida media-middle">
                    <span><i class="fa fa-users f-s-40 color-danger"></i></span>
                </div>
                <div class="media-body media-text-right">
                    <h2><?php echo $totalRegistros; ?></h2>
                    <p class="m-b-0">Total Global</p>
                </div>
            </div>
        </div>
    </div>
    <!-- Column -->
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="card-two">
                    <header>
                        <div class="avatar">
                            <img src="images/users/profile.png" alt="Allison Walker" />
                        </div>
                    </header>

                    <h3><?php echo $porcentaje; ?> %</h3>
                    <div class="desc">
                        Usted representa el <?php echo $porcentaje; ?> % de registros del Total Global Capturados
                    </div>                                
                </div>
            </div>
        </div>
    </div>
    <!-- Column -->
</div>