<?php global $select, $helper; ?>
<?php $data = $select->TableRegistrosCasillasTodos(); ?>
<!-- Container fluid  -->
<div class="container-fluid">
    <!-- Start Page Content -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Lista de Casillas</h4>
                    <h6 class="card-subtitle">Exportar a los formatos, CSV, Excel, PDF & Print</h6>
                    <div class="table-responsive m-t-40">
                        <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                	<th>#</th>
                                    <th>Nombre</th>
                                    <th>Descripción</th>                                    
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                	<th>#</th>
                                    <th>Nombre</th>
                                    <th>Descripción</th>
                                    <th>Acciones</th>
                                </tr>
                            </tfoot>
                            <tbody>
                            	<?php 
                            	for($i=0;$i<count($data);$i++){
	                            	echo '<tr>';
	                            	echo '<td>'.($i+1).'</td>';
	                            	echo '<td>'.$data[$i]['nombre'].'</td>';
	                            	echo '<td>'.utf8_encode($data[$i]['descripcion']).'</td>';
	                            	echo '<td>';
	                            	echo '<a href="'.$data[$i]['id_casilla'].'" class="sweet-confirm"><i class="fa fa-minus-circle text-danger"></i></a>';
	                            	echo '</td>';
	                            	echo '</tr>';
                            	}
                            	?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- End PAge Content -->
</div>
<!-- End Container fluid  -->
    <script type="text/javascript">

jQuery(document).ready(function(){
	jQuery('a.sweet-confirm').each(function(){		
		jQuery(this).bind('click', function(e){			
			obj = jQuery(this);
			e.preventDefault();
					swal({
			            title: "Realmente deseas eliminar el registro?",
			            text: "Esta accion eliminara el registro por completo",
			            type: "warning",
			            showCancelButton: true,
			            confirmButtonColor: "#DD6B55",
			            confirmButtonText: "Eliminar",
			            closeOnConfirm: false
			        },
			        function(){
			            swal("eliminado !!", "El registro se ha eliminado", "success");
			            obj.parent().parent().fadeOut();
                        eliminarRegistro(obj.attr("href"));
			        });
		});

	});
});

function eliminarRegistro(id_casilla)
{
    var pagina = 'functions/eliminar-casilla.php';
    var datos = 'id_casilla='+id_casilla+'&rand='+Math.random()*99999;

    jQuery.ajax({
        type : 'post',
        url : pagina,
        data : datos,
        success : function(data){
            // dato eliminado
        }
    });
}

        
    </script>