<?php

class query
{
	public function __construct()
	{

	}
	
	public function validaAcceso()
	{
		/*
		$sql = "select a.id_usuario, a.usuario, a.estado, u.nombre, u.apellido_paterno, u.apellido_materno, u.sexo, u.edad, u.email, tu.nombre nombre_tipo, tu.descripcion, tu.id_tipo_usuario
		          from acceso a,
		               usuarios u,
		               tipos_usuarios tu
		         where a.id_usuario = u.id_usuario
		           and a.id_tipo_usuario = tu.id_tipo_usuario
		           and a.usuario = :usuario
		           and a.contrasena = :contrasena";
		*/
		$sql = "select a.id_usuario, a.usuario, a.estado, u.nombre, u.apellido_paterno, u.apellido_materno, u.sexo, u.edad, u.email, tu.nombre nombre_tipo, tu.descripcion, tu.id_tipo_usuario, ifnull(p.action, '') accion
		          from acceso a
            inner join usuarios u on(a.id_usuario = u.id_usuario)
            inner join tipos_usuarios tu on(a.id_tipo_usuario = tu.id_tipo_usuario)            		               
             left join privilegios p on(p.id_usuario = a.id_usuario)
		         where a.usuario = :usuario
		           and a.contrasena = :contrasena";

		return $sql;
	}

	public function registraAcceso()
	{
		$sql = "INSERT INTO acceso(id_usuario, id_tipo_usuario, usuario, contrasena, estado) VALUES(:id_usuario, :id_tipo_usuario, :usuario, :contrasena, :estado)";
		return $sql;
	}

	public function registraUsuario()
	{
		$sql = "INSERT INTO usuarios(nombre, apellido_paterno, apellido_materno, sexo, edad, email, telefono) VALUES(:nombre, :apellido_paterno, :apellido_materno, :sexo, :edad, :email, :telefono)";
		return $sql;
	}

	public function registraCapturista()
	{
		$sql = "INSERT INTO registro_capturista(id_registro, id_usuario) VALUES(:id_registro, :id_usuario)";
		return $sql;
	}

	public function registraNoPadron()
	{
		$sql = "INSERT INTO padron(id_registro, no_padron) VALUES(:id_registro, :no_padron)";
		return $sql;
	}

	public function registraCiudadano()
	{
		$sql = "INSERT INTO ciudadanos(nombre, apellido_paterno, apellido_materno, sexo, telefono) VALUES(:nombre, :apellido_paterno, :apellido_materno, :sexo, :telefono)";
		return $sql;
	}

	public function registraDireccion()
	{
		$sql = "INSERT INTO direccion_ciudadano(id_estado, calle, numero, colonia, municipio) VALUES(:id_estado, :calle, :numero, :colonia, :municipio)";
		return $sql;
	}

	public function registraRegistro()
	{
		$sql = "INSERT INTO registro_ciudadano(id_ciudadano, id_direccion, id_seccion, id_usuario, id_casilla) VALUES(:id_ciudadano, :id_direccion, :id_seccion, :id_usuario, :id_casilla)";
		return $sql;
	}
}