<?php

class data
{
	public function __construct()
	{

	}

	public function pagina()
	{
		$pagina = array_pop(explode('/', $_SERVER['PHP_SELF']));
		$path = '';
		if(file_exists('content/'.$pagina)){			
			require_once("content/".$pagina);
		}else{			
			$this->createPage($pagina);
		}				
	}

	private function createPage($page)
	{
		$page = fopen('content/'.$page, "w+");
		fwrite($page, '<div class="row"></div>');
		fclose($page);
	}

	public function porcentajePadron($totalGeneral, $totalUsuario)
	{
		return ($totalUsuario * 100) / $totalGeneral;
	}

	public function nivelUsuario($dato)
	{
		$s = '';
		switch($dato){
			case 'Administrador':
			$s .= '<span class="badge badge-primary">'.$dato.'</span>';
			break;
			case 'Movilizador':
			$s .= '<span class="badge badge-success">'.$dato.'</span>';
			break;
			case 'Capturista':
			$s .= '<span class="badge badge-danger">'.$dato.'</span>';
			break;
		}
		return $s;
	}

	public function Msg()
	{
		if(isset($_GET['msg'])&&$_GET['msg']!=''){
			echo '<script type="text/javascript">jQuery(document).ready(function(){setTimeout(function(){ jQuery(".alert.alert-success").fadeOut() },4000)});</script>';
			echo '<div class="alert alert-success">';
			echo '    '.$_GET['msg'];
			echo '</div>';
		}
	}
}