<?php
class Session
{
	private $_data = array();

	public function __construct()
	{
		session_start();
	}

	public function session($row)
	{				
		foreach($row as $item => $value)
		{
			$_SESSION[$item] = $value;			
		}		
	}	
}