<?php

class Redirect
{
	public function __construct()
	{

	}

	public function Location($page, $msg='')
	{
		switch($page)
		{
			case 'Admin':
			//echo $this->curPageURL()."admin.php";
			header("Location: ".$this->curPageURL()."admin.php".$this->msg($msg));
			break;
			case 'Login':			
			header("Location: ".$this->curPageURL()."index.php".$this->msg($msg));
			break;
			case 'AgregarSeccion':
			header("Location: ".$this->curPageURL()."agregar-seccion.php".$this->msg($msg));
			break;
			case 'AgregarCasilla':
			header("Location: ".$this->curPageURL()."agregar-casilla.php".$this->msg($msg));
			break;			
			case 'Perfil':
			header("Location: ".$this->curPageURL()."perfil.php".$this->msg($msg));
			break;
		}
	}	

	public function msg($msg)
	{
		$s = '';
		if($msg!=''){
			$s .= '?msg='.$msg;
		}
		return $s;
	}

	public function curPageURL() 
	{		
	$host  = $_SERVER['HTTP_HOST'];
	$path   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
	$pathC = substr($path, 1, strlen($path));//substr($path, 0, (strpos((substr($path, 1, strlen($path))), "/"))+1);

	//echo "[".$_SERVER['HTTP_HOST']."]";
	//echo "[".$_SERVER['PHP_SELF']."]";
/*
	if(strlen($pathC)>10){
		$pathC = substr($path, 0, (strpos((substr($path, 1, strlen($path))), "/"))+1);
	}else{
		$pathC = "/".substr($path, 1, strlen($path));
	}	
	*/
	$baseurl = "http://" . $host . "/";
	return $baseurl;
	}
}