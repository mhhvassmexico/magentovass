<?php
include '../include/config.php';
include '../include/query.php';
include '../class/Session.php';
include '../class/Redirect.php';

$cn = new connection();

if(isset($_POST['valida_acceso'])&&$_POST['valida_acceso']==1){
	// Redirect
	$redirect = new Redirect();
	// Validamos el acceso
	$sql = new query();
	$sqlRes = $sql->validaAcceso();
	
	$cn->query($sqlRes);
	$cn->bind(':usuario',$_POST['email']);
	$cn->bind(':contrasena', MD5($_POST['contrasena']));
	// si es mayor a 0 generamos la session de usuario	
	$row = $cn->single();	
	
	if($cn->rowCount() > 0){
		// generar las sessiones
		$session = new Session();		
		$session->session($row);
		$redirect->Location('Admin');
	}else{
		$redirect->Location('Login');		
	}	
}
?>
