<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="images/favicon.ico" type="image/ico" />

    <title>Gentelella Alela! | </title>

    <!-- Bootstrap -->
    <link href="../../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
	
    <!-- bootstrap-progressbar -->
    <link href="../../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="../../vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="../../vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../../build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            <form>
              <h1>MAQUETAS</h1>
        
            </form>
          </section>


          <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>HTML</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <p>Paginas HTML Maquetadas</p>

                    <!-- start project list -->
                    <table class="table table-striped projects">
                      <thead>
                        <tr>
                          <th style="width: 1%">#</th>
                          <th style="width: 60%">Nombre</th>
                          <th>Link</th>                          
                        </tr>
                      </thead>
                      <tbody>                        
                        <tr>
                          <td>1</td>                          
                          <td>
                            Detalle del producto
                          </td>
                          <td>
                            <a href="detalle-producto.html" target="_blank" class="btn btn-primary btn-xs"><i class="fa fa-link"></i> Ver </a>                            
                          </td>
                        </tr>
                        <tr>
                          <td>2</td>                          
                          <td>
                            Detalle del SIM
                          </td>
                          <td>
                            <a href="detalle-sim.html" target="_blank" class="btn btn-primary btn-xs"><i class="fa fa-link"></i> Ver </a>                            
                          </td>
                        </tr>
                        <tr>
                          <td>3</td>                          
                          <td>
                            Malla Terminales
                          </td>
                          <td>
                            <a href="malla.html" target="_blank" class="btn btn-primary btn-xs"><i class="fa fa-link"></i> Ver </a>                            
                          </td>
                        </tr>
                        <tr>
                          <td>4</td>                          
                          <td>
                            Car Checkout
                          </td>
                          <td>
                            <a href="car-checkout.html" target="_blank" class="btn btn-primary btn-xs"><i class="fa fa-link"></i> Ver </a>                            
                          </td>
                        </tr>
                        <tr>
                          <td>5</td>                          
                          <td>
                            Car Comprar como Invitado
                          </td>
                          <td>
                            <a href="car-comprar-como-invitado.html" target="_blank" class="btn btn-primary btn-xs"><i class="fa fa-link"></i> Ver </a>
                          </td>
                        </tr>
                        <tr>
                          <td>6</td>                          
                          <td>
                            Car Datos de facturacion
                          </td>
                          <td>
                            <a href="car-datos-de-facturacion.html" target="_blank" class="btn btn-primary btn-xs"><i class="fa fa-link"></i> Ver </a>
                          </td>
                        </tr>
                        <tr>
                          <td>7</td>                          
                          <td>
                            Car Datos de pago
                          </td>
                          <td>
                            <a href="car-datos-de-pago.html" target="_blank" class="btn btn-primary btn-xs"><i class="fa fa-link"></i> Ver </a>
                          </td>
                        </tr>
                        <tr>
                          <td>8</td>                          
                          <td>
                            Car Ingresa tu contraseña
                          </td>
                          <td>
                            <a href="car-ingresa-tu-contrasena.html" target="_blank" class="btn btn-primary btn-xs"><i class="fa fa-link"></i> Ver </a>
                          </td>
                        </tr>
                        <tr>
                          <td>9</td>                          
                          <td>
                            Car Ingresa tus datos
                          </td>
                          <td>
                            <a href="car-ingresa-tus-datos.html" target="_blank" class="btn btn-primary btn-xs"><i class="fa fa-link"></i> Ver </a>
                          </td>
                        </tr>
                        <tr>
                          <td>9</td>                          
                          <td>
                            Car metodos de envio desplegado
                          </td>
                          <td>
                            <a href="car-metodos-de-envio-desplegado.html" target="_blank" class="btn btn-primary btn-xs"><i class="fa fa-link"></i> Ver </a>
                          </td>
                        </tr>
                        <tr>
                          <td>10</td>                          
                          <td>
                            Car metodos de envio
                          </td>
                          <td>
                            <a href="car-metodos-de-envio.html" target="_blank" class="btn btn-primary btn-xs"><i class="fa fa-link"></i> Ver </a>
                          </td>
                        </tr>
                        <tr>
                          <td>11</td>                          
                          <td>
                            Car registro
                          </td>
                          <td>
                            <a href="car-registro.html" target="_blank" class="btn btn-primary btn-xs"><i class="fa fa-link"></i> Ver </a>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                    <!-- end project list -->

                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>
        </div>    



      </div>





    </div>







  </body>
</html>
