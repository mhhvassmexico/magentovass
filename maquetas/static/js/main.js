const meses = ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
function obtenerFecha (fecha) {
  return `${fecha.getDate()} ${meses[fecha.getMonth()]}`
}
const optionCalendar = {
  locale: 'es',
  mode: 'range',
  closeOnSelect: false,
  plugins: [new confirmDatePlugin({
    confirmText: "Aplicar ",
    showAlways: true,
  })],
  showMonths: 2,
  onClose: function (selectedDates, dateStr, instance) {
    if(selectedDates[1]){
      const fecha = `${obtenerFecha(selectedDates[0])} - ${obtenerFecha(selectedDates[1])}`
      $('.js-date-picker').text(fecha)
    } else {
      this.clear()
    }
  }
}
$(document).ready(function () {
  flatpickr('.js-date-picker', optionCalendar)
})

/*------------------------------------*\
  Funcionalidad open filtro y select order
\*------------------------------------*/


$(".js-filtroBtn").click(function () {
  if ($(".js-filtro").hasClass("js-filtroOpen")) {
    $(".js-filtro").removeClass("js-filtroOpen");
  } else {
    $(".js-filtro").addClass("js-filtroOpen");
  }
});
$(".js-filtro").click(function () {
  $(".js-filtro").removeClass("js-filtroOpen");
});
$(".js-orderBtn").click(function () {
  if ($(".js-order").hasClass("js-orderOpen")) {
    $(".js-order").removeClass("js-orderOpen");
  } else {
    $(".js-order").addClass("js-orderOpen");
  }
});

$(".js-order").click(function () {
  $(".js-order").removeClass("js-orderOpen");
});

$(".js-stop").click(function (e) {
  e.stopPropagation();
});

/*------------------------------------*\
  rango precio
\*------------------------------------*/
$(".range-price").asRange({
  range: true,
  limit: true,
  tip: {
    active: 'onMove'
  }
});

/*------------------------------------*\
  Dropdown Filtro
\*------------------------------------*/
$(".js-dropdownBtn").click(function () {
  if ($(this).hasClass("js-dropdownActive")) {
    $(this).removeClass("js-dropdownActive")
    $(this).siblings(".js-dropdownSlide").slideToggle();
  } else {
    $(this).addClass("js-dropdownActive")
    $(this).siblings(".js-dropdownSlide").slideToggle();
    if ($(window).width() > 1023) {
      $('html, body').animate({
        scrollTop: $(this).offset().top - 40
      }, 500);
    }
  }
});

/*------------------------------------*\
  Filtro Predictivo
\*------------------------------------*/
$('.js-inputAutocomplete').keyup(function() {
  this.value.length > 0 ? $('.js-panelAutocomplete').slideDown() : $('.js-panelAutocomplete').slideUp('fast')
});
// ////////////////////////////////////////////////////////////
// Inicio Función ocultar o mostrar contraseña
function showHidePassword () {
  $('button.form__ico.form__js-ico.i-eye').on('click', function () {
    const el = $(this).parent().find('input[name="contraseña"]')[0]
    el.type = el.type === 'password' ? 'text' : 'password'
  })
}
// Fin Función ocultar o mostrar contraseña
// ////////////////////////////////////////////////////////////

// ////////////////////////////////////////////////////////////
// Inicio Validación Formularios
const messages = {
  emailTel: 'introduce un email o teléfono valido.',
  password: 'introduce una contraseña valida.',
  name: 'Ingresa un Nombre valido.',
  lastName: 'Ingresa un Apellido valido.',
  phone: 'Ingresa un numero de teléfono valido',
  email: 'Ingrese un correo electrónico valido',
  checkPrivacy: 'Para continuar tienes que aceptar nuestro aviso de privacidad',
  street: 'Calle incorrecta. Ingresa una calle valida.',
  streetNumber: 'Numero incorrecto. Ingresa un Numero valido.',
  streetColony: 'Colonia incorrecta. Ingresa una Colonia valida.',
  city: 'Ciudad incorrecta. Ingresa una Ciudad valida.',
  municipality: 'Municipio incorrecto. Ingresa un Municipio valido.',
  RFC: 'Ingresa un RFC valido.',
  confirmEmail: 'Ingresa el mismo correo electrónico.',
  postalCode: 'Código postal incorrect. Ingresa un Código postal valido.',
  value: 'Introduce un valor valido.',
  radio: 'Seleccionar una opción'
}

function validateForms () {
  $('.validate').validate({
    errorPlacement: function () {} // Soluciona problemas con labels y estilos en checkbox y radios
  })

  $('[data-validate]').each(function () { // Buscamos todos los input con atributo data-validate y les añadimos las reglas de validación.
    const elem = this
    const name = elem.getAttribute('name')
    const dataValidate = elem.getAttribute('data-validate')

    $(this).rules('add', {
      required: true,
      messages: {
        required: function (el) {          
          $('small.form__alert[name="' + name + '"]').text(messages[dataValidate]) // Se busca el elemento small.form__alert distinguiéndolo por el name y se agrega el mensaje de error
        }
      },
      success: function () {
        $('small.form__alert[name="' + name + '"]').text('')
      }
    })

    if (dataValidate === 'confirmEmail') {
      $(this).rules('add', {
        equalTo: '[data-validate-equalEmail]'
      })
    }
  })

  jQuery.extend(jQuery.validator.messages, {
    required () { changeCustomMessageValidate(this) },
    equalTo () { changeCustomMessageValidate(this) },
    email () { changeCustomMessageValidate(this) }
  })
}

function changeCustomMessageValidate (el) {
  const elem = el.lastActive
  const name = elem.getAttribute('name')
  const dataValidate = elem.getAttribute('data-validate')
  $('small.form__alert[name="' + name + '"]').text(messages[dataValidate])
}
// Fin Función Validación Formularios
// ////////////////////////////////////////////////////////////

$(document).ready(function () {
  $('button.form__ico.form__js-ico.i-eye')[0] && showHidePassword() // Verifica si hay botón de esconder contraseña y ejecuta la función de esconder y mostrar contraseña
  $('.validate')[0] && validateForms() // Verifica si hay formulario con clase .validate y si asi es ejecuta la función de validación
})

  // ///////////////////////////////////////////////////////////////
  // Inicio botones mas y menos
  $('.group-input__btn').click(function (e) {
    var num = document.getElementById('addon')
    var max
    if (e.target.dataset.btn == '+') {// Setear el numero maximo de productos
      max = e.target.dataset.max
    }
    // Funcion botones aumentar o disminuir
    if (e.target.dataset.btn == '+' && num.value * 1 < max) {
      num.value = num.value * 1 + 1      
    }
    if (e.target.dataset.btn == '-' && num.value * 1 > 1) {
      num.value = num.value * 1 - 1
    }

    if (num.value == max) { // Si sellega al maximo muestra modal
      $('.modal').addClass('modal__view')
    }
  })

  // Fin botones mas y menos
  // ///////////////////////////////////////////////////////////////

//Interacciones de menú

//dropdown list
function dropdownList() {
  $('.js-btn__hidden').click(function () {
    if ($(window).width() < 768) {
      var heightList = 0
      $(this).parent('.menu-brand__item').prependTo($(".menu-brand__list"))
      $(this).parent('.menu-brand__item').addClass('active').siblings().removeClass('active')
      if ($(this).parent('.menu-brand__item active') && $(".menu-brand__list").hasClass("list-close")) {
        heightList = $('.menu-brand__item').outerHeight() * 3
        $(".menu-brand__list").removeClass('list-close')
        $(".menu-brand__list").addClass('list-open')
        $(".menu-brand__list").css("height", heightList)
      } else {
        $(".menu-brand__list").removeClass("list-open")
        $(".menu-brand__list").addClass('list-close')
        $(".menu-brand__list").css("height", $('.menu-brand__item').outerHeight())
      }
    }
  })
}

/////////////////////////////////////////////////////////////////
//Funcionalidad desplegable de search en mobile
function menuSearchMob() {
  if ($(window).width() < 768) {
    $(".js-btnSearch").click(function () {
      $(".submenu-tabs__list").removeClass("js-menuLoginOpen")
      $(".submenu-tabs__list").addClass("js-menuLoginClose")
      $(".menu-gral__list").removeClass("js-menuOpen")
      $(".menu-gral__list").addClass("js-menuClose")
      $(".menu-gral__burger").removeClass("js-menuOpen")
      $(".menu-gral__burger").addClass("js-menuClose")

      if ($(".menu-search").hasClass("js-menuSearchClose")) {
        $(".menu-search").removeClass("js-menuSearchClose")
        $(".menu-search").addClass("js-menuSearchOpen")
      } else {
        $(".menu-search").removeClass("js-menuSearchOpen")
        $(".menu-search").addClass("js-menuSearchClose")
      }
    })
  }
}
//fin Funcionalidad desplegable de search en mobile
/////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////
//Funcionalidad desplegable de iniciar sesion
function menuLogin() {
  $(".js-menuLogin").click(function () {
    $(".menu-search").removeClass("js-menuSearchOpen")
    $(".menu-search").addClass("js-menuSearchClose")
    $(".menu-gral__list").removeClass("js-menuOpen")
    $(".menu-gral__list").addClass("js-menuClose")
    $(".menu-gral__burger").removeClass("js-menuOpen")
    $(".menu-gral__burger").addClass("js-menuClose")

    if ($(window).width() > 768) {
      $(".menu-gral__item").removeClass("js-menuGralOpen")
      $(".menu-gral__item").addClass("js-menuGralClose")
      $(".submenu-gral__list").slideUp()
    }
    if ($(".submenu-tabs__list").hasClass("js-menuLoginClose")) {
      $(".submenu-tabs__list").removeClass("js-menuLoginClose")
      $(".submenu-tabs__list").addClass("js-menuLoginOpen")
    } else {
      $(".submenu-tabs__list").removeClass("js-menuLoginOpen")
      $(".submenu-tabs__list").addClass("js-menuLoginClose")
    }
  })
}
//fin Funcionalidad desplegable de iniciar sesion
/////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////
//Funcionalidad menu off canvas

function menuOffCanvas() {

  $(".menu-gral__burger").click(function () {
    $(".menu-search").removeClass("js-menuSearchOpen")
    $(".menu-search").addClass("js-menuSearchClose")
    $(".submenu-tabs__list").removeClass("js-menuLoginOpen")
    $(".submenu-tabs__list").addClass("js-menuLoginClose")

    if ($(this).hasClass("js-menuClose") && ($(".menu-gral__list").hasClass("js-menuClose"))) {
      $(this).removeClass("js-menuClose")
      $(this).addClass("js-menuOpen")
      $(".menu-gral__list").removeClass("js-menuClose")
      $(".menu-gral__list").addClass("js-menuOpen")
    } else {
      $(this).removeClass("js-menuOpen")
      $(this).addClass("js-menuClose")
      $(".menu-gral__list").removeClass("js-menuOpen")
      $(".menu-gral__list").addClass("js-menuClose")
    }
  })
}
//fin Funcionalidad menu off canvas
/////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////
//Funcionalidad dropdown menu
function dropdownMenu() {
  $(".js-menuGralBtn").click(function () {
    var menuItemActive = $(this).parents(".menu-gral__item")
    var heightMenuItem = $(this).siblings(".submenu-gral__list").outerHeight()
    if ($(window).width() < 768) {
      if (menuItemActive.hasClass("js-menuGralClose")) {
        menuItemActive.css("height", heightMenuItem + 40)
        menuItemActive.removeClass("js-menuGralClose")
        menuItemActive.addClass("js-menuGralOpen")
        menuItemActive.siblings().removeClass("js-menuGralOpen").addClass("js-menuGralClose").css("height", 40)
      } else {
        menuItemActive.addClass("js-menuGralClose")
        menuItemActive.removeClass("js-menuGralOpen")
        menuItemActive.css("height", 40)
      }
    } else {
      $(".submenu-tabs__list").removeClass("js-menuLoginOpen")
      $(".submenu-tabs__list").addClass("js-menuLoginClose")

      if (menuItemActive.hasClass("js-menuGralClose")) {
        menuItemActive.removeClass("js-menuGralClose")
        menuItemActive.addClass("js-menuGralOpen")
        $(this).siblings(".submenu-gral__list").slideDown()
        menuItemActive.siblings(".menu-gral__item").children(".submenu-gral__list").slideUp()
        menuItemActive.siblings(".menu-gral__item").removeClass("js-menuGralOpen").addClass("js-menuGralClose")
      } else {
        menuItemActive.addClass("js-menuGralClose")
        menuItemActive.removeClass("js-menuGralOpen")
        $(this).siblings(".submenu-gral__list").slideUp()
      }
    }
  })
}
//fin Funcionalidad dropdown menu
/////////////////////////////////////////////////////////////////

//hover menu
// function dropdownMenuDesk() {

//   $(".js-menuGralClose").hover(function () {
//     $(this).addClass("hola")
//     $(this).children(".submenu-gral__list").slideToggle()
//   })
// }
//fin hover menu

/////////////////////////////////////////////////////////////////
//Funcionalidad dropdown footer
function dropdownFooter() {
  $(".js-footBtn").click(function () {
    if ($(window).width() < 768) {
      var footListActive = $(this).siblings(".foot__sublist")
      if ($(this).hasClass("js-footItemClose")) {
        // $(".js-footBtn").removeClass("js-footItemOpen")
        $(this).removeClass("js-footItemClose")
        $(this).addClass("js-footItemOpen")
        footListActive.slideToggle()
        $("html, body").animate({
            scrollTop: $(this).offset().top - 50
          },
          500
        )
      } else {
        footListActive.slideToggle()
        $(this).removeClass("js-footItemOpen")
        $(this).addClass("js-footItemClose")
      }
    }
  })
}
//fin Funcionalidad dropdown footer
/////////////////////////////////////////////////////////////////

//Funcionalidad Side menu
function sideMenu() {
  $('.js-btnCart').click(function () {
    
    if ($('.nav-side').hasClass('js-slideOutofView')) {
      if(window.scrollY < 190){
        const top = 180 - window.scrollY
        $('.nav-side').css('top', top + 'px')
      } else {
        $('.nav-side').css('top', '0')
      }

      $('.nav-side').removeClass('js-slideOutofView')
      $('.nav-side').addClass('js-slideToView')
    } else {
      $('.nav-side').addClass('js-slideOutofView')
      $('.nav-side').removeClass('js-slideToView')
    }
  })
  $('.js-sideClose').click(function () {
    $('.nav-side').addClass('js-slideOutofView')
    $('.nav-side').removeClass('js-slideToView')
  })
}
$(window).scroll(function () {
  if ($('.nav-side').hasClass('js-slideToView') && window.scrollY < 190){
    const top = 180 - window.scrollY
    $('.nav-side').css('top', top + 'px')
  } else {
    $('.nav-side').css('top', '0')
  }
})
// Funcionalidad conteo y actualización numero items de slide menu
function updateCountItemsCart () {
  const count = $('.js-deleteItemCart').length
  const textSlideMenu = count === 1 ? 'equipo añadido' : 'equipos añadidos'
  $('.nav-side__title').text(`${count} ${textSlideMenu} al carrito`)
  $('.btn-circle-small').text(count)
}

// Funcionalidad eliminar items de slide menu
function deleteItemCart (elem) {
  $(elem).parent().parent().remove()
  updateCountItemsCart()
}

$('.js-deleteItemCart').on('click', function () {
  deleteItemCart(this)
})

$(document).ready(function () {
  //Funcionalidad desplegable de search en mobile
  menuSearchMob()
  //dropdown list
  dropdownList()
  //menu logion - desplegable
  menuLogin()
  //Funcionalidad menu off canvas
  menuOffCanvas()
  //Funcionalidad dropdown menu
  dropdownMenu()
  //Funcionalidad dropdown footer
  dropdownFooter()
  // dropdownMenuDesk()
  sideMenu()
  // Actualiza numero de items en el carrito
  updateCountItemsCart()
})

$(window).resize(function () {
  //Funcionalidad desplegable de search en mobile
  $(".js-btnSearch").unbind("click")
  menuSearchMob()
  //dropdown list
  $(".js-btn__hidden").unbind("click")
  dropdownList()
  //menu logion - desplegable
  $(".js-menuLogin").unbind("click")
  menuLogin()
  $(".menu-gral__burger").unbind("click")
  //Funcionalidad menu off canvas
  menuOffCanvas()
  //Funcionalidad dropdown menu
  $(".js-menuGralBtn").unbind("click")
  if ($(window).width() < 768) {
    $(".submenu-gral__list").slideDown()
  } else {
    $(".submenu-gral__list").slideUp()
    $(".menu-gral__item").removeClass("js-menuGralOpen").addClass("js-menuGralClose")
  }
  dropdownMenu()
  //Funcionalidad dropdown footer
  $(".js-footBtn").unbind("click")
  dropdownFooter()
  // dropdownMenuDesk()
})

$('.modal__close').click(() => { $('.modal__view').removeClass('modal__view') })

$('#paymentMethod1').show();
$('#paymentMethod2').hide();

$('#btnPaymentMethod1').click(function () {
  $('#paymentMethod1').toggle('fast');
  $('#paymentMethod2').toggle('fast');
  $("i").toggleClass("i-arrow-down i-arrow-up");
});

$('#btnPaymentMethod2').click(function () {
  $('#paymentMethod1').toggle('fast');
  $('#paymentMethod2').toggle('fast');
  $("i").toggleClass("i-arrow-up i-arrow-down");
});

//Inicio funcionalidad [Detalle de producto - BARRA STICKY] : Funcionalidad sticky para desktop
if ($(".band .list-sticky")[0]) {
  //Identifica si esta en pagina detalle producto
  window.addEventListener("scroll", () => {
    //Añade evento al scrool
    el = $("section:nth-child(1)"); //Identifica y guarda el primer section que es despues de este done aparece la barra
    pos = el.height() + el.position().top; //Guarda el valor de la posición (alto del div + el alto de la posición)
    if (window.scrollY >= pos - 170) {
      // Si ya se paso el primer section añade la classe sticky si no la quita
      $(".band").addClass("sticky");
      $(".content").addClass("band-sticky");
    } else {
      $(".band").removeClass("sticky");
      $(".content").removeClass("band-sticky");
    }
  });
}
//Fin funcionalidad [Detalle de producto - BARRA STICKY] : Funcionalidad sticky para desktop

//Inicio funcionalidad Boton carrito fixed bottom
function btnFixedBottom() {
  if ($(".btn-circle.head__item")[0] && $(window).width() > 768) {
    window.addEventListener("scroll", () => {
      //Añade evento al scrool
      a = $(".band_shadow").height();
      if (window.scrollY >= a) {
        // Si ya se paso el primer section añade la classe sticky si no la quita
        $(".js-btnCart.head__item").addClass("btn-circle-Fixed-bottom");
        $('.js-btnCart').addClass('animated bounceInUp');
      } else {
        $(".js-btnCart.head__item").removeClass("btn-circle-Fixed-bottom");
        $('.js-btnCart').removeClass('animated bounceInUp');
      }
    });
  }
}
//Fin funcionalidad Boton carrito fixed bottom

$(document).ready(function () {
  // Botón carrito de compra se fija desde 768px
  // sala a partir del alto de una sección
  btnFixedBottom();
});

var price=$("#priceRange").val();$("#price").text(price),$("#priceRange").asRange({range:!0,limit:!0,step:1,min:0,max:6e5,onChange:function(){var e=$("#priceRange").val();$("#price").text(e)}});
// Slide range
$('#priceRange').asRange({
  range: true,
  limit: true,
  step: 1,
  min: 0,
  max: 600000,
  onChange: function (value) {
    $('#price').text(`$${value[0].toLocaleString('es-ES')} - $${value[1].toLocaleString('es-ES')}`)
  }
})


// Inicio Configuracion Sliders
// Inicio Configuracion menu como carrusel
var options = {
  horizontal: 1,
  itemNav: "basic",
  speed: 300,
  mouseDragging: 1,
  touchDragging: 1
};

$(".menu").sly(options);
$(window).resize(function (e) {
  $(".menu").sly("reload");
});


$(".main-slider__item").slick({ //Slider detalle producto
  arrows: false,
  infinite: false,
  dots: true,
  autoplaySpeed: 6000,
  autoplay: true,
  pauseOnFocus: false,
  dotsClass: 'slick-dots slick-dots_static'
});

//Slider lista Accesorios [cart-showcase.pug]
$(".slider-showcase__list").slick({ //Slider lista de productos
  slidesToShow: 3,
  slidesToScroll: 3,
  pauseOnFocus: false,
  prevArrow: $(".prev"),
  nextArrow: $(".next"),
  responsive: [{
      breakpoint: 420,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      }
    },
    {
      breakpoint: 785,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
      }
    },
  ]
});

$(".hero-banner").slick({ //Slider banner principal
  slidesToShow: 1,
  slidesToScroll: 1,
  mobileFirst: true,
  arrows: false,
  infinite: false,
  dots: true,
  autoplay: true,
  autoplaySpeed: 6000,
  dotsClass: 'slick-dots slick-dots_animated',
});

//Slider Checkbox [board-tabs.pug]
$(".board-tabs__slider").slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  centerMode: true,
  mobileFirst: true,
  centerPadding: '20px',
  pauseOnFocus: false,
  prevArrow: $(".left"),
  nextArrow: $(".right"),
  responsive: [{
      breakpoint: 480,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
        centerMode: true
      }
    },
    {
      breakpoint: 660,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
        centerMode: true
      }
    },
    {
      breakpoint: 890,
      settings: 'unslick'
    }
  ]
});

//Slider Radiobuttons [list-radio.pug]
function listSlider() {
  if ($(window).width() < 829) {
    $(".list-radio__list").slick({
      arrows: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      pauseOnFocus: false,
      prevArrow: $(".left"),
      nextArrow: $(".right")
    });
  } else {
    $('.list-radio__list').slick('unslick');
  }
}

listSlider();

var r;

$(window).resize(function () {
  //$('.list-radio__list')[0].slick.refresh();
  $('.list-radio__list').slick('resize');
  $('.board-tabs__slider').slick('resize');
  clearTimeout(r);
  //console.log(r);
  r = setTimeout(listSlider, 5000);
});

//Slider Detalle de Terminal [slider-detail.pug]
$(".js-slideDetail").slick({
  //Slider lista de productos
  slidesToShow: 2,
  slidesToScroll: 2,
  pauseOnFocus: false,
  prevArrow: $(".prev"),
  nextArrow: $(".next"),
  responsive: [{
    breakpoint: 640,
    settings: {
      slidesToShow: 1,
      slidesToScroll: 1,
    },
  }]
});

//Slider Hero Home [home.html]
$(".js-sliderHome1").slick({ //Slider banner principal
  slidesToShow: 1,
  slidesToScroll: 1,
  mobileFirst: true,
  arrows: false,
  infinite: false,
  dots: true,
  autoplay: true,
  autoplaySpeed: 6000,
  dotsClass: 'slick-dots slick-dots_animated',
});

//Slider Brands [home.html]
$(".js-sliderHome2").slick({
  centerMode: true,
  centerPadding: '200px',
  slidesToShow: 3,
  speed: 1500,
  index: 2,
  arrows: true,
  prevArrow: $(".i-prev"),
  nextArrow: $(".i-next"),
  responsive: [{
    breakpoint: 768,
    settings: {
      arrows: false,
      centerMode: true,
      centerPadding: '100px',
      slidesToShow: 1
      //slidesToShow: 3
    }
  }, {
    breakpoint: 480,
    settings: {
      arrows: false,
      centerMode: true,
      centerPadding: '70px',
      slidesToShow: 1
    }
  }]
});


//Slider de Terminales [home.html]
$(".js-slideHome3").slick({ //Slider lista de productos
  slidesToShow: 3,
  //centerMode: true,
  prevArrow: $(".prev"),
  nextArrow: $(".next"),
  responsive: [{
      breakpoint: 540,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      }
    },
    {
      breakpoint: 785,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
      }
    },
  ]
});

//- [.slider__cards Cards de Vitrina Prepago]
$('.js-slideCards').slick({
  centerMode: true,
  //centerPadding: '20px',
  slidesToShow: 1,
  slidesToScroll: 1,
  mobileFirst: true,
  infinite: true,
  arrows: true,
  responsive: [
    {
      breakpoint: 320,
      settings: {
        centerMode: true,
        centerPadding: '5px',
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true
      }
    },
    {
      breakpoint: 540,
      settings: {
        centerMode: true,
        centerPadding: '8px',
        slidesToShow: 2,
        slidesToScroll: 2,
        arrows: true
      }
    },
    {
      breakpoint: 640,
      settings: {
        centerMode: true,
        centerPadding: '8px',
        slidesToShow: 2,
        slidesToScroll: 2,
        arrows: true
      }
    },
    {
      breakpoint: 768,
      settings: {
        centerMode: false,
        slidesToShow: 3,
        slidesToScroll: 3,
        arrows: true
      }
    },
    {
      breakpoint: 1024,
      settings: {
        centerMode: false,
        slidesToShow: 4,
        slidesToScroll: 4,
        arrows: true,
        adaptiveHeight: true,
      }
    }
  ]
});

///////////////////////TABS -[Car Checkout paso 2]///////////////////
//Estado cuando carga la página
$(".board-tabs__cont").hide(); //contenido oculto
$(".board-tabs__link:first").addClass("js-tab__linkActive").show(); //Tab activa
$(".board-tabs__cont:first").show(); //Contenido activo a primera vista

// //Función para el evento click en el botón activo
$(".board-tabs__link").click(function () {

  $(".board-tabs__link").removeClass("js-tab__linkActive"); //Remove any "active" class
  $(this).addClass("js-tab__linkActive"); //Add "active" class to selected tab
  $(".board-tabs__cont").hide(); //Hide all tab content

  var activeTab = $(this).attr("href"); //Lee el valor del atributo href para identificar la pestaña activa
  //console.log(activeTab);
  $(activeTab).fadeIn(); //Fade in para visibilizar el contenido
  return false;
});
////# end TABS -[Car Checkout paso 2]///////
///TABS FORM -[Car Checkout paso 2]////
// $(window).resize(function () {
//   if (this.resizeTO) clearTimeout(this.resizeTO);
//   this.resizeTO = setTimeout(function () {
//     $(this).trigger('resizeEnd');
//   }, 500);
//   console.log(resizeEnd);
// });
// $(window).bind("resizeEnd", function () {

//   if ($(window).width() < 768) {
//     $(".js-tabContent").hide();
//     $(".js-tabBtn").click(function () {
//       $(this).next(".js-tabContent").slideToggle();
//       $(this).toggleClass("down"); //console.log(this);
//     });
//   }else {
//     $('.js-tabContent').css('display', 'flex');
//   }
// });


// $('.js-tabBtn').click(function () {
//   $('.js-tabContent').toggleClass('js-open');
// });

///# TABS FORM -[Car Checkout paso 2]////
/////Data-sheet__list [comparador]//////
$(".js-btnData").click(function (e) {
  e.preventDefault();
  $(".js-listData").slideToggle();
});

// Panel Tabs - [panel.html]
function panelTabs() {
  $('.js-tabBtn').click(function () {
    if ($(this).hasClass('js-tabOpen')) {
      $(this).removeClass('js-tabOpen').addClass('js-tabClose');
      $(this).siblings(".js-tabContent").slideUp();
    } else {
      $('.js-tabBtn').addClass('js-tabClose').removeClass('js-tabOpen');
      $(".js-tabContent").slideUp();
      $(this).removeClass('js-tabClose').addClass('js-tabOpen');
      $(this).siblings(".js-tabContent").slideDown();
    }
  });
}

$(document).ready(function () {
  // Panel Tabs - [panel.html]
  panelTabs()
})

///////////////////////////////////////////////////////////
  //Inicio Funcionalidad tabs y Dropdown
  // Funcion para obtener el alto de la tab activa
  function tabHeight() {
    if ($(window).width() > 768) {
      var heightTabActive = $(".js-tabItemActive")
        .children(".tabs__pane")
        .height();
      $(".tabs").css("height", heightTabActive + 140);
      //console.log(heightTabActive);
    } else {
      $(".tabs").css("height", "initial");
    }
  }

  $(".tabs__item button").click(function() {
    const parent = this.parentElement;
    if (window.innerWidth <= 767 && $(parent).hasClass("js-tabItemActive")) {
      //Al dar clic cierra el tab si esta abierto cuando la pantalla es menor a 767
      $(parent).removeClass("js-tabItemActive");
    } else {
      $(".js-tabItemActive").removeClass("js-tabItemActive");
      $(parent).addClass("js-tabItemActive");
      tabHeight();
    }
  });

  function tabsmediaquery(x) {
    //Verifica al redimensionar la ventana y abre o cierra los tabs correspondientes
    if (!x.matches) {
      // If media query matches
      if (
        !$(".tabs__item").hasClass("js-tabItemActive") &&
        $(".tabs__item").length > 0
      ) {
        $(".tabs__item")[0].classList.add("js-tabItemActive");
      }
    }
  }

  const mediaquery = window.matchMedia("(max-width: 767px)");
  tabsmediaquery(mediaquery); // Call listener function at run time
  mediaquery.addListener(tabsmediaquery); // Attach listener function on state changes

  //Fin Funcionalidad tabs y Dropdown
  /////////////////////////////////////////////////////////////////
  $(".faq__btn").click(function() {
    if (
      $(this)
        .parents(".faq__item")
        .hasClass("js-dropdownActive")
    ) {
      $(this)
        .parents(".faq__item")
        .removeClass("js-dropdownActive");
      $(this)
        .siblings(".faq__content")
        .slideToggle();
    } else {
      $(this)
        .parents(".faq__item")
        .addClass("js-dropdownActive");
      $(this)
        .siblings(".faq__content")
        .slideToggle();
      $("html, body").animate(
        {
          scrollTop: $(this).offset().top - 100
        },
        500
      );
    }
  });

  function dropDownButton(e){
    const el = $(e).parent()
    const contenido= el[0].getElementsByClassName('form__cont')
    $(contenido).is(":visible")? $(contenido).slideUp('fast'):$(contenido).slideDown('fast')
  }

  function dropDownRadio(e){
    const el = $(e).parent().parent()
    const contenido = el[0].getElementsByClassName('js-formDropdown')
    console.log(el)
    if( !$(contenido).is(":visible")){
      $( ".js-formDropdown" ).slideUp('fast')
      $(contenido).slideDown('fast')
    }
  }
  $('.form__radio:not(:checked) ~ .js-formDropdown').hide() // Oculta los el contenido de los radio buttons no seleccionados

  function dropDownInit(){
    const elems = $( ".js-formDropdown" )
    if(!elems[0]) return undefined
    elems.hide()
    $(elems[0]).show()
    return undefined
  }
  // Funcion detalle sim radio buttons informacion
  //En el HTML añadir el atributo data-radio-name="sim1" donde "sim1" corresponde al id del radio al que corresponde.
  function hidePriceRadio () { // Esconde todos los div con informacion excepto el correspondiente al radio seleeccionado
    try{
      $('.card-box').hide()
      $('.card-box__link').hide()
      const id = $('.card-radio__input[name="priceSim"]:checked')[0].id
      $('[data-radio-name="' + id + '"]').show()
    }
    catch (err) {}
  }

  function radioSim () { // Cambia el div al seleccionar otro radio
    $('.card-box').hide()
    $('.card-box__link').hide()
    $('[data-radio-name="' + this.id + '"]').show()
  }
  $('.card-radio__input[name="priceSim"]').click(radioSim) // Vincula la funcion de cambio de radio a los corespondientes radios
  /////////////////////////////////////////////////////////////////
  // Funcionalidad tabs terminos y condiciones

  // Dropdown tabla
  $('.js-dataDetailBtn').on('click', function () {
    const btnInvoice = $(this).data().invoice // Obtiene el atributo data-invoice para saber el código de la factura y asi identificar la tabla
    const tableCont = $('.js-dataDetail[data-invoice="' + btnInvoice + '"]') // Obtiene la tabla vinculada según  el código de la factura en el botón
    return tableCont.is(':visible') ? tableCont.slideUp('fast') : tableCont.slideDown('fast') // Esconde o muestra la tabla de la factura
  })

  //Dropdown tabla de notificaciones
  function dropdownNotify() {
    $('.js-btnCloseBanner').on('click', function (e) {
      $(this).parent($(".es-table-banner__wrap")).slideUp();
    })
  }

  $(document).ready(function() {
    // Funcion para obtener el alto de la tab activa
    tabHeight()
    hidePriceRadio()
    dropDownInit()
    dropdownNotify()
  })

  $(window).resize(function() {
    tabHeight()
  })
  //Fin Funcionalidad tabs terminos y condiciones
  /////////////////////////////////////////////////////////////////

function tootltipData() {
  $(".js-btnTooltipData").on('click', function (e) {
    if ($(this).siblings(".js-tooltipData").hasClass("js-tooltipData-close")) {
      $(".js-tooltipData.js-tooltipData-open").slideUp().removeClass("js-tooltipData-open").addClass("js-tooltipData-close");
      $(this).siblings(".js-tooltipData").slideDown().addClass("js-tooltipData-open").removeClass("js-tooltipData-close");
    } else {
      $(this).siblings(".js-tooltipData").slideUp().removeClass("js-tooltipData-open").addClass("js-tooltipData-close");
    }
  });
}

$(document).ready(function () {
  tootltipData();
});

$(window).resize(function () {
  $(".js-btnTooltipData").unbind("click");
  tootltipData();
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIkNhbGVuZGFyaW9zLmpzIiwiZmlsdHJvcy5qcyIsImZvcm1zVmFsaWRhdGUuanMiLCJtYXNNZW5vc1Byb2R1Y3RvLmpzIiwibWVudS5qcyIsIm1vZGFscy5qcyIsInBheUZvcm0uLmpzIiwic2Nyb2xsLmpzIiwic2xpZGUtcmFuZ2UtbWluLmpzIiwic2xpZGUtcmFuZ2UuanMiLCJzbGlkZXMuanMiLCJ0YWJzLmpzIiwidGFic0Ryb3Bkb3duLmpzIiwidG9vbHRpcC5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDekJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ2pFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDbEZBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ3ZCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDbFJBO0FBQ0E7QUNEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUNkQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDM0NBO0FDQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUNaQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUM3UUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDdkVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ2xKQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiJtYWluLmpzIiwic291cmNlc0NvbnRlbnQiOlsiY29uc3QgbWVzZXMgPSBbJ0VuZScsICdGZWInLCAnTWFyJywgJ0FicicsICdNYXknLCAnSnVuJywgJ0p1bCcsICdBZ28nLCAnU2VwJywgJ09jdCcsICdOb3YnLCAnRGljJ11cbmZ1bmN0aW9uIG9idGVuZXJGZWNoYSAoZmVjaGEpIHtcbiAgcmV0dXJuIGAke2ZlY2hhLmdldERhdGUoKX0gJHttZXNlc1tmZWNoYS5nZXRNb250aCgpXX1gXG59XG5jb25zdCBvcHRpb25DYWxlbmRhciA9IHtcbiAgbG9jYWxlOiAnZXMnLFxuICBtb2RlOiAncmFuZ2UnLFxuICBjbG9zZU9uU2VsZWN0OiBmYWxzZSxcbiAgcGx1Z2luczogW25ldyBjb25maXJtRGF0ZVBsdWdpbih7XG4gICAgY29uZmlybVRleHQ6IFwiQXBsaWNhciBcIixcbiAgICBzaG93QWx3YXlzOiB0cnVlLFxuICB9KV0sXG4gIHNob3dNb250aHM6IDIsXG4gIG9uQ2xvc2U6IGZ1bmN0aW9uIChzZWxlY3RlZERhdGVzLCBkYXRlU3RyLCBpbnN0YW5jZSkge1xuICAgIGlmKHNlbGVjdGVkRGF0ZXNbMV0pe1xuICAgICAgY29uc3QgZmVjaGEgPSBgJHtvYnRlbmVyRmVjaGEoc2VsZWN0ZWREYXRlc1swXSl9IC0gJHtvYnRlbmVyRmVjaGEoc2VsZWN0ZWREYXRlc1sxXSl9YFxuICAgICAgJCgnLmpzLWRhdGUtcGlja2VyJykudGV4dChmZWNoYSlcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5jbGVhcigpXG4gICAgfVxuICB9XG59XG4kKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbiAoKSB7XG4gIGZsYXRwaWNrcignLmpzLWRhdGUtcGlja2VyJywgb3B0aW9uQ2FsZW5kYXIpXG59KVxuIiwiLyotLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0qXFxcbiAgRnVuY2lvbmFsaWRhZCBvcGVuIGZpbHRybyB5IHNlbGVjdCBvcmRlclxuXFwqLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tKi9cblxuXG4kKFwiLmpzLWZpbHRyb0J0blwiKS5jbGljayhmdW5jdGlvbiAoKSB7XG4gIGlmICgkKFwiLmpzLWZpbHRyb1wiKS5oYXNDbGFzcyhcImpzLWZpbHRyb09wZW5cIikpIHtcbiAgICAkKFwiLmpzLWZpbHRyb1wiKS5yZW1vdmVDbGFzcyhcImpzLWZpbHRyb09wZW5cIik7XG4gIH0gZWxzZSB7XG4gICAgJChcIi5qcy1maWx0cm9cIikuYWRkQ2xhc3MoXCJqcy1maWx0cm9PcGVuXCIpO1xuICB9XG59KTtcbiQoXCIuanMtZmlsdHJvXCIpLmNsaWNrKGZ1bmN0aW9uICgpIHtcbiAgJChcIi5qcy1maWx0cm9cIikucmVtb3ZlQ2xhc3MoXCJqcy1maWx0cm9PcGVuXCIpO1xufSk7XG4kKFwiLmpzLW9yZGVyQnRuXCIpLmNsaWNrKGZ1bmN0aW9uICgpIHtcbiAgaWYgKCQoXCIuanMtb3JkZXJcIikuaGFzQ2xhc3MoXCJqcy1vcmRlck9wZW5cIikpIHtcbiAgICAkKFwiLmpzLW9yZGVyXCIpLnJlbW92ZUNsYXNzKFwianMtb3JkZXJPcGVuXCIpO1xuICB9IGVsc2Uge1xuICAgICQoXCIuanMtb3JkZXJcIikuYWRkQ2xhc3MoXCJqcy1vcmRlck9wZW5cIik7XG4gIH1cbn0pO1xuXG4kKFwiLmpzLW9yZGVyXCIpLmNsaWNrKGZ1bmN0aW9uICgpIHtcbiAgJChcIi5qcy1vcmRlclwiKS5yZW1vdmVDbGFzcyhcImpzLW9yZGVyT3BlblwiKTtcbn0pO1xuXG4kKFwiLmpzLXN0b3BcIikuY2xpY2soZnVuY3Rpb24gKGUpIHtcbiAgZS5zdG9wUHJvcGFnYXRpb24oKTtcbn0pO1xuXG4vKi0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLSpcXFxuICByYW5nbyBwcmVjaW9cblxcKi0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLSovXG4kKFwiLnJhbmdlLXByaWNlXCIpLmFzUmFuZ2Uoe1xuICByYW5nZTogdHJ1ZSxcbiAgbGltaXQ6IHRydWUsXG4gIHRpcDoge1xuICAgIGFjdGl2ZTogJ29uTW92ZSdcbiAgfVxufSk7XG5cbi8qLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tKlxcXG4gIERyb3Bkb3duIEZpbHRyb1xuXFwqLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tKi9cbiQoXCIuanMtZHJvcGRvd25CdG5cIikuY2xpY2soZnVuY3Rpb24gKCkge1xuICBpZiAoJCh0aGlzKS5oYXNDbGFzcyhcImpzLWRyb3Bkb3duQWN0aXZlXCIpKSB7XG4gICAgJCh0aGlzKS5yZW1vdmVDbGFzcyhcImpzLWRyb3Bkb3duQWN0aXZlXCIpXG4gICAgJCh0aGlzKS5zaWJsaW5ncyhcIi5qcy1kcm9wZG93blNsaWRlXCIpLnNsaWRlVG9nZ2xlKCk7XG4gIH0gZWxzZSB7XG4gICAgJCh0aGlzKS5hZGRDbGFzcyhcImpzLWRyb3Bkb3duQWN0aXZlXCIpXG4gICAgJCh0aGlzKS5zaWJsaW5ncyhcIi5qcy1kcm9wZG93blNsaWRlXCIpLnNsaWRlVG9nZ2xlKCk7XG4gICAgaWYgKCQod2luZG93KS53aWR0aCgpID4gMTAyMykge1xuICAgICAgJCgnaHRtbCwgYm9keScpLmFuaW1hdGUoe1xuICAgICAgICBzY3JvbGxUb3A6ICQodGhpcykub2Zmc2V0KCkudG9wIC0gNDBcbiAgICAgIH0sIDUwMCk7XG4gICAgfVxuICB9XG59KTtcblxuLyotLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0qXFxcbiAgRmlsdHJvIFByZWRpY3Rpdm9cblxcKi0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLSovXG4kKCcuanMtaW5wdXRBdXRvY29tcGxldGUnKS5rZXl1cChmdW5jdGlvbigpIHtcbiAgdGhpcy52YWx1ZS5sZW5ndGggPiAwID8gJCgnLmpzLXBhbmVsQXV0b2NvbXBsZXRlJykuc2xpZGVEb3duKCkgOiAkKCcuanMtcGFuZWxBdXRvY29tcGxldGUnKS5zbGlkZVVwKCdmYXN0Jylcbn0pOyIsIi8vIC8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gSW5pY2lvIEZ1bmNpw7NuIG9jdWx0YXIgbyBtb3N0cmFyIGNvbnRyYXNlw7FhXG5mdW5jdGlvbiBzaG93SGlkZVBhc3N3b3JkICgpIHtcbiAgJCgnYnV0dG9uLmZvcm1fX2ljby5mb3JtX19qcy1pY28uaS1leWUnKS5vbignY2xpY2snLCBmdW5jdGlvbiAoKSB7XG4gICAgY29uc3QgZWwgPSAkKHRoaXMpLnBhcmVudCgpLmZpbmQoJ2lucHV0W25hbWU9XCJjb250cmFzZcOxYVwiXScpWzBdXG4gICAgZWwudHlwZSA9IGVsLnR5cGUgPT09ICdwYXNzd29yZCcgPyAndGV4dCcgOiAncGFzc3dvcmQnXG4gIH0pXG59XG4vLyBGaW4gRnVuY2nDs24gb2N1bHRhciBvIG1vc3RyYXIgY29udHJhc2XDsWFcbi8vIC8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL1xuXG4vLyAvLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIEluaWNpbyBWYWxpZGFjacOzbiBGb3JtdWxhcmlvc1xuY29uc3QgbWVzc2FnZXMgPSB7XG4gIGVtYWlsVGVsOiAnaW50cm9kdWNlIHVuIGVtYWlsIG8gdGVsw6lmb25vIHZhbGlkby4nLFxuICBwYXNzd29yZDogJ2ludHJvZHVjZSB1bmEgY29udHJhc2XDsWEgdmFsaWRhLicsXG4gIG5hbWU6ICdJbmdyZXNhIHVuIE5vbWJyZSB2YWxpZG8uJyxcbiAgbGFzdE5hbWU6ICdJbmdyZXNhIHVuIEFwZWxsaWRvIHZhbGlkby4nLFxuICBwaG9uZTogJ0luZ3Jlc2EgdW4gbnVtZXJvIGRlIHRlbMOpZm9ubyB2YWxpZG8nLFxuICBlbWFpbDogJ0luZ3Jlc2UgdW4gY29ycmVvIGVsZWN0csOzbmljbyB2YWxpZG8nLFxuICBjaGVja1ByaXZhY3k6ICdQYXJhIGNvbnRpbnVhciB0aWVuZXMgcXVlIGFjZXB0YXIgbnVlc3RybyBhdmlzbyBkZSBwcml2YWNpZGFkJyxcbiAgc3RyZWV0OiAnQ2FsbGUgaW5jb3JyZWN0YS4gSW5ncmVzYSB1bmEgY2FsbGUgdmFsaWRhLicsXG4gIHN0cmVldE51bWJlcjogJ051bWVybyBpbmNvcnJlY3RvLiBJbmdyZXNhIHVuIE51bWVybyB2YWxpZG8uJyxcbiAgc3RyZWV0Q29sb255OiAnQ29sb25pYSBpbmNvcnJlY3RhLiBJbmdyZXNhIHVuYSBDb2xvbmlhIHZhbGlkYS4nLFxuICBjaXR5OiAnQ2l1ZGFkIGluY29ycmVjdGEuIEluZ3Jlc2EgdW5hIENpdWRhZCB2YWxpZGEuJyxcbiAgbXVuaWNpcGFsaXR5OiAnTXVuaWNpcGlvIGluY29ycmVjdG8uIEluZ3Jlc2EgdW4gTXVuaWNpcGlvIHZhbGlkby4nLFxuICBSRkM6ICdJbmdyZXNhIHVuIFJGQyB2YWxpZG8uJyxcbiAgY29uZmlybUVtYWlsOiAnSW5ncmVzYSBlbCBtaXNtbyBjb3JyZW8gZWxlY3Ryw7NuaWNvLicsXG4gIHBvc3RhbENvZGU6ICdDw7NkaWdvIHBvc3RhbCBpbmNvcnJlY3QuIEluZ3Jlc2EgdW4gQ8OzZGlnbyBwb3N0YWwgdmFsaWRvLicsXG4gIHZhbHVlOiAnSW50cm9kdWNlIHVuIHZhbG9yIHZhbGlkby4nLFxuICByYWRpbzogJ1NlbGVjY2lvbmFyIHVuYSBvcGNpw7NuJ1xufVxuXG5mdW5jdGlvbiB2YWxpZGF0ZUZvcm1zICgpIHtcbiAgJCgnLnZhbGlkYXRlJykudmFsaWRhdGUoe1xuICAgIGVycm9yUGxhY2VtZW50OiBmdW5jdGlvbiAoKSB7fSAvLyBTb2x1Y2lvbmEgcHJvYmxlbWFzIGNvbiBsYWJlbHMgeSBlc3RpbG9zIGVuIGNoZWNrYm94IHkgcmFkaW9zXG4gIH0pXG5cbiAgJCgnW2RhdGEtdmFsaWRhdGVdJykuZWFjaChmdW5jdGlvbiAoKSB7IC8vIEJ1c2NhbW9zIHRvZG9zIGxvcyBpbnB1dCBjb24gYXRyaWJ1dG8gZGF0YS12YWxpZGF0ZSB5IGxlcyBhw7FhZGltb3MgbGFzIHJlZ2xhcyBkZSB2YWxpZGFjacOzbi5cbiAgICBjb25zdCBlbGVtID0gdGhpc1xuICAgIGNvbnN0IG5hbWUgPSBlbGVtLmdldEF0dHJpYnV0ZSgnbmFtZScpXG4gICAgY29uc3QgZGF0YVZhbGlkYXRlID0gZWxlbS5nZXRBdHRyaWJ1dGUoJ2RhdGEtdmFsaWRhdGUnKVxuXG4gICAgJCh0aGlzKS5ydWxlcygnYWRkJywge1xuICAgICAgcmVxdWlyZWQ6IHRydWUsXG4gICAgICBtZXNzYWdlczoge1xuICAgICAgICByZXF1aXJlZDogZnVuY3Rpb24gKGVsKSB7ICAgICAgICAgIFxuICAgICAgICAgICQoJ3NtYWxsLmZvcm1fX2FsZXJ0W25hbWU9XCInICsgbmFtZSArICdcIl0nKS50ZXh0KG1lc3NhZ2VzW2RhdGFWYWxpZGF0ZV0pIC8vIFNlIGJ1c2NhIGVsIGVsZW1lbnRvIHNtYWxsLmZvcm1fX2FsZXJ0IGRpc3Rpbmd1acOpbmRvbG8gcG9yIGVsIG5hbWUgeSBzZSBhZ3JlZ2EgZWwgbWVuc2FqZSBkZSBlcnJvclxuICAgICAgICB9XG4gICAgICB9LFxuICAgICAgc3VjY2VzczogZnVuY3Rpb24gKCkge1xuICAgICAgICAkKCdzbWFsbC5mb3JtX19hbGVydFtuYW1lPVwiJyArIG5hbWUgKyAnXCJdJykudGV4dCgnJylcbiAgICAgIH1cbiAgICB9KVxuXG4gICAgaWYgKGRhdGFWYWxpZGF0ZSA9PT0gJ2NvbmZpcm1FbWFpbCcpIHtcbiAgICAgICQodGhpcykucnVsZXMoJ2FkZCcsIHtcbiAgICAgICAgZXF1YWxUbzogJ1tkYXRhLXZhbGlkYXRlLWVxdWFsRW1haWxdJ1xuICAgICAgfSlcbiAgICB9XG4gIH0pXG5cbiAgalF1ZXJ5LmV4dGVuZChqUXVlcnkudmFsaWRhdG9yLm1lc3NhZ2VzLCB7XG4gICAgcmVxdWlyZWQgKCkgeyBjaGFuZ2VDdXN0b21NZXNzYWdlVmFsaWRhdGUodGhpcykgfSxcbiAgICBlcXVhbFRvICgpIHsgY2hhbmdlQ3VzdG9tTWVzc2FnZVZhbGlkYXRlKHRoaXMpIH0sXG4gICAgZW1haWwgKCkgeyBjaGFuZ2VDdXN0b21NZXNzYWdlVmFsaWRhdGUodGhpcykgfVxuICB9KVxufVxuXG5mdW5jdGlvbiBjaGFuZ2VDdXN0b21NZXNzYWdlVmFsaWRhdGUgKGVsKSB7XG4gIGNvbnN0IGVsZW0gPSBlbC5sYXN0QWN0aXZlXG4gIGNvbnN0IG5hbWUgPSBlbGVtLmdldEF0dHJpYnV0ZSgnbmFtZScpXG4gIGNvbnN0IGRhdGFWYWxpZGF0ZSA9IGVsZW0uZ2V0QXR0cmlidXRlKCdkYXRhLXZhbGlkYXRlJylcbiAgJCgnc21hbGwuZm9ybV9fYWxlcnRbbmFtZT1cIicgKyBuYW1lICsgJ1wiXScpLnRleHQobWVzc2FnZXNbZGF0YVZhbGlkYXRlXSlcbn1cbi8vIEZpbiBGdW5jacOzbiBWYWxpZGFjacOzbiBGb3JtdWxhcmlvc1xuLy8gLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vXG5cbiQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uICgpIHtcbiAgJCgnYnV0dG9uLmZvcm1fX2ljby5mb3JtX19qcy1pY28uaS1leWUnKVswXSAmJiBzaG93SGlkZVBhc3N3b3JkKCkgLy8gVmVyaWZpY2Egc2kgaGF5IGJvdMOzbiBkZSBlc2NvbmRlciBjb250cmFzZcOxYSB5IGVqZWN1dGEgbGEgZnVuY2nDs24gZGUgZXNjb25kZXIgeSBtb3N0cmFyIGNvbnRyYXNlw7FhXG4gICQoJy52YWxpZGF0ZScpWzBdICYmIHZhbGlkYXRlRm9ybXMoKSAvLyBWZXJpZmljYSBzaSBoYXkgZm9ybXVsYXJpbyBjb24gY2xhc2UgLnZhbGlkYXRlIHkgc2kgYXNpIGVzIGVqZWN1dGEgbGEgZnVuY2nDs24gZGUgdmFsaWRhY2nDs25cbn0pXG4iLCIgIC8vIC8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL1xuICAvLyBJbmljaW8gYm90b25lcyBtYXMgeSBtZW5vc1xuICAkKCcuZ3JvdXAtaW5wdXRfX2J0bicpLmNsaWNrKGZ1bmN0aW9uIChlKSB7XG4gICAgdmFyIG51bSA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdhZGRvbicpXG4gICAgdmFyIG1heFxuICAgIGlmIChlLnRhcmdldC5kYXRhc2V0LmJ0biA9PSAnKycpIHsvLyBTZXRlYXIgZWwgbnVtZXJvIG1heGltbyBkZSBwcm9kdWN0b3NcbiAgICAgIG1heCA9IGUudGFyZ2V0LmRhdGFzZXQubWF4XG4gICAgfVxuICAgIC8vIEZ1bmNpb24gYm90b25lcyBhdW1lbnRhciBvIGRpc21pbnVpclxuICAgIGlmIChlLnRhcmdldC5kYXRhc2V0LmJ0biA9PSAnKycgJiYgbnVtLnZhbHVlICogMSA8IG1heCkge1xuICAgICAgbnVtLnZhbHVlID0gbnVtLnZhbHVlICogMSArIDEgICAgICBcbiAgICB9XG4gICAgaWYgKGUudGFyZ2V0LmRhdGFzZXQuYnRuID09ICctJyAmJiBudW0udmFsdWUgKiAxID4gMSkge1xuICAgICAgbnVtLnZhbHVlID0gbnVtLnZhbHVlICogMSAtIDFcbiAgICB9XG5cbiAgICBpZiAobnVtLnZhbHVlID09IG1heCkgeyAvLyBTaSBzZWxsZWdhIGFsIG1heGltbyBtdWVzdHJhIG1vZGFsXG4gICAgICAkKCcubW9kYWwnKS5hZGRDbGFzcygnbW9kYWxfX3ZpZXcnKVxuICAgIH1cbiAgfSlcblxuICAvLyBGaW4gYm90b25lcyBtYXMgeSBtZW5vc1xuICAvLyAvLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy9cbiIsIi8vSW50ZXJhY2Npb25lcyBkZSBtZW7DulxuXG4vL2Ryb3Bkb3duIGxpc3RcbmZ1bmN0aW9uIGRyb3Bkb3duTGlzdCgpIHtcbiAgJCgnLmpzLWJ0bl9faGlkZGVuJykuY2xpY2soZnVuY3Rpb24gKCkge1xuICAgIGlmICgkKHdpbmRvdykud2lkdGgoKSA8IDc2OCkge1xuICAgICAgdmFyIGhlaWdodExpc3QgPSAwXG4gICAgICAkKHRoaXMpLnBhcmVudCgnLm1lbnUtYnJhbmRfX2l0ZW0nKS5wcmVwZW5kVG8oJChcIi5tZW51LWJyYW5kX19saXN0XCIpKVxuICAgICAgJCh0aGlzKS5wYXJlbnQoJy5tZW51LWJyYW5kX19pdGVtJykuYWRkQ2xhc3MoJ2FjdGl2ZScpLnNpYmxpbmdzKCkucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpXG4gICAgICBpZiAoJCh0aGlzKS5wYXJlbnQoJy5tZW51LWJyYW5kX19pdGVtIGFjdGl2ZScpICYmICQoXCIubWVudS1icmFuZF9fbGlzdFwiKS5oYXNDbGFzcyhcImxpc3QtY2xvc2VcIikpIHtcbiAgICAgICAgaGVpZ2h0TGlzdCA9ICQoJy5tZW51LWJyYW5kX19pdGVtJykub3V0ZXJIZWlnaHQoKSAqIDNcbiAgICAgICAgJChcIi5tZW51LWJyYW5kX19saXN0XCIpLnJlbW92ZUNsYXNzKCdsaXN0LWNsb3NlJylcbiAgICAgICAgJChcIi5tZW51LWJyYW5kX19saXN0XCIpLmFkZENsYXNzKCdsaXN0LW9wZW4nKVxuICAgICAgICAkKFwiLm1lbnUtYnJhbmRfX2xpc3RcIikuY3NzKFwiaGVpZ2h0XCIsIGhlaWdodExpc3QpXG4gICAgICB9IGVsc2Uge1xuICAgICAgICAkKFwiLm1lbnUtYnJhbmRfX2xpc3RcIikucmVtb3ZlQ2xhc3MoXCJsaXN0LW9wZW5cIilcbiAgICAgICAgJChcIi5tZW51LWJyYW5kX19saXN0XCIpLmFkZENsYXNzKCdsaXN0LWNsb3NlJylcbiAgICAgICAgJChcIi5tZW51LWJyYW5kX19saXN0XCIpLmNzcyhcImhlaWdodFwiLCAkKCcubWVudS1icmFuZF9faXRlbScpLm91dGVySGVpZ2h0KCkpXG4gICAgICB9XG4gICAgfVxuICB9KVxufVxuXG4vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL1xuLy9GdW5jaW9uYWxpZGFkIGRlc3BsZWdhYmxlIGRlIHNlYXJjaCBlbiBtb2JpbGVcbmZ1bmN0aW9uIG1lbnVTZWFyY2hNb2IoKSB7XG4gIGlmICgkKHdpbmRvdykud2lkdGgoKSA8IDc2OCkge1xuICAgICQoXCIuanMtYnRuU2VhcmNoXCIpLmNsaWNrKGZ1bmN0aW9uICgpIHtcbiAgICAgICQoXCIuc3VibWVudS10YWJzX19saXN0XCIpLnJlbW92ZUNsYXNzKFwianMtbWVudUxvZ2luT3BlblwiKVxuICAgICAgJChcIi5zdWJtZW51LXRhYnNfX2xpc3RcIikuYWRkQ2xhc3MoXCJqcy1tZW51TG9naW5DbG9zZVwiKVxuICAgICAgJChcIi5tZW51LWdyYWxfX2xpc3RcIikucmVtb3ZlQ2xhc3MoXCJqcy1tZW51T3BlblwiKVxuICAgICAgJChcIi5tZW51LWdyYWxfX2xpc3RcIikuYWRkQ2xhc3MoXCJqcy1tZW51Q2xvc2VcIilcbiAgICAgICQoXCIubWVudS1ncmFsX19idXJnZXJcIikucmVtb3ZlQ2xhc3MoXCJqcy1tZW51T3BlblwiKVxuICAgICAgJChcIi5tZW51LWdyYWxfX2J1cmdlclwiKS5hZGRDbGFzcyhcImpzLW1lbnVDbG9zZVwiKVxuXG4gICAgICBpZiAoJChcIi5tZW51LXNlYXJjaFwiKS5oYXNDbGFzcyhcImpzLW1lbnVTZWFyY2hDbG9zZVwiKSkge1xuICAgICAgICAkKFwiLm1lbnUtc2VhcmNoXCIpLnJlbW92ZUNsYXNzKFwianMtbWVudVNlYXJjaENsb3NlXCIpXG4gICAgICAgICQoXCIubWVudS1zZWFyY2hcIikuYWRkQ2xhc3MoXCJqcy1tZW51U2VhcmNoT3BlblwiKVxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgJChcIi5tZW51LXNlYXJjaFwiKS5yZW1vdmVDbGFzcyhcImpzLW1lbnVTZWFyY2hPcGVuXCIpXG4gICAgICAgICQoXCIubWVudS1zZWFyY2hcIikuYWRkQ2xhc3MoXCJqcy1tZW51U2VhcmNoQ2xvc2VcIilcbiAgICAgIH1cbiAgICB9KVxuICB9XG59XG4vL2ZpbiBGdW5jaW9uYWxpZGFkIGRlc3BsZWdhYmxlIGRlIHNlYXJjaCBlbiBtb2JpbGVcbi8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vXG5cbi8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vXG4vL0Z1bmNpb25hbGlkYWQgZGVzcGxlZ2FibGUgZGUgaW5pY2lhciBzZXNpb25cbmZ1bmN0aW9uIG1lbnVMb2dpbigpIHtcbiAgJChcIi5qcy1tZW51TG9naW5cIikuY2xpY2soZnVuY3Rpb24gKCkge1xuICAgICQoXCIubWVudS1zZWFyY2hcIikucmVtb3ZlQ2xhc3MoXCJqcy1tZW51U2VhcmNoT3BlblwiKVxuICAgICQoXCIubWVudS1zZWFyY2hcIikuYWRkQ2xhc3MoXCJqcy1tZW51U2VhcmNoQ2xvc2VcIilcbiAgICAkKFwiLm1lbnUtZ3JhbF9fbGlzdFwiKS5yZW1vdmVDbGFzcyhcImpzLW1lbnVPcGVuXCIpXG4gICAgJChcIi5tZW51LWdyYWxfX2xpc3RcIikuYWRkQ2xhc3MoXCJqcy1tZW51Q2xvc2VcIilcbiAgICAkKFwiLm1lbnUtZ3JhbF9fYnVyZ2VyXCIpLnJlbW92ZUNsYXNzKFwianMtbWVudU9wZW5cIilcbiAgICAkKFwiLm1lbnUtZ3JhbF9fYnVyZ2VyXCIpLmFkZENsYXNzKFwianMtbWVudUNsb3NlXCIpXG5cbiAgICBpZiAoJCh3aW5kb3cpLndpZHRoKCkgPiA3NjgpIHtcbiAgICAgICQoXCIubWVudS1ncmFsX19pdGVtXCIpLnJlbW92ZUNsYXNzKFwianMtbWVudUdyYWxPcGVuXCIpXG4gICAgICAkKFwiLm1lbnUtZ3JhbF9faXRlbVwiKS5hZGRDbGFzcyhcImpzLW1lbnVHcmFsQ2xvc2VcIilcbiAgICAgICQoXCIuc3VibWVudS1ncmFsX19saXN0XCIpLnNsaWRlVXAoKVxuICAgIH1cbiAgICBpZiAoJChcIi5zdWJtZW51LXRhYnNfX2xpc3RcIikuaGFzQ2xhc3MoXCJqcy1tZW51TG9naW5DbG9zZVwiKSkge1xuICAgICAgJChcIi5zdWJtZW51LXRhYnNfX2xpc3RcIikucmVtb3ZlQ2xhc3MoXCJqcy1tZW51TG9naW5DbG9zZVwiKVxuICAgICAgJChcIi5zdWJtZW51LXRhYnNfX2xpc3RcIikuYWRkQ2xhc3MoXCJqcy1tZW51TG9naW5PcGVuXCIpXG4gICAgfSBlbHNlIHtcbiAgICAgICQoXCIuc3VibWVudS10YWJzX19saXN0XCIpLnJlbW92ZUNsYXNzKFwianMtbWVudUxvZ2luT3BlblwiKVxuICAgICAgJChcIi5zdWJtZW51LXRhYnNfX2xpc3RcIikuYWRkQ2xhc3MoXCJqcy1tZW51TG9naW5DbG9zZVwiKVxuICAgIH1cbiAgfSlcbn1cbi8vZmluIEZ1bmNpb25hbGlkYWQgZGVzcGxlZ2FibGUgZGUgaW5pY2lhciBzZXNpb25cbi8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vXG5cbi8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vXG4vL0Z1bmNpb25hbGlkYWQgbWVudSBvZmYgY2FudmFzXG5cbmZ1bmN0aW9uIG1lbnVPZmZDYW52YXMoKSB7XG5cbiAgJChcIi5tZW51LWdyYWxfX2J1cmdlclwiKS5jbGljayhmdW5jdGlvbiAoKSB7XG4gICAgJChcIi5tZW51LXNlYXJjaFwiKS5yZW1vdmVDbGFzcyhcImpzLW1lbnVTZWFyY2hPcGVuXCIpXG4gICAgJChcIi5tZW51LXNlYXJjaFwiKS5hZGRDbGFzcyhcImpzLW1lbnVTZWFyY2hDbG9zZVwiKVxuICAgICQoXCIuc3VibWVudS10YWJzX19saXN0XCIpLnJlbW92ZUNsYXNzKFwianMtbWVudUxvZ2luT3BlblwiKVxuICAgICQoXCIuc3VibWVudS10YWJzX19saXN0XCIpLmFkZENsYXNzKFwianMtbWVudUxvZ2luQ2xvc2VcIilcblxuICAgIGlmICgkKHRoaXMpLmhhc0NsYXNzKFwianMtbWVudUNsb3NlXCIpICYmICgkKFwiLm1lbnUtZ3JhbF9fbGlzdFwiKS5oYXNDbGFzcyhcImpzLW1lbnVDbG9zZVwiKSkpIHtcbiAgICAgICQodGhpcykucmVtb3ZlQ2xhc3MoXCJqcy1tZW51Q2xvc2VcIilcbiAgICAgICQodGhpcykuYWRkQ2xhc3MoXCJqcy1tZW51T3BlblwiKVxuICAgICAgJChcIi5tZW51LWdyYWxfX2xpc3RcIikucmVtb3ZlQ2xhc3MoXCJqcy1tZW51Q2xvc2VcIilcbiAgICAgICQoXCIubWVudS1ncmFsX19saXN0XCIpLmFkZENsYXNzKFwianMtbWVudU9wZW5cIilcbiAgICB9IGVsc2Uge1xuICAgICAgJCh0aGlzKS5yZW1vdmVDbGFzcyhcImpzLW1lbnVPcGVuXCIpXG4gICAgICAkKHRoaXMpLmFkZENsYXNzKFwianMtbWVudUNsb3NlXCIpXG4gICAgICAkKFwiLm1lbnUtZ3JhbF9fbGlzdFwiKS5yZW1vdmVDbGFzcyhcImpzLW1lbnVPcGVuXCIpXG4gICAgICAkKFwiLm1lbnUtZ3JhbF9fbGlzdFwiKS5hZGRDbGFzcyhcImpzLW1lbnVDbG9zZVwiKVxuICAgIH1cbiAgfSlcbn1cbi8vZmluIEZ1bmNpb25hbGlkYWQgbWVudSBvZmYgY2FudmFzXG4vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL1xuXG4vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL1xuLy9GdW5jaW9uYWxpZGFkIGRyb3Bkb3duIG1lbnVcbmZ1bmN0aW9uIGRyb3Bkb3duTWVudSgpIHtcbiAgJChcIi5qcy1tZW51R3JhbEJ0blwiKS5jbGljayhmdW5jdGlvbiAoKSB7XG4gICAgdmFyIG1lbnVJdGVtQWN0aXZlID0gJCh0aGlzKS5wYXJlbnRzKFwiLm1lbnUtZ3JhbF9faXRlbVwiKVxuICAgIHZhciBoZWlnaHRNZW51SXRlbSA9ICQodGhpcykuc2libGluZ3MoXCIuc3VibWVudS1ncmFsX19saXN0XCIpLm91dGVySGVpZ2h0KClcbiAgICBpZiAoJCh3aW5kb3cpLndpZHRoKCkgPCA3NjgpIHtcbiAgICAgIGlmIChtZW51SXRlbUFjdGl2ZS5oYXNDbGFzcyhcImpzLW1lbnVHcmFsQ2xvc2VcIikpIHtcbiAgICAgICAgbWVudUl0ZW1BY3RpdmUuY3NzKFwiaGVpZ2h0XCIsIGhlaWdodE1lbnVJdGVtICsgNDApXG4gICAgICAgIG1lbnVJdGVtQWN0aXZlLnJlbW92ZUNsYXNzKFwianMtbWVudUdyYWxDbG9zZVwiKVxuICAgICAgICBtZW51SXRlbUFjdGl2ZS5hZGRDbGFzcyhcImpzLW1lbnVHcmFsT3BlblwiKVxuICAgICAgICBtZW51SXRlbUFjdGl2ZS5zaWJsaW5ncygpLnJlbW92ZUNsYXNzKFwianMtbWVudUdyYWxPcGVuXCIpLmFkZENsYXNzKFwianMtbWVudUdyYWxDbG9zZVwiKS5jc3MoXCJoZWlnaHRcIiwgNDApXG4gICAgICB9IGVsc2Uge1xuICAgICAgICBtZW51SXRlbUFjdGl2ZS5hZGRDbGFzcyhcImpzLW1lbnVHcmFsQ2xvc2VcIilcbiAgICAgICAgbWVudUl0ZW1BY3RpdmUucmVtb3ZlQ2xhc3MoXCJqcy1tZW51R3JhbE9wZW5cIilcbiAgICAgICAgbWVudUl0ZW1BY3RpdmUuY3NzKFwiaGVpZ2h0XCIsIDQwKVxuICAgICAgfVxuICAgIH0gZWxzZSB7XG4gICAgICAkKFwiLnN1Ym1lbnUtdGFic19fbGlzdFwiKS5yZW1vdmVDbGFzcyhcImpzLW1lbnVMb2dpbk9wZW5cIilcbiAgICAgICQoXCIuc3VibWVudS10YWJzX19saXN0XCIpLmFkZENsYXNzKFwianMtbWVudUxvZ2luQ2xvc2VcIilcblxuICAgICAgaWYgKG1lbnVJdGVtQWN0aXZlLmhhc0NsYXNzKFwianMtbWVudUdyYWxDbG9zZVwiKSkge1xuICAgICAgICBtZW51SXRlbUFjdGl2ZS5yZW1vdmVDbGFzcyhcImpzLW1lbnVHcmFsQ2xvc2VcIilcbiAgICAgICAgbWVudUl0ZW1BY3RpdmUuYWRkQ2xhc3MoXCJqcy1tZW51R3JhbE9wZW5cIilcbiAgICAgICAgJCh0aGlzKS5zaWJsaW5ncyhcIi5zdWJtZW51LWdyYWxfX2xpc3RcIikuc2xpZGVEb3duKClcbiAgICAgICAgbWVudUl0ZW1BY3RpdmUuc2libGluZ3MoXCIubWVudS1ncmFsX19pdGVtXCIpLmNoaWxkcmVuKFwiLnN1Ym1lbnUtZ3JhbF9fbGlzdFwiKS5zbGlkZVVwKClcbiAgICAgICAgbWVudUl0ZW1BY3RpdmUuc2libGluZ3MoXCIubWVudS1ncmFsX19pdGVtXCIpLnJlbW92ZUNsYXNzKFwianMtbWVudUdyYWxPcGVuXCIpLmFkZENsYXNzKFwianMtbWVudUdyYWxDbG9zZVwiKVxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgbWVudUl0ZW1BY3RpdmUuYWRkQ2xhc3MoXCJqcy1tZW51R3JhbENsb3NlXCIpXG4gICAgICAgIG1lbnVJdGVtQWN0aXZlLnJlbW92ZUNsYXNzKFwianMtbWVudUdyYWxPcGVuXCIpXG4gICAgICAgICQodGhpcykuc2libGluZ3MoXCIuc3VibWVudS1ncmFsX19saXN0XCIpLnNsaWRlVXAoKVxuICAgICAgfVxuICAgIH1cbiAgfSlcbn1cbi8vZmluIEZ1bmNpb25hbGlkYWQgZHJvcGRvd24gbWVudVxuLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy9cblxuLy9ob3ZlciBtZW51XG4vLyBmdW5jdGlvbiBkcm9wZG93bk1lbnVEZXNrKCkge1xuXG4vLyAgICQoXCIuanMtbWVudUdyYWxDbG9zZVwiKS5ob3ZlcihmdW5jdGlvbiAoKSB7XG4vLyAgICAgJCh0aGlzKS5hZGRDbGFzcyhcImhvbGFcIilcbi8vICAgICAkKHRoaXMpLmNoaWxkcmVuKFwiLnN1Ym1lbnUtZ3JhbF9fbGlzdFwiKS5zbGlkZVRvZ2dsZSgpXG4vLyAgIH0pXG4vLyB9XG4vL2ZpbiBob3ZlciBtZW51XG5cbi8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vXG4vL0Z1bmNpb25hbGlkYWQgZHJvcGRvd24gZm9vdGVyXG5mdW5jdGlvbiBkcm9wZG93bkZvb3RlcigpIHtcbiAgJChcIi5qcy1mb290QnRuXCIpLmNsaWNrKGZ1bmN0aW9uICgpIHtcbiAgICBpZiAoJCh3aW5kb3cpLndpZHRoKCkgPCA3NjgpIHtcbiAgICAgIHZhciBmb290TGlzdEFjdGl2ZSA9ICQodGhpcykuc2libGluZ3MoXCIuZm9vdF9fc3VibGlzdFwiKVxuICAgICAgaWYgKCQodGhpcykuaGFzQ2xhc3MoXCJqcy1mb290SXRlbUNsb3NlXCIpKSB7XG4gICAgICAgIC8vICQoXCIuanMtZm9vdEJ0blwiKS5yZW1vdmVDbGFzcyhcImpzLWZvb3RJdGVtT3BlblwiKVxuICAgICAgICAkKHRoaXMpLnJlbW92ZUNsYXNzKFwianMtZm9vdEl0ZW1DbG9zZVwiKVxuICAgICAgICAkKHRoaXMpLmFkZENsYXNzKFwianMtZm9vdEl0ZW1PcGVuXCIpXG4gICAgICAgIGZvb3RMaXN0QWN0aXZlLnNsaWRlVG9nZ2xlKClcbiAgICAgICAgJChcImh0bWwsIGJvZHlcIikuYW5pbWF0ZSh7XG4gICAgICAgICAgICBzY3JvbGxUb3A6ICQodGhpcykub2Zmc2V0KCkudG9wIC0gNTBcbiAgICAgICAgICB9LFxuICAgICAgICAgIDUwMFxuICAgICAgICApXG4gICAgICB9IGVsc2Uge1xuICAgICAgICBmb290TGlzdEFjdGl2ZS5zbGlkZVRvZ2dsZSgpXG4gICAgICAgICQodGhpcykucmVtb3ZlQ2xhc3MoXCJqcy1mb290SXRlbU9wZW5cIilcbiAgICAgICAgJCh0aGlzKS5hZGRDbGFzcyhcImpzLWZvb3RJdGVtQ2xvc2VcIilcbiAgICAgIH1cbiAgICB9XG4gIH0pXG59XG4vL2ZpbiBGdW5jaW9uYWxpZGFkIGRyb3Bkb3duIGZvb3RlclxuLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy9cblxuLy9GdW5jaW9uYWxpZGFkIFNpZGUgbWVudVxuZnVuY3Rpb24gc2lkZU1lbnUoKSB7XG4gICQoJy5qcy1idG5DYXJ0JykuY2xpY2soZnVuY3Rpb24gKCkge1xuICAgIFxuICAgIGlmICgkKCcubmF2LXNpZGUnKS5oYXNDbGFzcygnanMtc2xpZGVPdXRvZlZpZXcnKSkge1xuICAgICAgaWYod2luZG93LnNjcm9sbFkgPCAxOTApe1xuICAgICAgICBjb25zdCB0b3AgPSAxODAgLSB3aW5kb3cuc2Nyb2xsWVxuICAgICAgICAkKCcubmF2LXNpZGUnKS5jc3MoJ3RvcCcsIHRvcCArICdweCcpXG4gICAgICB9IGVsc2Uge1xuICAgICAgICAkKCcubmF2LXNpZGUnKS5jc3MoJ3RvcCcsICcwJylcbiAgICAgIH1cblxuICAgICAgJCgnLm5hdi1zaWRlJykucmVtb3ZlQ2xhc3MoJ2pzLXNsaWRlT3V0b2ZWaWV3JylcbiAgICAgICQoJy5uYXYtc2lkZScpLmFkZENsYXNzKCdqcy1zbGlkZVRvVmlldycpXG4gICAgfSBlbHNlIHtcbiAgICAgICQoJy5uYXYtc2lkZScpLmFkZENsYXNzKCdqcy1zbGlkZU91dG9mVmlldycpXG4gICAgICAkKCcubmF2LXNpZGUnKS5yZW1vdmVDbGFzcygnanMtc2xpZGVUb1ZpZXcnKVxuICAgIH1cbiAgfSlcbiAgJCgnLmpzLXNpZGVDbG9zZScpLmNsaWNrKGZ1bmN0aW9uICgpIHtcbiAgICAkKCcubmF2LXNpZGUnKS5hZGRDbGFzcygnanMtc2xpZGVPdXRvZlZpZXcnKVxuICAgICQoJy5uYXYtc2lkZScpLnJlbW92ZUNsYXNzKCdqcy1zbGlkZVRvVmlldycpXG4gIH0pXG59XG4kKHdpbmRvdykuc2Nyb2xsKGZ1bmN0aW9uICgpIHtcbiAgaWYgKCQoJy5uYXYtc2lkZScpLmhhc0NsYXNzKCdqcy1zbGlkZVRvVmlldycpICYmIHdpbmRvdy5zY3JvbGxZIDwgMTkwKXtcbiAgICBjb25zdCB0b3AgPSAxODAgLSB3aW5kb3cuc2Nyb2xsWVxuICAgICQoJy5uYXYtc2lkZScpLmNzcygndG9wJywgdG9wICsgJ3B4JylcbiAgfSBlbHNlIHtcbiAgICAkKCcubmF2LXNpZGUnKS5jc3MoJ3RvcCcsICcwJylcbiAgfVxufSlcbi8vIEZ1bmNpb25hbGlkYWQgY29udGVvIHkgYWN0dWFsaXphY2nDs24gbnVtZXJvIGl0ZW1zIGRlIHNsaWRlIG1lbnVcbmZ1bmN0aW9uIHVwZGF0ZUNvdW50SXRlbXNDYXJ0ICgpIHtcbiAgY29uc3QgY291bnQgPSAkKCcuanMtZGVsZXRlSXRlbUNhcnQnKS5sZW5ndGhcbiAgY29uc3QgdGV4dFNsaWRlTWVudSA9IGNvdW50ID09PSAxID8gJ2VxdWlwbyBhw7FhZGlkbycgOiAnZXF1aXBvcyBhw7FhZGlkb3MnXG4gICQoJy5uYXYtc2lkZV9fdGl0bGUnKS50ZXh0KGAke2NvdW50fSAke3RleHRTbGlkZU1lbnV9IGFsIGNhcnJpdG9gKVxuICAkKCcuYnRuLWNpcmNsZS1zbWFsbCcpLnRleHQoY291bnQpXG59XG5cbi8vIEZ1bmNpb25hbGlkYWQgZWxpbWluYXIgaXRlbXMgZGUgc2xpZGUgbWVudVxuZnVuY3Rpb24gZGVsZXRlSXRlbUNhcnQgKGVsZW0pIHtcbiAgJChlbGVtKS5wYXJlbnQoKS5wYXJlbnQoKS5yZW1vdmUoKVxuICB1cGRhdGVDb3VudEl0ZW1zQ2FydCgpXG59XG5cbiQoJy5qcy1kZWxldGVJdGVtQ2FydCcpLm9uKCdjbGljaycsIGZ1bmN0aW9uICgpIHtcbiAgZGVsZXRlSXRlbUNhcnQodGhpcylcbn0pXG5cbiQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uICgpIHtcbiAgLy9GdW5jaW9uYWxpZGFkIGRlc3BsZWdhYmxlIGRlIHNlYXJjaCBlbiBtb2JpbGVcbiAgbWVudVNlYXJjaE1vYigpXG4gIC8vZHJvcGRvd24gbGlzdFxuICBkcm9wZG93bkxpc3QoKVxuICAvL21lbnUgbG9naW9uIC0gZGVzcGxlZ2FibGVcbiAgbWVudUxvZ2luKClcbiAgLy9GdW5jaW9uYWxpZGFkIG1lbnUgb2ZmIGNhbnZhc1xuICBtZW51T2ZmQ2FudmFzKClcbiAgLy9GdW5jaW9uYWxpZGFkIGRyb3Bkb3duIG1lbnVcbiAgZHJvcGRvd25NZW51KClcbiAgLy9GdW5jaW9uYWxpZGFkIGRyb3Bkb3duIGZvb3RlclxuICBkcm9wZG93bkZvb3RlcigpXG4gIC8vIGRyb3Bkb3duTWVudURlc2soKVxuICBzaWRlTWVudSgpXG4gIC8vIEFjdHVhbGl6YSBudW1lcm8gZGUgaXRlbXMgZW4gZWwgY2Fycml0b1xuICB1cGRhdGVDb3VudEl0ZW1zQ2FydCgpXG59KVxuXG4kKHdpbmRvdykucmVzaXplKGZ1bmN0aW9uICgpIHtcbiAgLy9GdW5jaW9uYWxpZGFkIGRlc3BsZWdhYmxlIGRlIHNlYXJjaCBlbiBtb2JpbGVcbiAgJChcIi5qcy1idG5TZWFyY2hcIikudW5iaW5kKFwiY2xpY2tcIilcbiAgbWVudVNlYXJjaE1vYigpXG4gIC8vZHJvcGRvd24gbGlzdFxuICAkKFwiLmpzLWJ0bl9faGlkZGVuXCIpLnVuYmluZChcImNsaWNrXCIpXG4gIGRyb3Bkb3duTGlzdCgpXG4gIC8vbWVudSBsb2dpb24gLSBkZXNwbGVnYWJsZVxuICAkKFwiLmpzLW1lbnVMb2dpblwiKS51bmJpbmQoXCJjbGlja1wiKVxuICBtZW51TG9naW4oKVxuICAkKFwiLm1lbnUtZ3JhbF9fYnVyZ2VyXCIpLnVuYmluZChcImNsaWNrXCIpXG4gIC8vRnVuY2lvbmFsaWRhZCBtZW51IG9mZiBjYW52YXNcbiAgbWVudU9mZkNhbnZhcygpXG4gIC8vRnVuY2lvbmFsaWRhZCBkcm9wZG93biBtZW51XG4gICQoXCIuanMtbWVudUdyYWxCdG5cIikudW5iaW5kKFwiY2xpY2tcIilcbiAgaWYgKCQod2luZG93KS53aWR0aCgpIDwgNzY4KSB7XG4gICAgJChcIi5zdWJtZW51LWdyYWxfX2xpc3RcIikuc2xpZGVEb3duKClcbiAgfSBlbHNlIHtcbiAgICAkKFwiLnN1Ym1lbnUtZ3JhbF9fbGlzdFwiKS5zbGlkZVVwKClcbiAgICAkKFwiLm1lbnUtZ3JhbF9faXRlbVwiKS5yZW1vdmVDbGFzcyhcImpzLW1lbnVHcmFsT3BlblwiKS5hZGRDbGFzcyhcImpzLW1lbnVHcmFsQ2xvc2VcIilcbiAgfVxuICBkcm9wZG93bk1lbnUoKVxuICAvL0Z1bmNpb25hbGlkYWQgZHJvcGRvd24gZm9vdGVyXG4gICQoXCIuanMtZm9vdEJ0blwiKS51bmJpbmQoXCJjbGlja1wiKVxuICBkcm9wZG93bkZvb3RlcigpXG4gIC8vIGRyb3Bkb3duTWVudURlc2soKVxufSlcbiIsIiQoJy5tb2RhbF9fY2xvc2UnKS5jbGljaygoKSA9PiB7ICQoJy5tb2RhbF9fdmlldycpLnJlbW92ZUNsYXNzKCdtb2RhbF9fdmlldycpIH0pXG4iLCIkKCcjcGF5bWVudE1ldGhvZDEnKS5zaG93KCk7XG4kKCcjcGF5bWVudE1ldGhvZDInKS5oaWRlKCk7XG5cbiQoJyNidG5QYXltZW50TWV0aG9kMScpLmNsaWNrKGZ1bmN0aW9uICgpIHtcbiAgJCgnI3BheW1lbnRNZXRob2QxJykudG9nZ2xlKCdmYXN0Jyk7XG4gICQoJyNwYXltZW50TWV0aG9kMicpLnRvZ2dsZSgnZmFzdCcpO1xuICAkKFwiaVwiKS50b2dnbGVDbGFzcyhcImktYXJyb3ctZG93biBpLWFycm93LXVwXCIpO1xufSk7XG5cbiQoJyNidG5QYXltZW50TWV0aG9kMicpLmNsaWNrKGZ1bmN0aW9uICgpIHtcbiAgJCgnI3BheW1lbnRNZXRob2QxJykudG9nZ2xlKCdmYXN0Jyk7XG4gICQoJyNwYXltZW50TWV0aG9kMicpLnRvZ2dsZSgnZmFzdCcpO1xuICAkKFwiaVwiKS50b2dnbGVDbGFzcyhcImktYXJyb3ctdXAgaS1hcnJvdy1kb3duXCIpO1xufSk7XG4iLCIvL0luaWNpbyBmdW5jaW9uYWxpZGFkIFtEZXRhbGxlIGRlIHByb2R1Y3RvIC0gQkFSUkEgU1RJQ0tZXSA6IEZ1bmNpb25hbGlkYWQgc3RpY2t5IHBhcmEgZGVza3RvcFxuaWYgKCQoXCIuYmFuZCAubGlzdC1zdGlja3lcIilbMF0pIHtcbiAgLy9JZGVudGlmaWNhIHNpIGVzdGEgZW4gcGFnaW5hIGRldGFsbGUgcHJvZHVjdG9cbiAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoXCJzY3JvbGxcIiwgKCkgPT4ge1xuICAgIC8vQcOxYWRlIGV2ZW50byBhbCBzY3Jvb2xcbiAgICBlbCA9ICQoXCJzZWN0aW9uOm50aC1jaGlsZCgxKVwiKTsgLy9JZGVudGlmaWNhIHkgZ3VhcmRhIGVsIHByaW1lciBzZWN0aW9uIHF1ZSBlcyBkZXNwdWVzIGRlIGVzdGUgZG9uZSBhcGFyZWNlIGxhIGJhcnJhXG4gICAgcG9zID0gZWwuaGVpZ2h0KCkgKyBlbC5wb3NpdGlvbigpLnRvcDsgLy9HdWFyZGEgZWwgdmFsb3IgZGUgbGEgcG9zaWNpw7NuIChhbHRvIGRlbCBkaXYgKyBlbCBhbHRvIGRlIGxhIHBvc2ljacOzbilcbiAgICBpZiAod2luZG93LnNjcm9sbFkgPj0gcG9zIC0gMTcwKSB7XG4gICAgICAvLyBTaSB5YSBzZSBwYXNvIGVsIHByaW1lciBzZWN0aW9uIGHDsWFkZSBsYSBjbGFzc2Ugc3RpY2t5IHNpIG5vIGxhIHF1aXRhXG4gICAgICAkKFwiLmJhbmRcIikuYWRkQ2xhc3MoXCJzdGlja3lcIik7XG4gICAgICAkKFwiLmNvbnRlbnRcIikuYWRkQ2xhc3MoXCJiYW5kLXN0aWNreVwiKTtcbiAgICB9IGVsc2Uge1xuICAgICAgJChcIi5iYW5kXCIpLnJlbW92ZUNsYXNzKFwic3RpY2t5XCIpO1xuICAgICAgJChcIi5jb250ZW50XCIpLnJlbW92ZUNsYXNzKFwiYmFuZC1zdGlja3lcIik7XG4gICAgfVxuICB9KTtcbn1cbi8vRmluIGZ1bmNpb25hbGlkYWQgW0RldGFsbGUgZGUgcHJvZHVjdG8gLSBCQVJSQSBTVElDS1ldIDogRnVuY2lvbmFsaWRhZCBzdGlja3kgcGFyYSBkZXNrdG9wXG5cbi8vSW5pY2lvIGZ1bmNpb25hbGlkYWQgQm90b24gY2Fycml0byBmaXhlZCBib3R0b21cbmZ1bmN0aW9uIGJ0bkZpeGVkQm90dG9tKCkge1xuICBpZiAoJChcIi5idG4tY2lyY2xlLmhlYWRfX2l0ZW1cIilbMF0gJiYgJCh3aW5kb3cpLndpZHRoKCkgPiA3NjgpIHtcbiAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcihcInNjcm9sbFwiLCAoKSA9PiB7XG4gICAgICAvL0HDsWFkZSBldmVudG8gYWwgc2Nyb29sXG4gICAgICBhID0gJChcIi5iYW5kX3NoYWRvd1wiKS5oZWlnaHQoKTtcbiAgICAgIGlmICh3aW5kb3cuc2Nyb2xsWSA+PSBhKSB7XG4gICAgICAgIC8vIFNpIHlhIHNlIHBhc28gZWwgcHJpbWVyIHNlY3Rpb24gYcOxYWRlIGxhIGNsYXNzZSBzdGlja3kgc2kgbm8gbGEgcXVpdGFcbiAgICAgICAgJChcIi5qcy1idG5DYXJ0LmhlYWRfX2l0ZW1cIikuYWRkQ2xhc3MoXCJidG4tY2lyY2xlLUZpeGVkLWJvdHRvbVwiKTtcbiAgICAgICAgJCgnLmpzLWJ0bkNhcnQnKS5hZGRDbGFzcygnYW5pbWF0ZWQgYm91bmNlSW5VcCcpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgJChcIi5qcy1idG5DYXJ0LmhlYWRfX2l0ZW1cIikucmVtb3ZlQ2xhc3MoXCJidG4tY2lyY2xlLUZpeGVkLWJvdHRvbVwiKTtcbiAgICAgICAgJCgnLmpzLWJ0bkNhcnQnKS5yZW1vdmVDbGFzcygnYW5pbWF0ZWQgYm91bmNlSW5VcCcpO1xuICAgICAgfVxuICAgIH0pO1xuICB9XG59XG4vL0ZpbiBmdW5jaW9uYWxpZGFkIEJvdG9uIGNhcnJpdG8gZml4ZWQgYm90dG9tXG5cbiQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uICgpIHtcbiAgLy8gQm90w7NuIGNhcnJpdG8gZGUgY29tcHJhIHNlIGZpamEgZGVzZGUgNzY4cHhcbiAgLy8gc2FsYSBhIHBhcnRpciBkZWwgYWx0byBkZSB1bmEgc2VjY2nDs25cbiAgYnRuRml4ZWRCb3R0b20oKTtcbn0pO1xuIiwidmFyIHByaWNlPSQoXCIjcHJpY2VSYW5nZVwiKS52YWwoKTskKFwiI3ByaWNlXCIpLnRleHQocHJpY2UpLCQoXCIjcHJpY2VSYW5nZVwiKS5hc1JhbmdlKHtyYW5nZTohMCxsaW1pdDohMCxzdGVwOjEsbWluOjAsbWF4OjZlNSxvbkNoYW5nZTpmdW5jdGlvbigpe3ZhciBlPSQoXCIjcHJpY2VSYW5nZVwiKS52YWwoKTskKFwiI3ByaWNlXCIpLnRleHQoZSl9fSk7IiwiLy8gU2xpZGUgcmFuZ2VcbiQoJyNwcmljZVJhbmdlJykuYXNSYW5nZSh7XG4gIHJhbmdlOiB0cnVlLFxuICBsaW1pdDogdHJ1ZSxcbiAgc3RlcDogMSxcbiAgbWluOiAwLFxuICBtYXg6IDYwMDAwMCxcbiAgb25DaGFuZ2U6IGZ1bmN0aW9uICh2YWx1ZSkge1xuICAgICQoJyNwcmljZScpLnRleHQoYCQke3ZhbHVlWzBdLnRvTG9jYWxlU3RyaW5nKCdlcy1FUycpfSAtICQke3ZhbHVlWzFdLnRvTG9jYWxlU3RyaW5nKCdlcy1FUycpfWApXG4gIH1cbn0pXG5cbiIsIi8vIEluaWNpbyBDb25maWd1cmFjaW9uIFNsaWRlcnNcbi8vIEluaWNpbyBDb25maWd1cmFjaW9uIG1lbnUgY29tbyBjYXJydXNlbFxudmFyIG9wdGlvbnMgPSB7XG4gIGhvcml6b250YWw6IDEsXG4gIGl0ZW1OYXY6IFwiYmFzaWNcIixcbiAgc3BlZWQ6IDMwMCxcbiAgbW91c2VEcmFnZ2luZzogMSxcbiAgdG91Y2hEcmFnZ2luZzogMVxufTtcblxuJChcIi5tZW51XCIpLnNseShvcHRpb25zKTtcbiQod2luZG93KS5yZXNpemUoZnVuY3Rpb24gKGUpIHtcbiAgJChcIi5tZW51XCIpLnNseShcInJlbG9hZFwiKTtcbn0pO1xuXG5cbiQoXCIubWFpbi1zbGlkZXJfX2l0ZW1cIikuc2xpY2soeyAvL1NsaWRlciBkZXRhbGxlIHByb2R1Y3RvXG4gIGFycm93czogZmFsc2UsXG4gIGluZmluaXRlOiBmYWxzZSxcbiAgZG90czogdHJ1ZSxcbiAgYXV0b3BsYXlTcGVlZDogNjAwMCxcbiAgYXV0b3BsYXk6IHRydWUsXG4gIHBhdXNlT25Gb2N1czogZmFsc2UsXG4gIGRvdHNDbGFzczogJ3NsaWNrLWRvdHMgc2xpY2stZG90c19zdGF0aWMnXG59KTtcblxuLy9TbGlkZXIgbGlzdGEgQWNjZXNvcmlvcyBbY2FydC1zaG93Y2FzZS5wdWddXG4kKFwiLnNsaWRlci1zaG93Y2FzZV9fbGlzdFwiKS5zbGljayh7IC8vU2xpZGVyIGxpc3RhIGRlIHByb2R1Y3Rvc1xuICBzbGlkZXNUb1Nob3c6IDMsXG4gIHNsaWRlc1RvU2Nyb2xsOiAzLFxuICBwYXVzZU9uRm9jdXM6IGZhbHNlLFxuICBwcmV2QXJyb3c6ICQoXCIucHJldlwiKSxcbiAgbmV4dEFycm93OiAkKFwiLm5leHRcIiksXG4gIHJlc3BvbnNpdmU6IFt7XG4gICAgICBicmVha3BvaW50OiA0MjAsXG4gICAgICBzZXR0aW5nczoge1xuICAgICAgICBzbGlkZXNUb1Nob3c6IDEsXG4gICAgICAgIHNsaWRlc1RvU2Nyb2xsOiAxLFxuICAgICAgfVxuICAgIH0sXG4gICAge1xuICAgICAgYnJlYWtwb2ludDogNzg1LFxuICAgICAgc2V0dGluZ3M6IHtcbiAgICAgICAgc2xpZGVzVG9TaG93OiAyLFxuICAgICAgICBzbGlkZXNUb1Njcm9sbDogMixcbiAgICAgIH1cbiAgICB9LFxuICBdXG59KTtcblxuJChcIi5oZXJvLWJhbm5lclwiKS5zbGljayh7IC8vU2xpZGVyIGJhbm5lciBwcmluY2lwYWxcbiAgc2xpZGVzVG9TaG93OiAxLFxuICBzbGlkZXNUb1Njcm9sbDogMSxcbiAgbW9iaWxlRmlyc3Q6IHRydWUsXG4gIGFycm93czogZmFsc2UsXG4gIGluZmluaXRlOiBmYWxzZSxcbiAgZG90czogdHJ1ZSxcbiAgYXV0b3BsYXk6IHRydWUsXG4gIGF1dG9wbGF5U3BlZWQ6IDYwMDAsXG4gIGRvdHNDbGFzczogJ3NsaWNrLWRvdHMgc2xpY2stZG90c19hbmltYXRlZCcsXG59KTtcblxuLy9TbGlkZXIgQ2hlY2tib3ggW2JvYXJkLXRhYnMucHVnXVxuJChcIi5ib2FyZC10YWJzX19zbGlkZXJcIikuc2xpY2soe1xuICBzbGlkZXNUb1Nob3c6IDEsXG4gIHNsaWRlc1RvU2Nyb2xsOiAxLFxuICBjZW50ZXJNb2RlOiB0cnVlLFxuICBtb2JpbGVGaXJzdDogdHJ1ZSxcbiAgY2VudGVyUGFkZGluZzogJzIwcHgnLFxuICBwYXVzZU9uRm9jdXM6IGZhbHNlLFxuICBwcmV2QXJyb3c6ICQoXCIubGVmdFwiKSxcbiAgbmV4dEFycm93OiAkKFwiLnJpZ2h0XCIpLFxuICByZXNwb25zaXZlOiBbe1xuICAgICAgYnJlYWtwb2ludDogNDgwLFxuICAgICAgc2V0dGluZ3M6IHtcbiAgICAgICAgc2xpZGVzVG9TaG93OiAzLFxuICAgICAgICBzbGlkZXNUb1Njcm9sbDogMSxcbiAgICAgICAgY2VudGVyTW9kZTogdHJ1ZVxuICAgICAgfVxuICAgIH0sXG4gICAge1xuICAgICAgYnJlYWtwb2ludDogNjYwLFxuICAgICAgc2V0dGluZ3M6IHtcbiAgICAgICAgc2xpZGVzVG9TaG93OiAzLFxuICAgICAgICBzbGlkZXNUb1Njcm9sbDogMSxcbiAgICAgICAgY2VudGVyTW9kZTogdHJ1ZVxuICAgICAgfVxuICAgIH0sXG4gICAge1xuICAgICAgYnJlYWtwb2ludDogODkwLFxuICAgICAgc2V0dGluZ3M6ICd1bnNsaWNrJ1xuICAgIH1cbiAgXVxufSk7XG5cbi8vU2xpZGVyIFJhZGlvYnV0dG9ucyBbbGlzdC1yYWRpby5wdWddXG5mdW5jdGlvbiBsaXN0U2xpZGVyKCkge1xuICBpZiAoJCh3aW5kb3cpLndpZHRoKCkgPCA4MjkpIHtcbiAgICAkKFwiLmxpc3QtcmFkaW9fX2xpc3RcIikuc2xpY2soe1xuICAgICAgYXJyb3dzOiB0cnVlLFxuICAgICAgc2xpZGVzVG9TaG93OiAxLFxuICAgICAgc2xpZGVzVG9TY3JvbGw6IDEsXG4gICAgICBwYXVzZU9uRm9jdXM6IGZhbHNlLFxuICAgICAgcHJldkFycm93OiAkKFwiLmxlZnRcIiksXG4gICAgICBuZXh0QXJyb3c6ICQoXCIucmlnaHRcIilcbiAgICB9KTtcbiAgfSBlbHNlIHtcbiAgICAkKCcubGlzdC1yYWRpb19fbGlzdCcpLnNsaWNrKCd1bnNsaWNrJyk7XG4gIH1cbn1cblxubGlzdFNsaWRlcigpO1xuXG52YXIgcjtcblxuJCh3aW5kb3cpLnJlc2l6ZShmdW5jdGlvbiAoKSB7XG4gIC8vJCgnLmxpc3QtcmFkaW9fX2xpc3QnKVswXS5zbGljay5yZWZyZXNoKCk7XG4gICQoJy5saXN0LXJhZGlvX19saXN0Jykuc2xpY2soJ3Jlc2l6ZScpO1xuICAkKCcuYm9hcmQtdGFic19fc2xpZGVyJykuc2xpY2soJ3Jlc2l6ZScpO1xuICBjbGVhclRpbWVvdXQocik7XG4gIC8vY29uc29sZS5sb2cocik7XG4gIHIgPSBzZXRUaW1lb3V0KGxpc3RTbGlkZXIsIDUwMDApO1xufSk7XG5cbi8vU2xpZGVyIERldGFsbGUgZGUgVGVybWluYWwgW3NsaWRlci1kZXRhaWwucHVnXVxuJChcIi5qcy1zbGlkZURldGFpbFwiKS5zbGljayh7XG4gIC8vU2xpZGVyIGxpc3RhIGRlIHByb2R1Y3Rvc1xuICBzbGlkZXNUb1Nob3c6IDIsXG4gIHNsaWRlc1RvU2Nyb2xsOiAyLFxuICBwYXVzZU9uRm9jdXM6IGZhbHNlLFxuICBwcmV2QXJyb3c6ICQoXCIucHJldlwiKSxcbiAgbmV4dEFycm93OiAkKFwiLm5leHRcIiksXG4gIHJlc3BvbnNpdmU6IFt7XG4gICAgYnJlYWtwb2ludDogNjQwLFxuICAgIHNldHRpbmdzOiB7XG4gICAgICBzbGlkZXNUb1Nob3c6IDEsXG4gICAgICBzbGlkZXNUb1Njcm9sbDogMSxcbiAgICB9LFxuICB9XVxufSk7XG5cbi8vU2xpZGVyIEhlcm8gSG9tZSBbaG9tZS5odG1sXVxuJChcIi5qcy1zbGlkZXJIb21lMVwiKS5zbGljayh7IC8vU2xpZGVyIGJhbm5lciBwcmluY2lwYWxcbiAgc2xpZGVzVG9TaG93OiAxLFxuICBzbGlkZXNUb1Njcm9sbDogMSxcbiAgbW9iaWxlRmlyc3Q6IHRydWUsXG4gIGFycm93czogZmFsc2UsXG4gIGluZmluaXRlOiBmYWxzZSxcbiAgZG90czogdHJ1ZSxcbiAgYXV0b3BsYXk6IHRydWUsXG4gIGF1dG9wbGF5U3BlZWQ6IDYwMDAsXG4gIGRvdHNDbGFzczogJ3NsaWNrLWRvdHMgc2xpY2stZG90c19hbmltYXRlZCcsXG59KTtcblxuLy9TbGlkZXIgQnJhbmRzIFtob21lLmh0bWxdXG4kKFwiLmpzLXNsaWRlckhvbWUyXCIpLnNsaWNrKHtcbiAgY2VudGVyTW9kZTogdHJ1ZSxcbiAgY2VudGVyUGFkZGluZzogJzIwMHB4JyxcbiAgc2xpZGVzVG9TaG93OiAzLFxuICBzcGVlZDogMTUwMCxcbiAgaW5kZXg6IDIsXG4gIGFycm93czogdHJ1ZSxcbiAgcHJldkFycm93OiAkKFwiLmktcHJldlwiKSxcbiAgbmV4dEFycm93OiAkKFwiLmktbmV4dFwiKSxcbiAgcmVzcG9uc2l2ZTogW3tcbiAgICBicmVha3BvaW50OiA3NjgsXG4gICAgc2V0dGluZ3M6IHtcbiAgICAgIGFycm93czogZmFsc2UsXG4gICAgICBjZW50ZXJNb2RlOiB0cnVlLFxuICAgICAgY2VudGVyUGFkZGluZzogJzEwMHB4JyxcbiAgICAgIHNsaWRlc1RvU2hvdzogMVxuICAgICAgLy9zbGlkZXNUb1Nob3c6IDNcbiAgICB9XG4gIH0sIHtcbiAgICBicmVha3BvaW50OiA0ODAsXG4gICAgc2V0dGluZ3M6IHtcbiAgICAgIGFycm93czogZmFsc2UsXG4gICAgICBjZW50ZXJNb2RlOiB0cnVlLFxuICAgICAgY2VudGVyUGFkZGluZzogJzcwcHgnLFxuICAgICAgc2xpZGVzVG9TaG93OiAxXG4gICAgfVxuICB9XVxufSk7XG5cblxuLy9TbGlkZXIgZGUgVGVybWluYWxlcyBbaG9tZS5odG1sXVxuJChcIi5qcy1zbGlkZUhvbWUzXCIpLnNsaWNrKHsgLy9TbGlkZXIgbGlzdGEgZGUgcHJvZHVjdG9zXG4gIHNsaWRlc1RvU2hvdzogMyxcbiAgLy9jZW50ZXJNb2RlOiB0cnVlLFxuICBwcmV2QXJyb3c6ICQoXCIucHJldlwiKSxcbiAgbmV4dEFycm93OiAkKFwiLm5leHRcIiksXG4gIHJlc3BvbnNpdmU6IFt7XG4gICAgICBicmVha3BvaW50OiA1NDAsXG4gICAgICBzZXR0aW5nczoge1xuICAgICAgICBzbGlkZXNUb1Nob3c6IDEsXG4gICAgICAgIHNsaWRlc1RvU2Nyb2xsOiAxLFxuICAgICAgfVxuICAgIH0sXG4gICAge1xuICAgICAgYnJlYWtwb2ludDogNzg1LFxuICAgICAgc2V0dGluZ3M6IHtcbiAgICAgICAgc2xpZGVzVG9TaG93OiAyLFxuICAgICAgICBzbGlkZXNUb1Njcm9sbDogMixcbiAgICAgIH1cbiAgICB9LFxuICBdXG59KTtcblxuLy8tIFsuc2xpZGVyX19jYXJkcyBDYXJkcyBkZSBWaXRyaW5hIFByZXBhZ29dXG4kKCcuanMtc2xpZGVDYXJkcycpLnNsaWNrKHtcbiAgY2VudGVyTW9kZTogdHJ1ZSxcbiAgLy9jZW50ZXJQYWRkaW5nOiAnMjBweCcsXG4gIHNsaWRlc1RvU2hvdzogMSxcbiAgc2xpZGVzVG9TY3JvbGw6IDEsXG4gIG1vYmlsZUZpcnN0OiB0cnVlLFxuICBpbmZpbml0ZTogdHJ1ZSxcbiAgYXJyb3dzOiB0cnVlLFxuICByZXNwb25zaXZlOiBbXG4gICAge1xuICAgICAgYnJlYWtwb2ludDogMzIwLFxuICAgICAgc2V0dGluZ3M6IHtcbiAgICAgICAgY2VudGVyTW9kZTogdHJ1ZSxcbiAgICAgICAgY2VudGVyUGFkZGluZzogJzVweCcsXG4gICAgICAgIHNsaWRlc1RvU2hvdzogMSxcbiAgICAgICAgc2xpZGVzVG9TY3JvbGw6IDEsXG4gICAgICAgIGFycm93czogdHJ1ZVxuICAgICAgfVxuICAgIH0sXG4gICAge1xuICAgICAgYnJlYWtwb2ludDogNTQwLFxuICAgICAgc2V0dGluZ3M6IHtcbiAgICAgICAgY2VudGVyTW9kZTogdHJ1ZSxcbiAgICAgICAgY2VudGVyUGFkZGluZzogJzhweCcsXG4gICAgICAgIHNsaWRlc1RvU2hvdzogMixcbiAgICAgICAgc2xpZGVzVG9TY3JvbGw6IDIsXG4gICAgICAgIGFycm93czogdHJ1ZVxuICAgICAgfVxuICAgIH0sXG4gICAge1xuICAgICAgYnJlYWtwb2ludDogNjQwLFxuICAgICAgc2V0dGluZ3M6IHtcbiAgICAgICAgY2VudGVyTW9kZTogdHJ1ZSxcbiAgICAgICAgY2VudGVyUGFkZGluZzogJzhweCcsXG4gICAgICAgIHNsaWRlc1RvU2hvdzogMixcbiAgICAgICAgc2xpZGVzVG9TY3JvbGw6IDIsXG4gICAgICAgIGFycm93czogdHJ1ZVxuICAgICAgfVxuICAgIH0sXG4gICAge1xuICAgICAgYnJlYWtwb2ludDogNzY4LFxuICAgICAgc2V0dGluZ3M6IHtcbiAgICAgICAgY2VudGVyTW9kZTogZmFsc2UsXG4gICAgICAgIHNsaWRlc1RvU2hvdzogMyxcbiAgICAgICAgc2xpZGVzVG9TY3JvbGw6IDMsXG4gICAgICAgIGFycm93czogdHJ1ZVxuICAgICAgfVxuICAgIH0sXG4gICAge1xuICAgICAgYnJlYWtwb2ludDogMTAyNCxcbiAgICAgIHNldHRpbmdzOiB7XG4gICAgICAgIGNlbnRlck1vZGU6IGZhbHNlLFxuICAgICAgICBzbGlkZXNUb1Nob3c6IDQsXG4gICAgICAgIHNsaWRlc1RvU2Nyb2xsOiA0LFxuICAgICAgICBhcnJvd3M6IHRydWUsXG4gICAgICAgIGFkYXB0aXZlSGVpZ2h0OiB0cnVlLFxuICAgICAgfVxuICAgIH1cbiAgXVxufSk7XG4iLCIvLy8vLy8vLy8vLy8vLy8vLy8vLy8vL1RBQlMgLVtDYXIgQ2hlY2tvdXQgcGFzbyAyXS8vLy8vLy8vLy8vLy8vLy8vLy9cbi8vRXN0YWRvIGN1YW5kbyBjYXJnYSBsYSBww6FnaW5hXG4kKFwiLmJvYXJkLXRhYnNfX2NvbnRcIikuaGlkZSgpOyAvL2NvbnRlbmlkbyBvY3VsdG9cbiQoXCIuYm9hcmQtdGFic19fbGluazpmaXJzdFwiKS5hZGRDbGFzcyhcImpzLXRhYl9fbGlua0FjdGl2ZVwiKS5zaG93KCk7IC8vVGFiIGFjdGl2YVxuJChcIi5ib2FyZC10YWJzX19jb250OmZpcnN0XCIpLnNob3coKTsgLy9Db250ZW5pZG8gYWN0aXZvIGEgcHJpbWVyYSB2aXN0YVxuXG4vLyAvL0Z1bmNpw7NuIHBhcmEgZWwgZXZlbnRvIGNsaWNrIGVuIGVsIGJvdMOzbiBhY3Rpdm9cbiQoXCIuYm9hcmQtdGFic19fbGlua1wiKS5jbGljayhmdW5jdGlvbiAoKSB7XG5cbiAgJChcIi5ib2FyZC10YWJzX19saW5rXCIpLnJlbW92ZUNsYXNzKFwianMtdGFiX19saW5rQWN0aXZlXCIpOyAvL1JlbW92ZSBhbnkgXCJhY3RpdmVcIiBjbGFzc1xuICAkKHRoaXMpLmFkZENsYXNzKFwianMtdGFiX19saW5rQWN0aXZlXCIpOyAvL0FkZCBcImFjdGl2ZVwiIGNsYXNzIHRvIHNlbGVjdGVkIHRhYlxuICAkKFwiLmJvYXJkLXRhYnNfX2NvbnRcIikuaGlkZSgpOyAvL0hpZGUgYWxsIHRhYiBjb250ZW50XG5cbiAgdmFyIGFjdGl2ZVRhYiA9ICQodGhpcykuYXR0cihcImhyZWZcIik7IC8vTGVlIGVsIHZhbG9yIGRlbCBhdHJpYnV0byBocmVmIHBhcmEgaWRlbnRpZmljYXIgbGEgcGVzdGHDsWEgYWN0aXZhXG4gIC8vY29uc29sZS5sb2coYWN0aXZlVGFiKTtcbiAgJChhY3RpdmVUYWIpLmZhZGVJbigpOyAvL0ZhZGUgaW4gcGFyYSB2aXNpYmlsaXphciBlbCBjb250ZW5pZG9cbiAgcmV0dXJuIGZhbHNlO1xufSk7XG4vLy8vIyBlbmQgVEFCUyAtW0NhciBDaGVja291dCBwYXNvIDJdLy8vLy8vL1xuLy8vVEFCUyBGT1JNIC1bQ2FyIENoZWNrb3V0IHBhc28gMl0vLy8vXG4vLyAkKHdpbmRvdykucmVzaXplKGZ1bmN0aW9uICgpIHtcbi8vICAgaWYgKHRoaXMucmVzaXplVE8pIGNsZWFyVGltZW91dCh0aGlzLnJlc2l6ZVRPKTtcbi8vICAgdGhpcy5yZXNpemVUTyA9IHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xuLy8gICAgICQodGhpcykudHJpZ2dlcigncmVzaXplRW5kJyk7XG4vLyAgIH0sIDUwMCk7XG4vLyAgIGNvbnNvbGUubG9nKHJlc2l6ZUVuZCk7XG4vLyB9KTtcbi8vICQod2luZG93KS5iaW5kKFwicmVzaXplRW5kXCIsIGZ1bmN0aW9uICgpIHtcblxuLy8gICBpZiAoJCh3aW5kb3cpLndpZHRoKCkgPCA3NjgpIHtcbi8vICAgICAkKFwiLmpzLXRhYkNvbnRlbnRcIikuaGlkZSgpO1xuLy8gICAgICQoXCIuanMtdGFiQnRuXCIpLmNsaWNrKGZ1bmN0aW9uICgpIHtcbi8vICAgICAgICQodGhpcykubmV4dChcIi5qcy10YWJDb250ZW50XCIpLnNsaWRlVG9nZ2xlKCk7XG4vLyAgICAgICAkKHRoaXMpLnRvZ2dsZUNsYXNzKFwiZG93blwiKTsgLy9jb25zb2xlLmxvZyh0aGlzKTtcbi8vICAgICB9KTtcbi8vICAgfWVsc2Uge1xuLy8gICAgICQoJy5qcy10YWJDb250ZW50JykuY3NzKCdkaXNwbGF5JywgJ2ZsZXgnKTtcbi8vICAgfVxuLy8gfSk7XG5cblxuLy8gJCgnLmpzLXRhYkJ0bicpLmNsaWNrKGZ1bmN0aW9uICgpIHtcbi8vICAgJCgnLmpzLXRhYkNvbnRlbnQnKS50b2dnbGVDbGFzcygnanMtb3BlbicpO1xuLy8gfSk7XG5cbi8vLyMgVEFCUyBGT1JNIC1bQ2FyIENoZWNrb3V0IHBhc28gMl0vLy8vXG4vLy8vL0RhdGEtc2hlZXRfX2xpc3QgW2NvbXBhcmFkb3JdLy8vLy8vXG4kKFwiLmpzLWJ0bkRhdGFcIikuY2xpY2soZnVuY3Rpb24gKGUpIHtcbiAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAkKFwiLmpzLWxpc3REYXRhXCIpLnNsaWRlVG9nZ2xlKCk7XG59KTtcblxuLy8gUGFuZWwgVGFicyAtIFtwYW5lbC5odG1sXVxuZnVuY3Rpb24gcGFuZWxUYWJzKCkge1xuICAkKCcuanMtdGFiQnRuJykuY2xpY2soZnVuY3Rpb24gKCkge1xuICAgIGlmICgkKHRoaXMpLmhhc0NsYXNzKCdqcy10YWJPcGVuJykpIHtcbiAgICAgICQodGhpcykucmVtb3ZlQ2xhc3MoJ2pzLXRhYk9wZW4nKS5hZGRDbGFzcygnanMtdGFiQ2xvc2UnKTtcbiAgICAgICQodGhpcykuc2libGluZ3MoXCIuanMtdGFiQ29udGVudFwiKS5zbGlkZVVwKCk7XG4gICAgfSBlbHNlIHtcbiAgICAgICQoJy5qcy10YWJCdG4nKS5hZGRDbGFzcygnanMtdGFiQ2xvc2UnKS5yZW1vdmVDbGFzcygnanMtdGFiT3BlbicpO1xuICAgICAgJChcIi5qcy10YWJDb250ZW50XCIpLnNsaWRlVXAoKTtcbiAgICAgICQodGhpcykucmVtb3ZlQ2xhc3MoJ2pzLXRhYkNsb3NlJykuYWRkQ2xhc3MoJ2pzLXRhYk9wZW4nKTtcbiAgICAgICQodGhpcykuc2libGluZ3MoXCIuanMtdGFiQ29udGVudFwiKS5zbGlkZURvd24oKTtcbiAgICB9XG4gIH0pO1xufVxuXG4kKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbiAoKSB7XG4gIC8vIFBhbmVsIFRhYnMgLSBbcGFuZWwuaHRtbF1cbiAgcGFuZWxUYWJzKClcbn0pXG4iLCIvLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL1xuICAvL0luaWNpbyBGdW5jaW9uYWxpZGFkIHRhYnMgeSBEcm9wZG93blxuICAvLyBGdW5jaW9uIHBhcmEgb2J0ZW5lciBlbCBhbHRvIGRlIGxhIHRhYiBhY3RpdmFcbiAgZnVuY3Rpb24gdGFiSGVpZ2h0KCkge1xuICAgIGlmICgkKHdpbmRvdykud2lkdGgoKSA+IDc2OCkge1xuICAgICAgdmFyIGhlaWdodFRhYkFjdGl2ZSA9ICQoXCIuanMtdGFiSXRlbUFjdGl2ZVwiKVxuICAgICAgICAuY2hpbGRyZW4oXCIudGFic19fcGFuZVwiKVxuICAgICAgICAuaGVpZ2h0KCk7XG4gICAgICAkKFwiLnRhYnNcIikuY3NzKFwiaGVpZ2h0XCIsIGhlaWdodFRhYkFjdGl2ZSArIDE0MCk7XG4gICAgICAvL2NvbnNvbGUubG9nKGhlaWdodFRhYkFjdGl2ZSk7XG4gICAgfSBlbHNlIHtcbiAgICAgICQoXCIudGFic1wiKS5jc3MoXCJoZWlnaHRcIiwgXCJpbml0aWFsXCIpO1xuICAgIH1cbiAgfVxuXG4gICQoXCIudGFic19faXRlbSBidXR0b25cIikuY2xpY2soZnVuY3Rpb24oKSB7XG4gICAgY29uc3QgcGFyZW50ID0gdGhpcy5wYXJlbnRFbGVtZW50O1xuICAgIGlmICh3aW5kb3cuaW5uZXJXaWR0aCA8PSA3NjcgJiYgJChwYXJlbnQpLmhhc0NsYXNzKFwianMtdGFiSXRlbUFjdGl2ZVwiKSkge1xuICAgICAgLy9BbCBkYXIgY2xpYyBjaWVycmEgZWwgdGFiIHNpIGVzdGEgYWJpZXJ0byBjdWFuZG8gbGEgcGFudGFsbGEgZXMgbWVub3IgYSA3NjdcbiAgICAgICQocGFyZW50KS5yZW1vdmVDbGFzcyhcImpzLXRhYkl0ZW1BY3RpdmVcIik7XG4gICAgfSBlbHNlIHtcbiAgICAgICQoXCIuanMtdGFiSXRlbUFjdGl2ZVwiKS5yZW1vdmVDbGFzcyhcImpzLXRhYkl0ZW1BY3RpdmVcIik7XG4gICAgICAkKHBhcmVudCkuYWRkQ2xhc3MoXCJqcy10YWJJdGVtQWN0aXZlXCIpO1xuICAgICAgdGFiSGVpZ2h0KCk7XG4gICAgfVxuICB9KTtcblxuICBmdW5jdGlvbiB0YWJzbWVkaWFxdWVyeSh4KSB7XG4gICAgLy9WZXJpZmljYSBhbCByZWRpbWVuc2lvbmFyIGxhIHZlbnRhbmEgeSBhYnJlIG8gY2llcnJhIGxvcyB0YWJzIGNvcnJlc3BvbmRpZW50ZXNcbiAgICBpZiAoIXgubWF0Y2hlcykge1xuICAgICAgLy8gSWYgbWVkaWEgcXVlcnkgbWF0Y2hlc1xuICAgICAgaWYgKFxuICAgICAgICAhJChcIi50YWJzX19pdGVtXCIpLmhhc0NsYXNzKFwianMtdGFiSXRlbUFjdGl2ZVwiKSAmJlxuICAgICAgICAkKFwiLnRhYnNfX2l0ZW1cIikubGVuZ3RoID4gMFxuICAgICAgKSB7XG4gICAgICAgICQoXCIudGFic19faXRlbVwiKVswXS5jbGFzc0xpc3QuYWRkKFwianMtdGFiSXRlbUFjdGl2ZVwiKTtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBjb25zdCBtZWRpYXF1ZXJ5ID0gd2luZG93Lm1hdGNoTWVkaWEoXCIobWF4LXdpZHRoOiA3NjdweClcIik7XG4gIHRhYnNtZWRpYXF1ZXJ5KG1lZGlhcXVlcnkpOyAvLyBDYWxsIGxpc3RlbmVyIGZ1bmN0aW9uIGF0IHJ1biB0aW1lXG4gIG1lZGlhcXVlcnkuYWRkTGlzdGVuZXIodGFic21lZGlhcXVlcnkpOyAvLyBBdHRhY2ggbGlzdGVuZXIgZnVuY3Rpb24gb24gc3RhdGUgY2hhbmdlc1xuXG4gIC8vRmluIEZ1bmNpb25hbGlkYWQgdGFicyB5IERyb3Bkb3duXG4gIC8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vXG4gICQoXCIuZmFxX19idG5cIikuY2xpY2soZnVuY3Rpb24oKSB7XG4gICAgaWYgKFxuICAgICAgJCh0aGlzKVxuICAgICAgICAucGFyZW50cyhcIi5mYXFfX2l0ZW1cIilcbiAgICAgICAgLmhhc0NsYXNzKFwianMtZHJvcGRvd25BY3RpdmVcIilcbiAgICApIHtcbiAgICAgICQodGhpcylcbiAgICAgICAgLnBhcmVudHMoXCIuZmFxX19pdGVtXCIpXG4gICAgICAgIC5yZW1vdmVDbGFzcyhcImpzLWRyb3Bkb3duQWN0aXZlXCIpO1xuICAgICAgJCh0aGlzKVxuICAgICAgICAuc2libGluZ3MoXCIuZmFxX19jb250ZW50XCIpXG4gICAgICAgIC5zbGlkZVRvZ2dsZSgpO1xuICAgIH0gZWxzZSB7XG4gICAgICAkKHRoaXMpXG4gICAgICAgIC5wYXJlbnRzKFwiLmZhcV9faXRlbVwiKVxuICAgICAgICAuYWRkQ2xhc3MoXCJqcy1kcm9wZG93bkFjdGl2ZVwiKTtcbiAgICAgICQodGhpcylcbiAgICAgICAgLnNpYmxpbmdzKFwiLmZhcV9fY29udGVudFwiKVxuICAgICAgICAuc2xpZGVUb2dnbGUoKTtcbiAgICAgICQoXCJodG1sLCBib2R5XCIpLmFuaW1hdGUoXG4gICAgICAgIHtcbiAgICAgICAgICBzY3JvbGxUb3A6ICQodGhpcykub2Zmc2V0KCkudG9wIC0gMTAwXG4gICAgICAgIH0sXG4gICAgICAgIDUwMFxuICAgICAgKTtcbiAgICB9XG4gIH0pO1xuXG4gIGZ1bmN0aW9uIGRyb3BEb3duQnV0dG9uKGUpe1xuICAgIGNvbnN0IGVsID0gJChlKS5wYXJlbnQoKVxuICAgIGNvbnN0IGNvbnRlbmlkbz0gZWxbMF0uZ2V0RWxlbWVudHNCeUNsYXNzTmFtZSgnZm9ybV9fY29udCcpXG4gICAgJChjb250ZW5pZG8pLmlzKFwiOnZpc2libGVcIik/ICQoY29udGVuaWRvKS5zbGlkZVVwKCdmYXN0Jyk6JChjb250ZW5pZG8pLnNsaWRlRG93bignZmFzdCcpXG4gIH1cblxuICBmdW5jdGlvbiBkcm9wRG93blJhZGlvKGUpe1xuICAgIGNvbnN0IGVsID0gJChlKS5wYXJlbnQoKS5wYXJlbnQoKVxuICAgIGNvbnN0IGNvbnRlbmlkbyA9IGVsWzBdLmdldEVsZW1lbnRzQnlDbGFzc05hbWUoJ2pzLWZvcm1Ecm9wZG93bicpXG4gICAgY29uc29sZS5sb2coZWwpXG4gICAgaWYoICEkKGNvbnRlbmlkbykuaXMoXCI6dmlzaWJsZVwiKSl7XG4gICAgICAkKCBcIi5qcy1mb3JtRHJvcGRvd25cIiApLnNsaWRlVXAoJ2Zhc3QnKVxuICAgICAgJChjb250ZW5pZG8pLnNsaWRlRG93bignZmFzdCcpXG4gICAgfVxuICB9XG4gICQoJy5mb3JtX19yYWRpbzpub3QoOmNoZWNrZWQpIH4gLmpzLWZvcm1Ecm9wZG93bicpLmhpZGUoKSAvLyBPY3VsdGEgbG9zIGVsIGNvbnRlbmlkbyBkZSBsb3MgcmFkaW8gYnV0dG9ucyBubyBzZWxlY2Npb25hZG9zXG5cbiAgZnVuY3Rpb24gZHJvcERvd25Jbml0KCl7XG4gICAgY29uc3QgZWxlbXMgPSAkKCBcIi5qcy1mb3JtRHJvcGRvd25cIiApXG4gICAgaWYoIWVsZW1zWzBdKSByZXR1cm4gdW5kZWZpbmVkXG4gICAgZWxlbXMuaGlkZSgpXG4gICAgJChlbGVtc1swXSkuc2hvdygpXG4gICAgcmV0dXJuIHVuZGVmaW5lZFxuICB9XG4gIC8vIEZ1bmNpb24gZGV0YWxsZSBzaW0gcmFkaW8gYnV0dG9ucyBpbmZvcm1hY2lvblxuICAvL0VuIGVsIEhUTUwgYcOxYWRpciBlbCBhdHJpYnV0byBkYXRhLXJhZGlvLW5hbWU9XCJzaW0xXCIgZG9uZGUgXCJzaW0xXCIgY29ycmVzcG9uZGUgYWwgaWQgZGVsIHJhZGlvIGFsIHF1ZSBjb3JyZXNwb25kZS5cbiAgZnVuY3Rpb24gaGlkZVByaWNlUmFkaW8gKCkgeyAvLyBFc2NvbmRlIHRvZG9zIGxvcyBkaXYgY29uIGluZm9ybWFjaW9uIGV4Y2VwdG8gZWwgY29ycmVzcG9uZGllbnRlIGFsIHJhZGlvIHNlbGVlY2Npb25hZG9cbiAgICB0cnl7XG4gICAgICAkKCcuY2FyZC1ib3gnKS5oaWRlKClcbiAgICAgICQoJy5jYXJkLWJveF9fbGluaycpLmhpZGUoKVxuICAgICAgY29uc3QgaWQgPSAkKCcuY2FyZC1yYWRpb19faW5wdXRbbmFtZT1cInByaWNlU2ltXCJdOmNoZWNrZWQnKVswXS5pZFxuICAgICAgJCgnW2RhdGEtcmFkaW8tbmFtZT1cIicgKyBpZCArICdcIl0nKS5zaG93KClcbiAgICB9XG4gICAgY2F0Y2ggKGVycikge31cbiAgfVxuXG4gIGZ1bmN0aW9uIHJhZGlvU2ltICgpIHsgLy8gQ2FtYmlhIGVsIGRpdiBhbCBzZWxlY2Npb25hciBvdHJvIHJhZGlvXG4gICAgJCgnLmNhcmQtYm94JykuaGlkZSgpXG4gICAgJCgnLmNhcmQtYm94X19saW5rJykuaGlkZSgpXG4gICAgJCgnW2RhdGEtcmFkaW8tbmFtZT1cIicgKyB0aGlzLmlkICsgJ1wiXScpLnNob3coKVxuICB9XG4gICQoJy5jYXJkLXJhZGlvX19pbnB1dFtuYW1lPVwicHJpY2VTaW1cIl0nKS5jbGljayhyYWRpb1NpbSkgLy8gVmluY3VsYSBsYSBmdW5jaW9uIGRlIGNhbWJpbyBkZSByYWRpbyBhIGxvcyBjb3Jlc3BvbmRpZW50ZXMgcmFkaW9zXG4gIC8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vXG4gIC8vIEZ1bmNpb25hbGlkYWQgdGFicyB0ZXJtaW5vcyB5IGNvbmRpY2lvbmVzXG5cbiAgLy8gRHJvcGRvd24gdGFibGFcbiAgJCgnLmpzLWRhdGFEZXRhaWxCdG4nKS5vbignY2xpY2snLCBmdW5jdGlvbiAoKSB7XG4gICAgY29uc3QgYnRuSW52b2ljZSA9ICQodGhpcykuZGF0YSgpLmludm9pY2UgLy8gT2J0aWVuZSBlbCBhdHJpYnV0byBkYXRhLWludm9pY2UgcGFyYSBzYWJlciBlbCBjw7NkaWdvIGRlIGxhIGZhY3R1cmEgeSBhc2kgaWRlbnRpZmljYXIgbGEgdGFibGFcbiAgICBjb25zdCB0YWJsZUNvbnQgPSAkKCcuanMtZGF0YURldGFpbFtkYXRhLWludm9pY2U9XCInICsgYnRuSW52b2ljZSArICdcIl0nKSAvLyBPYnRpZW5lIGxhIHRhYmxhIHZpbmN1bGFkYSBzZWfDum4gIGVsIGPDs2RpZ28gZGUgbGEgZmFjdHVyYSBlbiBlbCBib3TDs25cbiAgICByZXR1cm4gdGFibGVDb250LmlzKCc6dmlzaWJsZScpID8gdGFibGVDb250LnNsaWRlVXAoJ2Zhc3QnKSA6IHRhYmxlQ29udC5zbGlkZURvd24oJ2Zhc3QnKSAvLyBFc2NvbmRlIG8gbXVlc3RyYSBsYSB0YWJsYSBkZSBsYSBmYWN0dXJhXG4gIH0pXG5cbiAgLy9Ecm9wZG93biB0YWJsYSBkZSBub3RpZmljYWNpb25lc1xuICBmdW5jdGlvbiBkcm9wZG93bk5vdGlmeSgpIHtcbiAgICAkKCcuanMtYnRuQ2xvc2VCYW5uZXInKS5vbignY2xpY2snLCBmdW5jdGlvbiAoZSkge1xuICAgICAgJCh0aGlzKS5wYXJlbnQoJChcIi5lcy10YWJsZS1iYW5uZXJfX3dyYXBcIikpLnNsaWRlVXAoKTtcbiAgICB9KVxuICB9XG5cbiAgJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24oKSB7XG4gICAgLy8gRnVuY2lvbiBwYXJhIG9idGVuZXIgZWwgYWx0byBkZSBsYSB0YWIgYWN0aXZhXG4gICAgdGFiSGVpZ2h0KClcbiAgICBoaWRlUHJpY2VSYWRpbygpXG4gICAgZHJvcERvd25Jbml0KClcbiAgICBkcm9wZG93bk5vdGlmeSgpXG4gIH0pXG5cbiAgJCh3aW5kb3cpLnJlc2l6ZShmdW5jdGlvbigpIHtcbiAgICB0YWJIZWlnaHQoKVxuICB9KVxuICAvL0ZpbiBGdW5jaW9uYWxpZGFkIHRhYnMgdGVybWlub3MgeSBjb25kaWNpb25lc1xuICAvLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL1xuIiwiZnVuY3Rpb24gdG9vdGx0aXBEYXRhKCkge1xuICAkKFwiLmpzLWJ0blRvb2x0aXBEYXRhXCIpLm9uKCdjbGljaycsIGZ1bmN0aW9uIChlKSB7XG4gICAgaWYgKCQodGhpcykuc2libGluZ3MoXCIuanMtdG9vbHRpcERhdGFcIikuaGFzQ2xhc3MoXCJqcy10b29sdGlwRGF0YS1jbG9zZVwiKSkge1xuICAgICAgJChcIi5qcy10b29sdGlwRGF0YS5qcy10b29sdGlwRGF0YS1vcGVuXCIpLnNsaWRlVXAoKS5yZW1vdmVDbGFzcyhcImpzLXRvb2x0aXBEYXRhLW9wZW5cIikuYWRkQ2xhc3MoXCJqcy10b29sdGlwRGF0YS1jbG9zZVwiKTtcbiAgICAgICQodGhpcykuc2libGluZ3MoXCIuanMtdG9vbHRpcERhdGFcIikuc2xpZGVEb3duKCkuYWRkQ2xhc3MoXCJqcy10b29sdGlwRGF0YS1vcGVuXCIpLnJlbW92ZUNsYXNzKFwianMtdG9vbHRpcERhdGEtY2xvc2VcIik7XG4gICAgfSBlbHNlIHtcbiAgICAgICQodGhpcykuc2libGluZ3MoXCIuanMtdG9vbHRpcERhdGFcIikuc2xpZGVVcCgpLnJlbW92ZUNsYXNzKFwianMtdG9vbHRpcERhdGEtb3BlblwiKS5hZGRDbGFzcyhcImpzLXRvb2x0aXBEYXRhLWNsb3NlXCIpO1xuICAgIH1cbiAgfSk7XG59XG5cbiQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uICgpIHtcbiAgdG9vdGx0aXBEYXRhKCk7XG59KTtcblxuJCh3aW5kb3cpLnJlc2l6ZShmdW5jdGlvbiAoKSB7XG4gICQoXCIuanMtYnRuVG9vbHRpcERhdGFcIikudW5iaW5kKFwiY2xpY2tcIik7XG4gIHRvb3RsdGlwRGF0YSgpO1xufSk7Il19
