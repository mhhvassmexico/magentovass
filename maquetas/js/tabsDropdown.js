///////////////////////////////////////////////////////////
  //Inicio Funcionalidad tabs y Dropdown
  // Funcion para obtener el alto de la tab activa
  function tabHeight() {
    if ($(window).width() > 768) {
      var heightTabActive = $(".js-tabItemActive")
        .children(".tabs__pane")
        .height();
      $(".tabs").css("height", heightTabActive + 140);
      //console.log(heightTabActive);
    } else {
      $(".tabs").css("height", "initial");
    }
  }

  $(".tabs__item button").click(function() {
    const parent = this.parentElement;
    if (window.innerWidth <= 767 && $(parent).hasClass("js-tabItemActive")) {
      //Al dar clic cierra el tab si esta abierto cuando la pantalla es menor a 767
      $(parent).removeClass("js-tabItemActive");
    } else {
      $(".js-tabItemActive").removeClass("js-tabItemActive");
      $(parent).addClass("js-tabItemActive");
      tabHeight();
    }
  });

  function tabsmediaquery(x) {
    //Verifica al redimensionar la ventana y abre o cierra los tabs correspondientes
    if (!x.matches) {
      // If media query matches
      if (
        !$(".tabs__item").hasClass("js-tabItemActive") &&
        $(".tabs__item").length > 0
      ) {
        $(".tabs__item")[0].classList.add("js-tabItemActive");
      }
    }
  }

  const mediaquery = window.matchMedia("(max-width: 767px)");
  tabsmediaquery(mediaquery); // Call listener function at run time
  mediaquery.addListener(tabsmediaquery); // Attach listener function on state changes

  //Fin Funcionalidad tabs y Dropdown
  /////////////////////////////////////////////////////////////////
  $(".faq__btn").click(function() {
    if (
      $(this)
        .parents(".faq__item")
        .hasClass("js-dropdownActive")
    ) {
      $(this)
        .parents(".faq__item")
        .removeClass("js-dropdownActive");
      $(this)
        .siblings(".faq__content")
        .slideToggle();
    } else {
      $(this)
        .parents(".faq__item")
        .addClass("js-dropdownActive");
      $(this)
        .siblings(".faq__content")
        .slideToggle();
      $("html, body").animate(
        {
          scrollTop: $(this).offset().top - 100
        },
        500
      );
    }
  });

  function dropDownButton(e){
    const el = $(e).parent()
    const contenido= el[0].getElementsByClassName('form__cont')
    $(contenido).is(":visible")? $(contenido).slideUp('fast'):$(contenido).slideDown('fast')
  }

  function dropDownRadio(e){
    const el = $(e).parent().parent()
    const contenido = el[0].getElementsByClassName('js-formDropdown')
    console.log(el)
    if( !$(contenido).is(":visible")){
      $( ".js-formDropdown" ).slideUp('fast')
      $(contenido).slideDown('fast')
    }
  }
  $('.form__radio:not(:checked) ~ .js-formDropdown').hide() // Oculta los el contenido de los radio buttons no seleccionados

  function dropDownInit(){
    const elems = $( ".js-formDropdown" )
    if(!elems[0]) return undefined
    elems.hide()
    $(elems[0]).show()
    return undefined
  }
  // Funcion detalle sim radio buttons informacion
  //En el HTML añadir el atributo data-radio-name="sim1" donde "sim1" corresponde al id del radio al que corresponde.
  function hidePriceRadio () { // Esconde todos los div con informacion excepto el correspondiente al radio seleeccionado
    try{
      $('.card-box').hide()
      $('.card-box__link').hide()
      const id = $('.card-radio__input[name="priceSim"]:checked')[0].id
      $('[data-radio-name="' + id + '"]').show()
    }
    catch (err) {}
  }

  function radioSim () { // Cambia el div al seleccionar otro radio
    $('.card-box').hide()
    $('.card-box__link').hide()
    $('[data-radio-name="' + this.id + '"]').show()
  }
  $('.card-radio__input[name="priceSim"]').click(radioSim) // Vincula la funcion de cambio de radio a los corespondientes radios
  /////////////////////////////////////////////////////////////////
  // Funcionalidad tabs terminos y condiciones

  // Dropdown tabla
  $('.js-dataDetailBtn').on('click', function () {
    const btnInvoice = $(this).data().invoice // Obtiene el atributo data-invoice para saber el código de la factura y asi identificar la tabla
    const tableCont = $('.js-dataDetail[data-invoice="' + btnInvoice + '"]') // Obtiene la tabla vinculada según  el código de la factura en el botón
    return tableCont.is(':visible') ? tableCont.slideUp('fast') : tableCont.slideDown('fast') // Esconde o muestra la tabla de la factura
  })

  //Dropdown tabla de notificaciones
  function dropdownNotify() {
    $('.js-btnCloseBanner').on('click', function (e) {
      $(this).parent($(".es-table-banner__wrap")).slideUp();
    })
  }

  $(document).ready(function() {
    // Funcion para obtener el alto de la tab activa
    tabHeight()
    hidePriceRadio()
    dropDownInit()
    dropdownNotify()
  })

  $(window).resize(function() {
    tabHeight()
  })
  //Fin Funcionalidad tabs terminos y condiciones
  /////////////////////////////////////////////////////////////////
