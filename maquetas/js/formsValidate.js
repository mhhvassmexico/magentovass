// ////////////////////////////////////////////////////////////
// Inicio Función ocultar o mostrar contraseña
function showHidePassword () {
  $('button.form__ico.form__js-ico.i-eye').on('click', function () {
    const el = $(this).parent().find('input[name="contraseña"]')[0]
    el.type = el.type === 'password' ? 'text' : 'password'
  })
}
// Fin Función ocultar o mostrar contraseña
// ////////////////////////////////////////////////////////////

// ////////////////////////////////////////////////////////////
// Inicio Validación Formularios
const messages = {
  emailTel: 'introduce un email o teléfono valido.',
  password: 'introduce una contraseña valida.',
  name: 'Ingresa un Nombre valido.',
  lastName: 'Ingresa un Apellido valido.',
  phone: 'Ingresa un numero de teléfono valido',
  email: 'Ingrese un correo electrónico valido',
  checkPrivacy: 'Para continuar tienes que aceptar nuestro aviso de privacidad',
  street: 'Calle incorrecta. Ingresa una calle valida.',
  streetNumber: 'Numero incorrecto. Ingresa un Numero valido.',
  streetColony: 'Colonia incorrecta. Ingresa una Colonia valida.',
  city: 'Ciudad incorrecta. Ingresa una Ciudad valida.',
  municipality: 'Municipio incorrecto. Ingresa un Municipio valido.',
  RFC: 'Ingresa un RFC valido.',
  confirmEmail: 'Ingresa el mismo correo electrónico.',
  postalCode: 'Código postal incorrect. Ingresa un Código postal valido.',
  value: 'Introduce un valor valido.',
  radio: 'Seleccionar una opción'
}

function validateForms () {
  $('.validate').validate({
    errorPlacement: function () {} // Soluciona problemas con labels y estilos en checkbox y radios
  })

  $('[data-validate]').each(function () { // Buscamos todos los input con atributo data-validate y les añadimos las reglas de validación.
    const elem = this
    const name = elem.getAttribute('name')
    const dataValidate = elem.getAttribute('data-validate')

    $(this).rules('add', {
      required: true,
      messages: {
        required: function (el) {          
          $('small.form__alert[name="' + name + '"]').text(messages[dataValidate]) // Se busca el elemento small.form__alert distinguiéndolo por el name y se agrega el mensaje de error
        }
      },
      success: function () {
        $('small.form__alert[name="' + name + '"]').text('')
      }
    })

    if (dataValidate === 'confirmEmail') {
      $(this).rules('add', {
        equalTo: '[data-validate-equalEmail]'
      })
    }
  })

  jQuery.extend(jQuery.validator.messages, {
    required () { changeCustomMessageValidate(this) },
    equalTo () { changeCustomMessageValidate(this) },
    email () { changeCustomMessageValidate(this) }
  })
}

function changeCustomMessageValidate (el) {
  const elem = el.lastActive
  const name = elem.getAttribute('name')
  const dataValidate = elem.getAttribute('data-validate')
  $('small.form__alert[name="' + name + '"]').text(messages[dataValidate])
}
// Fin Función Validación Formularios
// ////////////////////////////////////////////////////////////

$(document).ready(function () {
  $('button.form__ico.form__js-ico.i-eye')[0] && showHidePassword() // Verifica si hay botón de esconder contraseña y ejecuta la función de esconder y mostrar contraseña
  $('.validate')[0] && validateForms() // Verifica si hay formulario con clase .validate y si asi es ejecuta la función de validación
})
