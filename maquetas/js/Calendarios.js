const meses = ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
function obtenerFecha (fecha) {
  return `${fecha.getDate()} ${meses[fecha.getMonth()]}`
}
const optionCalendar = {
  locale: 'es',
  mode: 'range',
  closeOnSelect: false,
  plugins: [new confirmDatePlugin({
    confirmText: "Aplicar ",
    showAlways: true,
  })],
  showMonths: 2,
  onClose: function (selectedDates, dateStr, instance) {
    if(selectedDates[1]){
      const fecha = `${obtenerFecha(selectedDates[0])} - ${obtenerFecha(selectedDates[1])}`
      $('.js-date-picker').text(fecha)
    } else {
      this.clear()
    }
  }
}
$(document).ready(function () {
  flatpickr('.js-date-picker', optionCalendar)
})
