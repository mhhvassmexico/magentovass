// Inicio Configuracion Sliders
// Inicio Configuracion menu como carrusel
var options = {
  horizontal: 1,
  itemNav: "basic",
  speed: 300,
  mouseDragging: 1,
  touchDragging: 1
};

$(".menu").sly(options);
$(window).resize(function (e) {
  $(".menu").sly("reload");
});


$(".main-slider__item").slick({ //Slider detalle producto
  arrows: false,
  infinite: false,
  dots: true,
  autoplaySpeed: 6000,
  autoplay: true,
  pauseOnFocus: false,
  dotsClass: 'slick-dots slick-dots_static'
});

//Slider lista Accesorios [cart-showcase.pug]
$(".slider-showcase__list").slick({ //Slider lista de productos
  slidesToShow: 3,
  slidesToScroll: 3,
  pauseOnFocus: false,
  prevArrow: $(".prev"),
  nextArrow: $(".next"),
  responsive: [{
      breakpoint: 420,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      }
    },
    {
      breakpoint: 785,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
      }
    },
  ]
});

$(".hero-banner").slick({ //Slider banner principal
  slidesToShow: 1,
  slidesToScroll: 1,
  mobileFirst: true,
  arrows: false,
  infinite: false,
  dots: true,
  autoplay: true,
  autoplaySpeed: 6000,
  dotsClass: 'slick-dots slick-dots_animated',
});

//Slider Checkbox [board-tabs.pug]
$(".board-tabs__slider").slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  centerMode: true,
  mobileFirst: true,
  centerPadding: '20px',
  pauseOnFocus: false,
  prevArrow: $(".left"),
  nextArrow: $(".right"),
  responsive: [{
      breakpoint: 480,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
        centerMode: true
      }
    },
    {
      breakpoint: 660,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
        centerMode: true
      }
    },
    {
      breakpoint: 890,
      settings: 'unslick'
    }
  ]
});

//Slider Radiobuttons [list-radio.pug]
function listSlider() {
  if ($(window).width() < 829) {
    $(".list-radio__list").slick({
      arrows: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      pauseOnFocus: false,
      prevArrow: $(".left"),
      nextArrow: $(".right")
    });
  } else {
    $('.list-radio__list').slick('unslick');
  }
}

listSlider();

var r;

$(window).resize(function () {
  //$('.list-radio__list')[0].slick.refresh();
  $('.list-radio__list').slick('resize');
  $('.board-tabs__slider').slick('resize');
  clearTimeout(r);
  //console.log(r);
  r = setTimeout(listSlider, 5000);
});

//Slider Detalle de Terminal [slider-detail.pug]
$(".js-slideDetail").slick({
  //Slider lista de productos
  slidesToShow: 2,
  slidesToScroll: 2,
  pauseOnFocus: false,
  prevArrow: $(".prev"),
  nextArrow: $(".next"),
  responsive: [{
    breakpoint: 640,
    settings: {
      slidesToShow: 1,
      slidesToScroll: 1,
    },
  }]
});

//Slider Hero Home [home.html]
$(".js-sliderHome1").slick({ //Slider banner principal
  slidesToShow: 1,
  slidesToScroll: 1,
  mobileFirst: true,
  arrows: false,
  infinite: false,
  dots: true,
  autoplay: true,
  autoplaySpeed: 6000,
  dotsClass: 'slick-dots slick-dots_animated',
});

//Slider Brands [home.html]
$(".js-sliderHome2").slick({
  centerMode: true,
  centerPadding: '200px',
  slidesToShow: 3,
  speed: 1500,
  index: 2,
  arrows: true,
  prevArrow: $(".i-prev"),
  nextArrow: $(".i-next"),
  responsive: [{
    breakpoint: 768,
    settings: {
      arrows: false,
      centerMode: true,
      centerPadding: '100px',
      slidesToShow: 1
      //slidesToShow: 3
    }
  }, {
    breakpoint: 480,
    settings: {
      arrows: false,
      centerMode: true,
      centerPadding: '70px',
      slidesToShow: 1
    }
  }]
});


//Slider de Terminales [home.html]
$(".js-slideHome3").slick({ //Slider lista de productos
  slidesToShow: 3,
  //centerMode: true,
  prevArrow: $(".prev"),
  nextArrow: $(".next"),
  responsive: [{
      breakpoint: 540,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      }
    },
    {
      breakpoint: 785,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
      }
    },
  ]
});

//- [.slider__cards Cards de Vitrina Prepago]
$('.js-slideCards').slick({
  centerMode: true,
  //centerPadding: '20px',
  slidesToShow: 1,
  slidesToScroll: 1,
  mobileFirst: true,
  infinite: true,
  arrows: true,
  responsive: [
    {
      breakpoint: 320,
      settings: {
        centerMode: true,
        centerPadding: '5px',
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true
      }
    },
    {
      breakpoint: 540,
      settings: {
        centerMode: true,
        centerPadding: '8px',
        slidesToShow: 2,
        slidesToScroll: 2,
        arrows: true
      }
    },
    {
      breakpoint: 640,
      settings: {
        centerMode: true,
        centerPadding: '8px',
        slidesToShow: 2,
        slidesToScroll: 2,
        arrows: true
      }
    },
    {
      breakpoint: 768,
      settings: {
        centerMode: false,
        slidesToShow: 3,
        slidesToScroll: 3,
        arrows: true
      }
    },
    {
      breakpoint: 1024,
      settings: {
        centerMode: false,
        slidesToShow: 4,
        slidesToScroll: 4,
        arrows: true,
        adaptiveHeight: true,
      }
    }
  ]
});
