        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>comunicación</h3>
              </div>

            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel" style="height:600px;">
                  <div class="x_title">
                    <h2>Medios de comunicación para el equipo</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>                      
                    </ul>
                    <div class="clearfix"></div>
                  </div>

                  <div class="x_content">
                    <div class="row">

                      <div class="col-md-12">

                        <!-- price element -->
                        <div class="col-md-3 col-sm-6 col-xs-12">
                          <div class="pricing">
                            <div class="title">
                              <h2>JIRA</h2>
                              <h1>JIRA ATLASSIAN</h1>
                            </div>
                            <div class="x_content">
                              <div class="">
                                <div class="pricing_features">
                                  <ul class="list-unstyled text-left">                                    
                                    <li><i class="fa fa-check text-success"></i> TelefonicaMovistar <strong> <a href="https://vassmagentomx.atlassian.net" target="_blank">Acceso</a></strong></li>                                   
                                  </ul>
                                </div>
                              </div>
                              <div class="pricing_footer">
                              	<img src="images/jira.png" width="260" style="margin: 8px;">
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- price element -->

                        <!-- price element -->
                        <div class="col-md-3 col-sm-6 col-xs-12">
                          <div class="pricing">
                            <div class="title">
                              <h2>CANAL SLACK</h2>
                              <h1># magentovass</h1>
                            </div>
                            <div class="x_content">
                              <div class="">
                                <div class="pricing_features">
                                  <ul class="list-unstyled text-left">                                    
                                    <li><i class="fa fa-check text-success"></i> Acceso <strong> <a href="https://vassmexico.slack.com/" target="_blank">via WEB</a></strong></li>
                                    <li><i class="fa fa-check text-success"></i> Descargar <strong> <a href="https://slack.com/intl/es/downloads/windows" target="_blank">para Escritorio</a></strong></li>
                                  </ul>
                                </div>
                              </div>
                              <div class="pricing_footer">                                
                                <img src="images/slack.png" width="178">
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- price element -->

                        <!-- price element -->
                        <div class="col-md-3 col-sm-6 col-xs-12">
                          <div class="pricing">
                            <div class="title">
                              <h2>SKYPE</h2>
                              <h1>SKYPE</h1>
                            </div>
                            <div class="x_content">
                              <div class="">
                                <div class="pricing_features">
                                  <ul class="list-unstyled text-left">                                    
                                    <li><i class="fa fa-check text-success"></i> SKYPE <strong> <a href="https://web.skype.com/" target="_blank">via WEB</a></strong></li>
                                    <li><i class="fa fa-check text-success"></i> Descargar <strong> <a href="https://go.skype.com/download" target="_blank">para Escritorio</a></strong></li>                               
                                  </ul>
                                </div>
                              </div>
                              <div class="pricing_footer">
                              	<img src="images/skype.webp" width="50">
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- price element -->

                        <!-- price element -->
                        <div class="col-md-3 col-sm-6 col-xs-12">
                          <div class="pricing">
                            <div class="title">
                              <h2>APPEAR</h2>
                              <h1>appear.in</h1>
                            </div>
                            <div class="x_content">
                              <div class="">
                                <div class="pricing_features">
                                  <ul class="list-unstyled text-left">                                    
                                    <li><i class="fa fa-check text-success"></i> APPEAR <strong> <a href="https://appear.in/" target="_blank">via WEB</a></strong></li>
                                    <li class="text-danger"><i class="fa fa-ban text-danger"></i> ROOM ACTIVO <strong> <a href="javascript:void(0);" target="_blank" class="text-danger">Entrara</a></strong></li>                               
                                  </ul>
                                </div>
                              </div>
                              <div class="pricing_footer">
                              	<img src="images/appear.svg" width="224">
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- price element -->
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->