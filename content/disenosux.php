<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Diseño UX <small> Invisionapp</small></h3>
      </div>

    </div>

    <div class="clearfix"></div>

    <div class="row" style="display: none;">
      <div class="clearfix"></div>
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Diseños publicos <small></small></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>              
            </ul>
            <div class="clearfix"></div>
          </div>

          <div class="x_content">

            <p>Los diseño presentados a continuación pueden sufrí cambios atreves del tiempo de desarrollo por los cambios aplicados por parte del cliente</p>

            <div class="table-responsive">
              <table class="table table-striped jambo_table bulk_action">
                <thead>
                  <tr class="headings">                    
                    <th class="column-title">Fecha Muestra </th>
                    <th class="column-title">Nombre pantalla </th>
                    <th class="column-title">Link </th>
                    <th class="column-title">Status </th>
                    <th class="column-title">Comentario </th>
                  </tr>
                </thead>

                <tbody>
                  <tr class="even pointer">                    
                    <td class=" ">121000040</td>
                    <td class=" ">May 23, 2014 11:47:56 PM </td>
                    <td class=" ">121000210 <i class="success fa fa-long-arrow-up"></i></td>
                    <td class=" ">John Blank L</td>
                    <td class=" ">Paid</td>                    
                    </td>
                  </tr>
                  <tr class="odd pointer">                    
                    <td class=" ">121000039</td>
                    <td class=" ">May 23, 2014 11:30:12 PM</td>
                    <td class=" ">121000208 <i class="success fa fa-long-arrow-up"></i>
                    </td>
                    <td class=" ">John Blank L</td>
                    <td class=" ">Paid</td>                    
                  </tr>
                </tbody>
              </table>
            </div>
			
		
          </div>
        </div>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
</div>