<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Codigo <small> fuente</small></h3>
      </div>

    </div>

    <div class="clearfix"></div>

    <div class="row">
      <div class="clearfix"></div>
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>version Magento 2.2.3 Community <small></small></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>              
            </ul>
            <div class="clearfix"></div>
          </div>

          <div class="x_content">

            <p>Version que se ha utilizado para desarrollar este proyecto de Telefonica Movistar</p>

            <div class="table-responsive">
              <table class="table table-striped jambo_table bulk_action">
                <thead>
                  <tr class="headings">                    
                    <th class="column-title">Fecha Publicación </th>
                    <th class="column-title">Nombre Paquete </th>
                    <th class="column-title">Link </th>
                    <th class="column-title">Comentario </th>
                    <th class="column-title">Estatus </th>                    
                  </tr>
                </thead>

                <tbody>
                  <tr class="even pointer">                    
                    <td class="">03-07-2018</td>
                    <td class="">Magento-EE-2.2.3</td>
                    <td class=""><a href="recursos/sourcemagento/Magento-EE-2.2.3.zip">DESCARGAR</a></td>                    
                    <td class=" ">El proyecto se ha creado con esta version, descargarse para montar una version local desde cero</td>    
                    <td class=" ">Activo</td>                
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
			
		
          </div>
        </div>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
</div>