        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Repositorio Bitbucket</h3>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="">
              


<div class="col-md-12 col-sm-12 col-xs-12">
  <div class="x_panel">
    <div class="x_title">
      <h2><i class="fa fa-bars"></i> Detalle de instancias <small>( Productivo - Certificación - Desarrollo)</small></h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
        </li>
      </ul>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">

      <div class="col-xs-3">
        <!-- required for floating -->
        <!-- Nav tabs -->
        <ul class="nav nav-tabs tabs-left">
          <li class="active"><a href="#desarrollo" data-toggle="tab">Desarrollo</a>
          </li>
          <li><a href="#certificacion" data-toggle="tab">Certificación</a>
          </li>
          <li><a href="#produccion" data-toggle="tab">Producción</a>
          </li>          
        </ul>
      </div>

      <div class="col-xs-9">
        <!-- Tab panes -->
        <div class="tab-content">
          <div class="tab-pane active" id="desarrollo">
            <p class="lead">Instancia de Desarrollo</p>
            <p>Esta instancia permitirá a los desarrolladores trabajar sin problema alguno ya que es un ambiente preparado para integrar el desarrollo de la tienda de Telefónica Movistar.</p>
            <div class="col-md-12">

                <div class="col-md-4 col-lg-4 col-sm-8 col-xs-12 widget widget_tally_box">
                  <div class="x_panel fixed_height_390">
                    <div class="x_content">

                      <div class="flex">
                        <ul class="list-inline widget_profile_box">
                          <li>
                            <a>
                              <i class="fa fa-laptop"></i>
                            </a>
                          </li>
                          <li>
                            <img src="images/ec2.png" alt="..." class="img-circle profile_img">
                          </li>
                          <li>
                            <a>
                              <i class="fa fa-bitbucket"></i>
                            </a>
                          </li>
                        </ul>
                      </div>

                      <h3 class="name">Desarrollo</h3>

                      <div class="flex">
                        <ul class="list-inline count2">
                          <li>
                            <h3><span class="fa fa-arrow-circle-up"></span></h3>
                            <span>master</span>
                          </li>
                          <li class="text-primary">
                            <h3><span class="fa fa-arrow-circle-up"></span></h3>
                            <span>develop</span>
                          </li>
                          <li>
                            <h3><span class="fa fa-arrow-circle-up"></span></h3>
                            <span>cert</span>
                          </li>
                        </ul>
                      </div>
                      <p>
                        El repositorio esta conformado por 3 RAMAS de las cuales el desarrollador debe clonar el proyecto y a continuación crear una nueva rama partir de la RAMA <strong>develop</strong> <a href="https://bitbucket.org/mhhvassmexico/vass_telefonica/src/master/" target="_blank">ACCESO BITBUCKET</a>
                      </p>
                    </div>
                  </div>
                </div>              

                <div class="col-md-8 col-lg-8 col-sm-5">
                  <ol class="lista-comandos">
                    <li>clonar Proyecto a su computadora local
                      <p class="comando">
                        git clone https://mhhvassmexico@bitbucket.org/mhhvassmexico/vass_telefonica.git
                      </p>
                    </li>
                    <li>Crear RAMA apartir del numero de Ticket creado en JIRA, la nomenclatura es TEL guion medio mas numero de ticket como el siguiente ejemlo
                      <span>TEL-12</span> indica la RAMA para el proyecto movistar del ticket # 12
                    </li>                   
                    <li>Una vez se cree la RAMA apartir de <strong>develop</strong> se trabajar en el cambio hasta tenerlo listo, posterior se deberan agregar los cambios
                      <span class="comando">git add [Nombre de los archivos a agregar | . para agregar todos los archivos modificados]</span>
                    </li>
                    <li>
                      Posterior se Comitearan los cambios
                      <span class="comando">git commit -m "Nota del cambio a subir al repositorio"</span>
                    </li>
                    <li>
                      Se enviaran los cambios al repositorio REMOTO
                      <span class="comando">git push origin <span class="rama">NOMBRE_RAMA</span></span>
                    </li>
                    <li>
                      Dentro de Bitbucket se generara un <strong>PULLREQUEST</strong> del ticket commiteado contra la rama <span class="rama">cert</span> de la instancia de certificación

                    </li>
                  </ol>
                </div>                

            </div>
          </div>
          <div class="tab-pane" id="certificacion">
            <p class="lead">Instancia de Certificación</p>
            <p>Esta instancia representa una replica exacta de lo que será el servidor en Producción sirve para mostrara al cliente como va el proceso del proyecto</p>
            <div class="col-md-12">

                <div class="col-md-4 col-lg-4 col-sm-8 col-xs-12 widget widget_tally_box">
                  <div class="x_panel fixed_height_390">
                    <div class="x_content">

                      <div class="flex">
                        <ul class="list-inline widget_profile_box">
                          <li>
                            <a>
                              <i class="fa fa-laptop"></i>
                            </a>
                          </li>
                          <li>
                            <img src="images/ec2.png" alt="..." class="img-circle profile_img">
                          </li>
                          <li>
                            <a>
                              <i class="fa fa-bitbucket"></i>
                            </a>
                          </li>
                        </ul>
                      </div>

                      <h3 class="name">Certificacion</h3>

                      <div class="flex">
                        <ul class="list-inline count2">
                          <li>
                            <h3><span class="fa fa-arrow-circle-up"></span></h3>
                            <span>master</span>
                          </li>
                          <li>
                            <h3><span class="fa fa-arrow-circle-up"></span></h3>
                            <span>develop</span>
                          </li>
                          <li class="text-primary">
                            <h3><span class="fa fa-arrow-circle-up"></span></h3>
                            <span>cert</span>
                          </li>
                        </ul>
                      </div>
                      <p>
                        El repositorio esta conformado por 3 RAMAS de las cuales el desarrollador debe clonar el proyecto y a continuación crear una nueva rama partir de la RAMA <strong>develop</strong> <a href="https://bitbucket.org/mhhvassmexico/vass_telefonica/src/master/" target="_blank">ACCESO BITBUCKET</a>
                      </p>
                    </div>
                  </div>
                </div>              

                <div class="col-md-8 col-lg-8 col-sm-5">
                  <h3>Solo el administrador podrá publicar los cambios en esta instancia</h3>
                </div>                

            </div>
          </div>
          <div class="tab-pane" id="produccion">
            <p class="lead">Instancia de Producción</p>
            <p>Esta instancia será el servidor en Producción sirve para publicar la tienda final del proyecto al publico en general</p>
                        <div class="col-md-12">

                <div class="col-md-4 col-lg-4 col-sm-8 col-xs-12 widget widget_tally_box">
                  <div class="x_panel fixed_height_390">
                    <div class="x_content">

                      <div class="flex">
                        <ul class="list-inline widget_profile_box">
                          <li>
                            <a>
                              <i class="fa fa-laptop"></i>
                            </a>
                          </li>
                          <li>
                            <img src="images/ec2.png" alt="..." class="img-circle profile_img">
                          </li>
                          <li>
                            <a>
                              <i class="fa fa-bitbucket"></i>
                            </a>
                          </li>
                        </ul>
                      </div>

                      <h3 class="name">Producción</h3>

                      <div class="flex">
                        <ul class="list-inline count2">
                          <li class="text-primary">
                            <h3><span class="fa fa-arrow-circle-up"></span></h3>
                            <span>master</span>
                          </li>
                          <li>
                            <h3><span class="fa fa-arrow-circle-up"></span></h3>
                            <span>develop</span>
                          </li>
                          <li>
                            <h3><span class="fa fa-arrow-circle-up"></span></h3>
                            <span>cert</span>
                          </li>
                        </ul>
                      </div>
                      <p>
                        El repositorio esta conformado por 3 RAMAS de las cuales el desarrollador debe clonar el proyecto y a continuación crear una nueva rama partir de la RAMA <strong>develop</strong> <a href="https://bitbucket.org/mhhvassmexico/vass_telefonica/src/master/" target="_blank">ACCESO BITBUCKET</a>
                      </p>
                    </div>
                  </div>
                </div>              

                <div class="col-md-8 col-lg-8 col-sm-5">
                  <h3>Solo el administrador podrá publicar los cambios en esta instancia</h3>
                </div>                

            </div>
          </div>          
        </div>
      </div>

      <div class="clearfix"></div>

    </div>
  </div>
</div>              






            </div>

            <div class="clearfix"></div>

          </div>
          <div class="clearfix"></div>
        </div>