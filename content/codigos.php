<!-- page content -->
<div class="right_col" role="main">
<div class="">
  <div class="page-title">
    <div class="title_left">
      <h3>Codigos Fuentes</h3>
    </div>    
  </div>

  <div class="clearfix"></div>


  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Maquetas <small>UX</small></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

        <div class="bs-example" data-example-id="simple-jumbotron">
          <div class="jumbotron">            
            <p>Aqui se encuentra todo el material actualizado de los codigos de las maquetas UX.</p>
          </div>
        </div>

      </div>
    </div>
  </div>              <!-- Start to do list -->
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Archivos fuetes <small>(html, css, js etc...)</small></h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <div class="accordion">

                      <div class="panel">
                        <a class="panel-heading collapsed" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                          <h4 class="panel-title">Paquete de archivos</h4>
                        </a>
                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                          <div class="panel-body">                            
                            <p>
                              <ul>
                                <li><strong>Paquete</strong> <a href="download.php?file=maquetas/eshop_31072018.rar" target="_blank" class="text-success">eshop_31072018.rar</a> (31-07-2018) </li>
                                <li><strong>Paquete</strong> <a href="download.php?file=maquetas/eshop.rar" target="_blank" class="text-success">eshop.rar</a> (03-07-2018) </li>
                              </ul>
                            </p>
                          </div>
                        </div>
                      </div>

                      <div class="panel">
                        <a class="panel-heading collapsed" role="tab" id="headingThree" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                          <h4 class="panel-title">Ver por Web</h4>
                        </a>
                        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                          <div class="panel-body">                            
                            <p>
                              <ul>
                                <li><strong>Mostrar proyecto </strong> <a href="maquetas/" target="_blank">TEST ONLINE</a></li>                               
                              </ul>
                            </p>
                          </div>
                        </div>
                      </div>                      


                  </div>
                </div>
              </div>
            </div>
            <!-- End to do list -->



</div>
<div class="clearfix"></div>
</div>
<!-- /page content -->