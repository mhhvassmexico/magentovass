<?php 
    global $select, $helper; 
    $perfiles = $select->selectPerfiles();
    $sprints =  $select->selectSprints();    
    $fechaDetalleSprint = $helper->fechaDetalleSprint($sprints[0]['fecha_inicio'], $sprints[0]['fecha_fin']);

    $dataSprint = $select->selectTicketsSprint();
?>

        <!-- page content -->
        <div class="right_col" role="main">
          <!-- top tiles -->
            <div class="row top_tiles">
              <?php foreach($perfiles as $key => $value):?>

                      <div class="col-md-2 col-sm-2 col-xs-12 profile_details">
                        <div class="well profile_view">
                          <div class="col-sm-12">
                            <h4 class="brief"><i>Equipo MAGENTO</i></h4>
                            <div class="left col-xs-7">
                              <h2><?= $value['nombre'] ?>&nbsp;<?= $value['apellidos'] ?></h2>
                              <ul class="list-unstyled">                                
                                <li><i class="fa fa-map-marker"></i> Cargo: <?= $value['cargo'] ?></li>
                                <li><i class="fa fa-phone"></i> Telefono #: <?= $value['tel'] ?></li>
                                <li><i class="fa fa-envelope"></i> Email #: <?= $value['email'] ?></li>
                                <li><i class="fa fa-skype"></i> Skype #: <?= $value['skype'] ?></li>
                              </ul>
                            </div>
                            <div class="right col-xs-5 text-center">                              
                              <img src="<?= $helper->imagenPerfil($value['avatar'])?>" width="140" alt="" class="img-circle img-responsive">
                            </div>
                          </div>
                          <div class="col-xs-12 bottom text-center">
                            <div class="col-xs-12 col-sm-6 emphasis">
                              <p class="ratings">
                                <a>0.0</a>
                                <a href="#"><span class="fa fa-star-o"></span></a>
                                <a href="#"><span class="fa fa-star-o"></span></a>
                                <a href="#"><span class="fa fa-star-o"></span></a>
                                <a href="#"><span class="fa fa-star-o"></span></a>
                                <a href="#"><span class="fa fa-star-o"></span></a>
                              </p>
                            </div>
                            <div class="col-xs-12 col-sm-6 emphasis">                              
                              <a href="perfil.php?id_perfil=<?= $value['id_perfil'] ?>">
                              <button type="button" class="btn btn-primary btn-xs">
                                <i class="fa fa-user"> </i> Ver Perfil
                              </button>
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>

              <?php endforeach; ?>              
            </div>
          <!-- /top tiles -->
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel tile fixed_height_620">
                <div class="x_title">
                  <h2><?= $sprints[0]['nombre'] ?> (<?= $fechaDetalleSprint ?>)</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <h4>Proceso de avance del <?= $sprints[0]['nombre'] ?></h4>

                  <?php foreach($dataSprint as $key => $value): ?>
                  <div class="widget_summary">
                    <div class="w_left w_25">
                      <span><?= $value['numero_jira']?></span>
                    </div>
                    <div class="w_center w_55">
                      <div class="project_progress">
                        <div class="progress progress_sm">
                          <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="<?= $value['porcentaje_avance']?>"></div>
                        </div>
                        <small><?= $value['porcentaje_avance']?>% Complete</small>
                      </div>
                    </div>
                    <div class="w_right w_20">
                      <span><a href="<?= $value['link_jira'] ?>" target="_blank">JIRA</a></span>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  <?php endforeach; ?>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Detalle de Actividades <small><?= $sprints[0]['nombre'] ?></small></h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <div class="dashboard-widget-content">

                    <ul class="list-unstyled timeline widget">
                    <?php foreach($dataSprint as $key => $value): ?>
                      <li>
                        <div class="block">
                          <div class="block_content">
                            <h2 class="title">
                              <a><?=utf8_encode($value['nombre'])?></a>
                            </h2>
                            <div class="byline">
                              <span><?= $helper->fechaCreacionTicket($value['fecha_creacion'])?></span> asignado <a href="perfil.php?id_perfil=<?= $value['id_perfil']?>"><?= $value['nombreP']?><?= $value['apellidosP']?></a>
                            </div>
                            <p class="excerpt"><?= utf8_encode($value['descripcion'])?> <a> ver mas...</a>
                            </p>
                          </div>
                        </div>
                      </li>
                    <?php endforeach; ?>  
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->