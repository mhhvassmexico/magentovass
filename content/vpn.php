<!-- page content -->
<div class="right_col" role="main">
<div class="">
  <div class="page-title">
    <div class="title_left">
      <h3>Conectarse a la VPN</h3>
    </div>    
  </div>

  <div class="clearfix"></div>


  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Conexion <small>VPN</small></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

        <div class="bs-example" data-example-id="simple-jumbotron">
          <div class="jumbotron">            
            <p>Para poder revisar los cambios aplicados al ambiente en Desarrollo es importante abrir un canal de comunicación por VPN y revisar vía WEB el cambio aplicado.</p>
          </div>
        </div>

      </div>
    </div>
  </div>






  









              <!-- Start to do list -->
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Programas de conexion <small>Herramientas de acceso</small></h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <div class="accordion">
                    <div class="panel">                      
                      <ul class="to_do">
                        <li>
                          <a class="panel-heading" role="tab" id="headingOne1" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne1" aria-expanded="false" aria-controls="collapseOne" style="border: 1px solid #ccc;">                          
                            <span><img src="images/openvpn.png" width="50"></span>&nbsp;&nbsp;&nbsp;
                            <strong>OpenVPN GUI</strong> cliente VPN
                            </a>
                            <div id="collapseOne1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                              <div class="panel-body">
                                  <ul>
                                    <li><strong>1.- </strong> Descargar el programa <a href="https://openvpn.net/index.php/open-source/downloads.html" target="_blank">DESCARGAR</a></li>
                                    <li><strong>2.- </strong> Instalar la apllicación</li>
                                    <li><strong>3.- </strong> Solicitar las credenciales de acceso al correo <a href="mailto:marcos.huerta@vass.com.mx">marcos.huerta@vass.com.mx</a></li>             
                                    <li><strong>4.- </strong> Copiar las crdenciales dentro de la ruta "config" de la carpeta de instalacion del programa OpenVPN GUI en windows se localiza en "C:\Program Files\OpenVPN\config" dentro del este directorio hay que pegar los archivos de las credenciales</li>                       
                                    <li><strong>5.- </strong> Ejecutar el programa y se mostrara un icono en la barra de tareas <img src="images/icon_vpn.png" width="30"> </li>
                                    <li><strong>6.- </strong> Dar un clic botono derecho y se mostraran la configuracion de las VPN copiadas en la carpeta config <img src="images/clic_derecho.png" width="200"> en mi caso tendre 2 que representan los dos archivos que coloque en la carpeta config <img src="images/config.png" width="350"></li>
                                    <li><strong>7.- </strong> Conectar con una VPN simplemente dandole un clic a Connect <img src="images/conectar.png" width="350"></li>
                                    <li>Al Final se mostrara que la conexion fue exitosa <img src="images/s.png" width="550"></li>                                    
                                  </ul>
                              </div>
                            </div>
                        </li>                      
                      </ul>                      
                    </div>


                      <div class="panel">
                        <a class="panel-heading collapsed" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                          <h4 class="panel-title">Sitio web por URL</h4>
                        </a>
                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                          <div class="panel-body">
                            <p>
                              <strong>Desarrollo</strong>
                            </p>
                            <p>
                              <ul>
                                <li><strong>IP</strong> <a href="http://10.1.0.100" target="_blank">1 0.1.0.100</a></li>
                              </ul>
                            </p>
                            <p>
                              <strong>Certificación</strong>
                            </p>
                            <p>
                              <ul>
                                <li><strong>IP </strong><a href="http://10.2.0.101" target="_blank"> 10.2.0.101</a></li>
                                <li><strong>IP </strong><a href="http://10.2.0.102" target="_blank"> 10.2.0.102</a></li>
                              </ul>
                            </p>
                            <p>
                              <strong>Producción</strong>
                            </p>
                            <p>
                              <ul>
                                <li><strong>IP </strong><a href="#"> NO ASIGNADA</a></li>
                              </ul>
                            </p>
                          </div>
                        </div>
                      </div>

                      <div class="panel">
                        <a class="panel-heading collapsed" role="tab" id="headingThree" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                          <h4 class="panel-title">Credenciales</h4>
                        </a>
                        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                          <div class="panel-body">
                            <p><strong>Archivos de conexion</strong>
                            </p>
                            <p>
                              <ul>
                                <li><strong>Desarrollo </strong> <a href="download.php?file=cert/desarrollo.ovpn">desarrollo.ovpn</a></li>
                                <li><strong>Certificación </strong> <a href="download.php?file=cert/certificacion.ovpn">certificacion.ovpn</a></li>
                              </ul>
                            </p>
                          </div>
                        </div>
                      </div>                      


                  </div>
                </div>
              </div>
            </div>
            <!-- End to do list -->



</div>
<div class="clearfix"></div>
</div>
<!-- /page content -->