<?php
  
  //Authentication rest API magento2.Please change url accordingly your url
  $adminUrl='http://qa.movistar.com.mx/tienda/index.php/rest/V1/integration/admin/token';
  $ch = curl_init();
  $data = array("username" => "hector", "password" => "Hector2018api!");                                                                    
  $data_string = json_encode($data);                       
  $ch = curl_init($adminUrl); 
  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
  curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
  curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
      'Content-Type: application/json',                                                                                
      'Content-Length: ' . strlen($data_string))                                                                       
  );       
  $token = curl_exec($ch);
  $token =  json_decode($token);

?>


<!-- page content -->
<div class="right_col" role="main">
<div class="">
  <div class="page-title">
    <div class="title_left">
      <h3>Servicios API MAGENTO 2</h3>
    </div>    
  </div>

  <div class="clearfix"></div>







  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Consumo de Catalogo de Productos <small>MAGENTO 2.2.3</small></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">







                    <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
                      <div class="profile_img">
                        <div id="crop-avatar">
                          <!-- Current avatar -->
                          <img class="img-responsive avatar-view" src="images/logo-magento.png" alt="Avatar" title="Change the avatar">
                        </div>
                      </div>
                      <h3>Enrique Chavez</h3>

                      <ul class="list-unstyled user_data">
                        <li><i class="fa fa-envelope user-profile-icon"></i> enrique.chavez@vass.com.mx
                        </li>

                        

                        
                      </ul>
                      
                      <br>
                    </div>



                    <div class="col-md-9 col-sm-9 col-xs-12">

                      <div class="profile_title">
                        <div class="col-md-6">
                          <h2>Servicio WEB Uso del API en MAGENTO 2 </h2>
                        </div>
                        <div class="col-md-6">
                          
                          
                        </div>
                      </div>
                      <!-- start of user-activity-graph -->
                      <div id="graph_bar" style="width: 100%; height: 280px; position: relative; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                        
                        <div class="morris-hover morris-default-style" style="left: 57.9266px; top: 111px; display: none;">
                          <div class="morris-hover-row-label">iPhone 4</div>
                          <div class="morris-hover-point" style="color: #26B99A">Geekbench: 380</div>
                        </div>
                    </div>



                      <div class="" role="tabpanel" data-example-id="togglable-tabs">
                        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                          <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Referencia</a>
                          </li>
                          <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Detalles tecnicos</a>
                          </li>
                          <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Test funcionalidad</a>
                          </li>
                        </ul>
                        <div id="myTabContent" class="tab-content">
                          <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">

                            <!-- start recent activity -->
  <h2>"Ejemplo de uso de API Magento 2"</h2>
  <p>Explicación de autenticación basada en OAuth => <a href="https://devdocs.magento.com/guides/v2.2/get-started/authentication/gs-authentication-oauth.html" target="_blank"> https://devdocs.magento.com/guides/v2.2/get-started/authentication/gs-authentication-oauth.html</a></p>
  <p>Autenticación basada en tokens => <a href="https://devdocs.magento.com/guides/v2.2/get-started/authentication/gs-authentication-token.html" target="_blank">https://devdocs.magento.com/guides/v2.2/get-started/authentication/gs-authentication-token.html</a></p>
  <p>Link recomendado para crear búsquedas especializadas => <a href="https://devdocs.magento.com/guides/v2.2/rest/performing-searches.html" target="_blank"> https://devdocs.magento.com/guides/v2.2/rest/performing-searches.html</a></p>
                            <!-- end recent activity -->

                          </div>
                          <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">

                            <!-- start user projects -->
                            

<h2>Detalles de los archivos y código esencial:</h2>
<blockquote>
  PHP: /*Encargado de obtener el token de seguridad para poder hacer uso de la API de MAgento 2*/ <br>
  //Authentication rest API magento2.Please change url accordingly your url <br>
  $adminUrl='<b>-hostingMagento-</b>/index.php/rest/V1/integration/admin/token'; <br>
  $ch = curl_init(); <br>
  $data = array("username" => "<b>-usuario-</b>", "password" =>"<b>-contraseña-</b>"); <br>
  $data_string = json_encode($data); <br>                       
  $ch = curl_init($adminUrl); <br> 
  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST"); <br>                                                                     
  curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string); <br>                                                                 
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); <br>                                                                      
  curl_setopt($ch, CURLOPT_HTTPHEADER, array( <br>                                                                         
      'Content-Type: application/json', <br>                                                                                
      'Content-Length: ' . strlen($data_string)) <br>                                                                       
  ); <br>       
  $token = curl_exec($ch); <br>
  $token =  json_decode($token); <br>
</blockquote>
<blockquote>
  PHP: /*Encargado de realizar la búsqueda indicada y retornar la respuesta en formato json*/ <br>
  //Use above token into header <br>
  $headers = array("Authorization: Bearer $token"); <br><br>

  $requestUrl='<b>-hostingMagento-</b>/index.php/rest/V1/products/<b>-criterios de busqueda-</b>' <br><br>

  $ch = curl_init(); <br>
  $ch = curl_init($requestUrl); <br>
  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers); <br>
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); <br><br> 

  $result = curl_exec($ch); <br>
  $result=  json_decode($result); <br>
  var_dump($result); <br>
</blockquote>

<blockquote>
  JAVA: <br>
        Se encontró un módulo de ejemplo que circula por la red => 
        <a href="recursosdig/java-magento-client-master.zip" >Descarga aquí</a>

</blockquote>
<h2>Detalle a groso modo de la estructura de cada búsqueda señala en la sección superior:</h2>
<ul>
  <li>1.- Buscar un sku en específico: <b>-hostingMagento-</b>/index.php/rest/V1/products/<b>-sku-</b></li>
  <li>2.- Buscar los primeros 20 productos: <b>-hostingMagento-</b>/index.php/rest/V1/products<b>?searchCriteria[pageSize]=20</b></li>
  <li>3.- Buscar todos los productos del catálogo: <b>-hostingMagento-</b>/index.php/rest/V1/products<b>?searchCriteria</b></li>
  <li>4.- Filtrando datos relevantes sku 45734: <b>-hostingMagento-</b>/index.php/rest/V1/products/<b>-sku-</b> <br><br> 
    //En el ejemplo 4 manipulamos el json devuleto por Magento<br>
    foreach($result as $clave => $valor) { <br>
      if($clave == "price" || $clave == "sku" || $clave == "name"){ <br>
        echo "$clave => $valor\n"; <br>
      } <br>
    } <br>
  </li>
</ul>
<p>
  Con esto lograremos hacer uso de la API y manejar el json retornado por Magento como mejor nos convenga.
</p>                            



                            <!-- end user projects -->

                          </div>
                          <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
                            


<p>Test de funcionalidad <strong>para PHP</strong>:</p>
  <form action="#" method="POST">
    <select name="tquery" id="tquery" onchange="realizaBusqueda()" class="form-control">
      <option value="busqueda-0" id="elimina">Elija una opción</option>
        <option value="busqueda-1">1.- Buscar un sku en específico</option>
        <option value="busqueda-2">2.- Buscar los primeros 20 productos</option>
        <option value="busqueda-3">3.- Buscar todos los productos del catálogo</option>
        <option value="busqueda-4">4.- Filtrando datos relevantes sku 45734</option>
    </select>
    <input type="hidden" name="token" id="token" value="<?php echo $token; ?>">
  </form>
  
  <div id="resultado" style="border:2px solid #bababa;position:relative;display: block;margin: 24px auto;width: 90%;min-height: 24px;padding: 12px;"></div>



                          </div>
                        </div>
                      </div>








      </div>
    </div>
  </div>





</div>
<div class="clearfix"></div>
</div>
<!-- /page content -->            



<script type="text/javascript">
  function realizaBusqueda(valorCaja1,valorCaja2){
    $('#elimina').remove();
    var parametros ={
      "valorCaja1":  $('#tquery').val(),
      "valorCaja2": $('#token').val()
    };
    $.ajax({
      data: parametros,
      url: 'servicios/querys.php',
      type: 'post',
      beforeSend: function(){
        $('#resultado').html('Procesando, espere por favor....');   
      },
      success: function(response){
        $('#resultado').html(response);
      }
    });
  }
</script>