<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>DUMP <small> Mysql</small></h3>
      </div>

    </div>

    <div class="clearfix"></div>

    <div class="row">
      <div class="clearfix"></div>
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Dump de la Base de datos de Desarrollo <small></small></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>              
            </ul>
            <div class="clearfix"></div>
          </div>

          <div class="x_content">

            <p>Listado de Dumps de las intancias del proyecto en MAGENTO 2</p>

            <div class="table-responsive">
              <table class="table table-striped jambo_table bulk_action">
                <thead>
                  <tr class="headings">                    
                    <th class="column-title">Fecha Publicación </th>
                    <th class="column-title">Nombre Paquete </th>
                    <th class="column-title">Link </th>
                    <th class="column-title">Comentario </th>
                    <th class="column-title">Estatus </th>                    
                  </tr>
                </thead>

                <tbody>
                  <tr class="even pointer">                    
                    <td class="">24-07-2018</td>
                    <td class="">dump_magento_24072018</td>                                          
                    <td class=""><a href="download.php?file=recursos/db/24_07_2018.sql">DESCARGAR</a></td>                    
                    <td class=" ">Respaldo de la Baase de datos en Desarrollo</td>    
                    <td class=" ">Activo</td>                
                    </td>
                  </tr>                  
                  <tr class="even pointer">                    
                    <td class="">03-07-2018</td>
                    <td class="">dump_magento_03072018</td>                                          
                    <td class=""><a href="download.php?file=recursos/db/25_06_2018_magento_db.sql">DESCARGAR</a></td>                    
                    <td class=" ">Respaldo de la Baase de datos en Desarrollo</td>    
                    <td class=" ">Activo</td>                
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
			
		
          </div>
        </div>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
</div>