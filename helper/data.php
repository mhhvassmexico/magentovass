<?php

class data
{

	public function validaSession()
	{			
		if(!isset($_SESSION['magento'])){
			header("Location: login.php");
		}
		
	}

	public function __construct()
	{

	}

	public function pagina()
	{
		//$pagina = array_pop(explode('/', $_SERVER['PHP_SELF']));
		$p = explode('/', $_SERVER['PHP_SELF']);
		$pagina = $p[(count($p)-1)];
		
		$path = '';
		if(file_exists('content/'.$pagina)){			
			require_once("content/".$pagina);
		}else{			
			$this->createPage($pagina);
		}				
	}

	private function createPage($page)
	{
		$page = fopen('content/'.$page, "w+");
		fwrite($page, '<div class="row"></div>');
		fclose($page);
	}

	public function fechaDetalleSprint($fecha_incio, $fecha_fin)
	{
		// del 2 jun al 5 jun    
		$d = explode("-",$fecha_incio);
		$d2 = explode("-",$fecha_fin);
		$dia = $this->fechaNombreMes($d[1]);
		$dia2 = $this->fechaNombreMes($d2[1]);
		return "del ".$d[2]." de ".$dia." al ".$d2[2]." de ".$dia2;
	}

	public function fechaNombreMes($dia)
	{
		$s = '';
		switch($dia){
			case '01': $s = 'Enero'; break;
			case '02': $s = 'Febrero'; break;
			case '03': $s = 'Marzo'; break;
			case '04': $s = 'Abril'; break;
			case '05': $s = 'Mayo'; break;
			case '06': $s = 'Junio'; break;
			case '07': $s = 'Julio'; break;
			case '08': $s = 'Agosto'; break;
			case '09': $s = 'Septiembre'; break;
			case '10': $s = 'Octubre'; break;
			case '11': $s = 'Noviembre'; break;
			case '12': $s = 'Diciembre'; break;
		}
		return $s;
	}

	public function Msg()
	{
		if(isset($_GET['msg'])&&$_GET['msg']!=''){
			echo '<script type="text/javascript">jQuery(document).ready(function(){setTimeout(function(){ jQuery(".alert.alert-success").fadeOut() },4000)});</script>';
			echo '<div class="alert alert-success">';
			echo '    '.$_GET['msg'];
			echo '</div>';
		}
	}

	public function fechaCreacionTicket($fecha)
	{
		return $fecha;
	}

	public function imagenPerfil($imagen)
	{
		$img = 'images/user.png';
		if($imagen!=''){
			$img = 'images/'.$imagen;
		}
		return $img;
	}
}