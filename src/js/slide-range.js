// Slide range
$('#priceRange').asRange({
  range: true,
  limit: true,
  step: 1,
  min: 0,
  max: 600000,
  onChange: function (value) {
    $('#price').text(`$${value[0].toLocaleString('es-ES')} - $${value[1].toLocaleString('es-ES')}`)
  }
})

