$('#paymentMethod1').show();
$('#paymentMethod2').hide();

$('#btnPaymentMethod1').click(function () {
  $('#paymentMethod1').toggle('fast');
  $('#paymentMethod2').toggle('fast');
  $("i").toggleClass("i-arrow-down i-arrow-up");
});

$('#btnPaymentMethod2').click(function () {
  $('#paymentMethod1').toggle('fast');
  $('#paymentMethod2').toggle('fast');
  $("i").toggleClass("i-arrow-up i-arrow-down");
});
