///////////////////////TABS -[Car Checkout paso 2]///////////////////
//Estado cuando carga la página
$(".board-tabs__cont").hide(); //contenido oculto
$(".board-tabs__link:first").addClass("js-tab__linkActive").show(); //Tab activa
$(".board-tabs__cont:first").show(); //Contenido activo a primera vista

// //Función para el evento click en el botón activo
$(".board-tabs__link").click(function () {

  $(".board-tabs__link").removeClass("js-tab__linkActive"); //Remove any "active" class
  $(this).addClass("js-tab__linkActive"); //Add "active" class to selected tab
  $(".board-tabs__cont").hide(); //Hide all tab content

  var activeTab = $(this).attr("href"); //Lee el valor del atributo href para identificar la pestaña activa
  //console.log(activeTab);
  $(activeTab).fadeIn(); //Fade in para visibilizar el contenido
  return false;
});
////# end TABS -[Car Checkout paso 2]///////
///TABS FORM -[Car Checkout paso 2]////
// $(window).resize(function () {
//   if (this.resizeTO) clearTimeout(this.resizeTO);
//   this.resizeTO = setTimeout(function () {
//     $(this).trigger('resizeEnd');
//   }, 500);
//   console.log(resizeEnd);
// });
// $(window).bind("resizeEnd", function () {

//   if ($(window).width() < 768) {
//     $(".js-tabContent").hide();
//     $(".js-tabBtn").click(function () {
//       $(this).next(".js-tabContent").slideToggle();
//       $(this).toggleClass("down"); //console.log(this);
//     });
//   }else {
//     $('.js-tabContent').css('display', 'flex');
//   }
// });


// $('.js-tabBtn').click(function () {
//   $('.js-tabContent').toggleClass('js-open');
// });

///# TABS FORM -[Car Checkout paso 2]////
/////Data-sheet__list [comparador]//////
$(".js-btnData").click(function (e) {
  e.preventDefault();
  $(".js-listData").slideToggle();
});

// Panel Tabs - [panel.html]
function panelTabs() {
  $('.js-tabBtn').click(function () {
    if ($(this).hasClass('js-tabOpen')) {
      $(this).removeClass('js-tabOpen').addClass('js-tabClose');
      $(this).siblings(".js-tabContent").slideUp();
    } else {
      $('.js-tabBtn').addClass('js-tabClose').removeClass('js-tabOpen');
      $(".js-tabContent").slideUp();
      $(this).removeClass('js-tabClose').addClass('js-tabOpen');
      $(this).siblings(".js-tabContent").slideDown();
    }
  });
}

$(document).ready(function () {
  // Panel Tabs - [panel.html]
  panelTabs()
})
