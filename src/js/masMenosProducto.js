  // ///////////////////////////////////////////////////////////////
  // Inicio botones mas y menos
  $('.group-input__btn').click(function (e) {
    var num = document.getElementById('addon')
    var max
    if (e.target.dataset.btn == '+') {// Setear el numero maximo de productos
      max = e.target.dataset.max
    }
    // Funcion botones aumentar o disminuir
    if (e.target.dataset.btn == '+' && num.value * 1 < max) {
      num.value = num.value * 1 + 1      
    }
    if (e.target.dataset.btn == '-' && num.value * 1 > 1) {
      num.value = num.value * 1 - 1
    }

    if (num.value == max) { // Si sellega al maximo muestra modal
      $('.modal').addClass('modal__view')
    }
  })

  // Fin botones mas y menos
  // ///////////////////////////////////////////////////////////////
