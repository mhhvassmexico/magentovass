//Inicio funcionalidad [Detalle de producto - BARRA STICKY] : Funcionalidad sticky para desktop
if ($(".band .list-sticky")[0]) {
  //Identifica si esta en pagina detalle producto
  window.addEventListener("scroll", () => {
    //Añade evento al scrool
    el = $("section:nth-child(1)"); //Identifica y guarda el primer section que es despues de este done aparece la barra
    pos = el.height() + el.position().top; //Guarda el valor de la posición (alto del div + el alto de la posición)
    if (window.scrollY >= pos - 170) {
      // Si ya se paso el primer section añade la classe sticky si no la quita
      $(".band").addClass("sticky");
      $(".content").addClass("band-sticky");
    } else {
      $(".band").removeClass("sticky");
      $(".content").removeClass("band-sticky");
    }
  });
}
//Fin funcionalidad [Detalle de producto - BARRA STICKY] : Funcionalidad sticky para desktop

//Inicio funcionalidad Boton carrito fixed bottom
function btnFixedBottom() {
  if ($(".btn-circle.head__item")[0] && $(window).width() > 768) {
    window.addEventListener("scroll", () => {
      //Añade evento al scrool
      a = $(".band_shadow").height();
      if (window.scrollY >= a) {
        // Si ya se paso el primer section añade la classe sticky si no la quita
        $(".js-btnCart.head__item").addClass("btn-circle-Fixed-bottom");
        $('.js-btnCart').addClass('animated bounceInUp');
      } else {
        $(".js-btnCart.head__item").removeClass("btn-circle-Fixed-bottom");
        $('.js-btnCart').removeClass('animated bounceInUp');
      }
    });
  }
}
//Fin funcionalidad Boton carrito fixed bottom

$(document).ready(function () {
  // Botón carrito de compra se fija desde 768px
  // sala a partir del alto de una sección
  btnFixedBottom();
});
