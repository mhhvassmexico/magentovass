function tootltipData() {
  $(".js-btnTooltipData").on('click', function (e) {
    if ($(this).siblings(".js-tooltipData").hasClass("js-tooltipData-close")) {
      $(".js-tooltipData.js-tooltipData-open").slideUp().removeClass("js-tooltipData-open").addClass("js-tooltipData-close");
      $(this).siblings(".js-tooltipData").slideDown().addClass("js-tooltipData-open").removeClass("js-tooltipData-close");
    } else {
      $(this).siblings(".js-tooltipData").slideUp().removeClass("js-tooltipData-open").addClass("js-tooltipData-close");
    }
  });
}

$(document).ready(function () {
  tootltipData();
});

$(window).resize(function () {
  $(".js-btnTooltipData").unbind("click");
  tootltipData();
});