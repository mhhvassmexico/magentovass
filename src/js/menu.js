//Interacciones de menú

//dropdown list
function dropdownList() {
  $('.js-btn__hidden').click(function () {
    if ($(window).width() < 768) {
      var heightList = 0
      $(this).parent('.menu-brand__item').prependTo($(".menu-brand__list"))
      $(this).parent('.menu-brand__item').addClass('active').siblings().removeClass('active')
      if ($(this).parent('.menu-brand__item active') && $(".menu-brand__list").hasClass("list-close")) {
        heightList = $('.menu-brand__item').outerHeight() * 3
        $(".menu-brand__list").removeClass('list-close')
        $(".menu-brand__list").addClass('list-open')
        $(".menu-brand__list").css("height", heightList)
      } else {
        $(".menu-brand__list").removeClass("list-open")
        $(".menu-brand__list").addClass('list-close')
        $(".menu-brand__list").css("height", $('.menu-brand__item').outerHeight())
      }
    }
  })
}

/////////////////////////////////////////////////////////////////
//Funcionalidad desplegable de search en mobile
function menuSearchMob() {
  if ($(window).width() < 768) {
    $(".js-btnSearch").click(function () {
      $(".submenu-tabs__list").removeClass("js-menuLoginOpen")
      $(".submenu-tabs__list").addClass("js-menuLoginClose")
      $(".menu-gral__list").removeClass("js-menuOpen")
      $(".menu-gral__list").addClass("js-menuClose")
      $(".menu-gral__burger").removeClass("js-menuOpen")
      $(".menu-gral__burger").addClass("js-menuClose")

      if ($(".menu-search").hasClass("js-menuSearchClose")) {
        $(".menu-search").removeClass("js-menuSearchClose")
        $(".menu-search").addClass("js-menuSearchOpen")
      } else {
        $(".menu-search").removeClass("js-menuSearchOpen")
        $(".menu-search").addClass("js-menuSearchClose")
      }
    })
  }
}
//fin Funcionalidad desplegable de search en mobile
/////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////
//Funcionalidad desplegable de iniciar sesion
function menuLogin() {
  $(".js-menuLogin").click(function () {
    $(".menu-search").removeClass("js-menuSearchOpen")
    $(".menu-search").addClass("js-menuSearchClose")
    $(".menu-gral__list").removeClass("js-menuOpen")
    $(".menu-gral__list").addClass("js-menuClose")
    $(".menu-gral__burger").removeClass("js-menuOpen")
    $(".menu-gral__burger").addClass("js-menuClose")

    if ($(window).width() > 768) {
      $(".menu-gral__item").removeClass("js-menuGralOpen")
      $(".menu-gral__item").addClass("js-menuGralClose")
      $(".submenu-gral__list").slideUp()
    }
    if ($(".submenu-tabs__list").hasClass("js-menuLoginClose")) {
      $(".submenu-tabs__list").removeClass("js-menuLoginClose")
      $(".submenu-tabs__list").addClass("js-menuLoginOpen")
    } else {
      $(".submenu-tabs__list").removeClass("js-menuLoginOpen")
      $(".submenu-tabs__list").addClass("js-menuLoginClose")
    }
  })
}
//fin Funcionalidad desplegable de iniciar sesion
/////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////
//Funcionalidad menu off canvas

function menuOffCanvas() {

  $(".menu-gral__burger").click(function () {
    $(".menu-search").removeClass("js-menuSearchOpen")
    $(".menu-search").addClass("js-menuSearchClose")
    $(".submenu-tabs__list").removeClass("js-menuLoginOpen")
    $(".submenu-tabs__list").addClass("js-menuLoginClose")

    if ($(this).hasClass("js-menuClose") && ($(".menu-gral__list").hasClass("js-menuClose"))) {
      $(this).removeClass("js-menuClose")
      $(this).addClass("js-menuOpen")
      $(".menu-gral__list").removeClass("js-menuClose")
      $(".menu-gral__list").addClass("js-menuOpen")
    } else {
      $(this).removeClass("js-menuOpen")
      $(this).addClass("js-menuClose")
      $(".menu-gral__list").removeClass("js-menuOpen")
      $(".menu-gral__list").addClass("js-menuClose")
    }
  })
}
//fin Funcionalidad menu off canvas
/////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////
//Funcionalidad dropdown menu
function dropdownMenu() {
  $(".js-menuGralBtn").click(function () {
    var menuItemActive = $(this).parents(".menu-gral__item")
    var heightMenuItem = $(this).siblings(".submenu-gral__list").outerHeight()
    if ($(window).width() < 768) {
      if (menuItemActive.hasClass("js-menuGralClose")) {
        menuItemActive.css("height", heightMenuItem + 40)
        menuItemActive.removeClass("js-menuGralClose")
        menuItemActive.addClass("js-menuGralOpen")
        menuItemActive.siblings().removeClass("js-menuGralOpen").addClass("js-menuGralClose").css("height", 40)
      } else {
        menuItemActive.addClass("js-menuGralClose")
        menuItemActive.removeClass("js-menuGralOpen")
        menuItemActive.css("height", 40)
      }
    } else {
      $(".submenu-tabs__list").removeClass("js-menuLoginOpen")
      $(".submenu-tabs__list").addClass("js-menuLoginClose")

      if (menuItemActive.hasClass("js-menuGralClose")) {
        menuItemActive.removeClass("js-menuGralClose")
        menuItemActive.addClass("js-menuGralOpen")
        $(this).siblings(".submenu-gral__list").slideDown()
        menuItemActive.siblings(".menu-gral__item").children(".submenu-gral__list").slideUp()
        menuItemActive.siblings(".menu-gral__item").removeClass("js-menuGralOpen").addClass("js-menuGralClose")
      } else {
        menuItemActive.addClass("js-menuGralClose")
        menuItemActive.removeClass("js-menuGralOpen")
        $(this).siblings(".submenu-gral__list").slideUp()
      }
    }
  })
}
//fin Funcionalidad dropdown menu
/////////////////////////////////////////////////////////////////

//hover menu
// function dropdownMenuDesk() {

//   $(".js-menuGralClose").hover(function () {
//     $(this).addClass("hola")
//     $(this).children(".submenu-gral__list").slideToggle()
//   })
// }
//fin hover menu

/////////////////////////////////////////////////////////////////
//Funcionalidad dropdown footer
function dropdownFooter() {
  $(".js-footBtn").click(function () {
    if ($(window).width() < 768) {
      var footListActive = $(this).siblings(".foot__sublist")
      if ($(this).hasClass("js-footItemClose")) {
        // $(".js-footBtn").removeClass("js-footItemOpen")
        $(this).removeClass("js-footItemClose")
        $(this).addClass("js-footItemOpen")
        footListActive.slideToggle()
        $("html, body").animate({
            scrollTop: $(this).offset().top - 50
          },
          500
        )
      } else {
        footListActive.slideToggle()
        $(this).removeClass("js-footItemOpen")
        $(this).addClass("js-footItemClose")
      }
    }
  })
}
//fin Funcionalidad dropdown footer
/////////////////////////////////////////////////////////////////

//Funcionalidad Side menu
function sideMenu() {
  $('.js-btnCart').click(function () {
    
    if ($('.nav-side').hasClass('js-slideOutofView')) {
      if(window.scrollY < 190){
        const top = 180 - window.scrollY
        $('.nav-side').css('top', top + 'px')
      } else {
        $('.nav-side').css('top', '0')
      }

      $('.nav-side').removeClass('js-slideOutofView')
      $('.nav-side').addClass('js-slideToView')
    } else {
      $('.nav-side').addClass('js-slideOutofView')
      $('.nav-side').removeClass('js-slideToView')
    }
  })
  $('.js-sideClose').click(function () {
    $('.nav-side').addClass('js-slideOutofView')
    $('.nav-side').removeClass('js-slideToView')
  })
}
$(window).scroll(function () {
  if ($('.nav-side').hasClass('js-slideToView') && window.scrollY < 190){
    const top = 180 - window.scrollY
    $('.nav-side').css('top', top + 'px')
  } else {
    $('.nav-side').css('top', '0')
  }
})
// Funcionalidad conteo y actualización numero items de slide menu
function updateCountItemsCart () {
  const count = $('.js-deleteItemCart').length
  const textSlideMenu = count === 1 ? 'equipo añadido' : 'equipos añadidos'
  $('.nav-side__title').text(`${count} ${textSlideMenu} al carrito`)
  $('.btn-circle-small').text(count)
}

// Funcionalidad eliminar items de slide menu
function deleteItemCart (elem) {
  $(elem).parent().parent().remove()
  updateCountItemsCart()
}

$('.js-deleteItemCart').on('click', function () {
  deleteItemCart(this)
})

$(document).ready(function () {
  //Funcionalidad desplegable de search en mobile
  menuSearchMob()
  //dropdown list
  dropdownList()
  //menu logion - desplegable
  menuLogin()
  //Funcionalidad menu off canvas
  menuOffCanvas()
  //Funcionalidad dropdown menu
  dropdownMenu()
  //Funcionalidad dropdown footer
  dropdownFooter()
  // dropdownMenuDesk()
  sideMenu()
  // Actualiza numero de items en el carrito
  updateCountItemsCart()
})

$(window).resize(function () {
  //Funcionalidad desplegable de search en mobile
  $(".js-btnSearch").unbind("click")
  menuSearchMob()
  //dropdown list
  $(".js-btn__hidden").unbind("click")
  dropdownList()
  //menu logion - desplegable
  $(".js-menuLogin").unbind("click")
  menuLogin()
  $(".menu-gral__burger").unbind("click")
  //Funcionalidad menu off canvas
  menuOffCanvas()
  //Funcionalidad dropdown menu
  $(".js-menuGralBtn").unbind("click")
  if ($(window).width() < 768) {
    $(".submenu-gral__list").slideDown()
  } else {
    $(".submenu-gral__list").slideUp()
    $(".menu-gral__item").removeClass("js-menuGralOpen").addClass("js-menuGralClose")
  }
  dropdownMenu()
  //Funcionalidad dropdown footer
  $(".js-footBtn").unbind("click")
  dropdownFooter()
  // dropdownMenuDesk()
})
