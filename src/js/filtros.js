/*------------------------------------*\
  Funcionalidad open filtro y select order
\*------------------------------------*/


$(".js-filtroBtn").click(function () {
  if ($(".js-filtro").hasClass("js-filtroOpen")) {
    $(".js-filtro").removeClass("js-filtroOpen");
  } else {
    $(".js-filtro").addClass("js-filtroOpen");
  }
});
$(".js-filtro").click(function () {
  $(".js-filtro").removeClass("js-filtroOpen");
});
$(".js-orderBtn").click(function () {
  if ($(".js-order").hasClass("js-orderOpen")) {
    $(".js-order").removeClass("js-orderOpen");
  } else {
    $(".js-order").addClass("js-orderOpen");
  }
});

$(".js-order").click(function () {
  $(".js-order").removeClass("js-orderOpen");
});

$(".js-stop").click(function (e) {
  e.stopPropagation();
});

/*------------------------------------*\
  rango precio
\*------------------------------------*/
$(".range-price").asRange({
  range: true,
  limit: true,
  tip: {
    active: 'onMove'
  }
});

/*------------------------------------*\
  Dropdown Filtro
\*------------------------------------*/
$(".js-dropdownBtn").click(function () {
  if ($(this).hasClass("js-dropdownActive")) {
    $(this).removeClass("js-dropdownActive")
    $(this).siblings(".js-dropdownSlide").slideToggle();
  } else {
    $(this).addClass("js-dropdownActive")
    $(this).siblings(".js-dropdownSlide").slideToggle();
    if ($(window).width() > 1023) {
      $('html, body').animate({
        scrollTop: $(this).offset().top - 40
      }, 500);
    }
  }
});

/*------------------------------------*\
  Filtro Predictivo
\*------------------------------------*/
$('.js-inputAutocomplete').keyup(function() {
  this.value.length > 0 ? $('.js-panelAutocomplete').slideDown() : $('.js-panelAutocomplete').slideUp('fast')
});